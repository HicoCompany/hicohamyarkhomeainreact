import React, { Component } from 'react';
import { Image, StatusBar, Text, View } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import { setIsSeenIntroduce } from '../../redux/actions';
import { hp, normalize, wp } from '../../responsive';
import Colors from '../../utility/Colors';

class IntroScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            slides: [
                {
                    key: 'first',
                    title: 'به سامانه تفکیک از مبدا شهرداری خمین (دوستدار طبیعت) خوش آمدید',
                    text: 'شهروند عزیز، شما میتوانید به راحتی در اپلیکیشن همیار دوستدار طبیعت ثبت نام کرده و به عنوان همیار دوستدار طبیعت شناخته شوید.',
                    image: require('./../../assets/image/0.png'),
                    titleStyle: styles.titleStyle,
                    textStyle: styles.textStyle,
                    imageStyle: styles.imageStyle,
                    backgroundColor: '#015641',
                },
                {
                    key: 'second',
                    title: 'ثبت درخواست ',
                    text: 'همیار عزیز، شما می توانید زباله های قابل بازیافت(خشک) خود را از بین زمان های پیشنهاد شده، تحویل سفیران پاکی محله خود دهید و در ازای آن اعتبار دریافت کنید.',
                    image: require('./../../assets/image/1.png'),
                    titleStyle: styles.titleStyle,
                    textStyle: styles.textStyle,
                    imageStyle: styles.imageStyle,
                    backgroundColor: '#015641',
                },
                {
                    key: 'third',
                    title: 'جمع آوری',
                    text: 'سفیران پاکی در زمانی که مشخص نموده اید به محل زندگی شما مراجعه کرده و پسماند تفکیک شده ی شما را تحویل میگیرد .علاوه بر آن به ازای پسماند تحویل داده شده، امتیاز دریافت میکنید و میتوانید این امتیازات رو خرج کنید',
                    image: require('./../../assets/image/2.png'),
                    titleStyle: styles.titleStyle,
                    textStyle: styles.textStyle,
                    imageStyle: styles.imageStyle,
                    backgroundColor: '#015641',
                }
                // ,
                // {
                //     key: 'forth',
                //     title: 'تا زمینی پاک داشته باشیم...',
                //     text: '',
                //     image: require('./../../assets/image/3.png'),
                //     titleStyle: styles.titleStyle,
                //     textStyle: styles.textStyle,
                //     imageStyle: styles.imageStyle,
                //     backgroundColor: '#015641',
                // }
            ]
        };
    }

    renderItem = (item) => {
        return (
            <View style={styles.itemContainer}>
                <View style={styles.itemInner}>
                    <Image
                        source={item.image} 
                        style={item.imageStyle}
                    />
                    <Text style={item.titleStyle}>
                        {item.title}
                    </Text>
                    <Text style={item.textStyle}>
                        {item.text}
                    </Text>
                </View>
            </View>
        );
    }

    renderSkipButton () {
        return (
            <View style={styles.skipContainer}>
                <Text style={styles.skipText}>
                    {'گذشتن از صفحات'}
                </Text>
            </View>
        );
    }

    renderDoneButton () {
        return (
            <View style={styles.skipContainer}>
                <Text style={styles.skipText}>
                    {'ورود به نرم افزار'}
                </Text>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <AppIntroSlider
                    slides={this.state.slides}
                    showSkipButton
                    showPrevButton={false}
                    hideNextButton
                    hideDoneButton={false}
                    bottomButton
                    renderItem={this.renderItem}
                    renderSkipButton={this.renderSkipButton}
                    renderDoneButton={this.renderDoneButton}
                    onDone={() => {
                        this.props.setIsSeenIntroduce();
                        this.props.navigation.navigate('login');
                    }}
                    onSkip={() => {
                        this.props.setIsSeenIntroduce();
                        this.props.navigation.navigate('login');
                    }}
                />
            </View>
        );
    }
}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
    },
    skipContainer: {
        height: hp('5%'),
        width: wp('50%'),
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: Colors.button,
        color: Colors.white,
        // borderRadius: 30,
        // borderWidth: 1,
        // borderColor: Colors.white
    },
    skipText: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.white,
    },
    titleStyle: {
        marginTop: 10,
        fontSize: normalize(16),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.button
    },
    textStyle: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.button
    },
    imageStyle: {
        width: wp('60%'),
        marginTop:hp('10%'),
        height: hp('40%'),
        resizeMode : 'contain',
        alignSelf: 'center',
    },
    itemContainer: {
        flex: 1,
        //backgroundColor: '#015641',
        height: hp('60%'),
        width: wp('100%'),
    },
    itemInner: {
        margin: 10,
        height: hp('60%'),
        width: wp('100%'),
        backgroundColor: '#ffffff',
        alignSelf: 'center',
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {
    setIsSeenIntroduce
};

export default connect(mapStateToProps, mapActionToProps)(IntroScreen);
