import React, { Component,useState } from 'react';
import { Alert, Image, Keyboard, ScrollView, StatusBar, Text, TextInput,CheckBox, View,Linking,TouchableOpacity } from 'react-native';
import { NetInfo } from '@react-native-community/netinfo';

import DeviceInfo from 'react-native-device-info';
import EStyleSheet from 'react-native-extended-stylesheet';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { setMobileNumber } from '../../redux/actions';
import Repository from '../../repository/Repository';
import Colors from '../../utility/Colors';
import Utils from '../../utility/Utils';
import { Button } from './../../components';
import { hp, normalize, wp } from './../../responsive';

class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      isLoadingButton: false,
      Mobile: '',
      isSelected:false,
      link:''
    };

    this.onConfirm = this.onConfirm.bind(this);
  }

  componentDidMount = () => {
    this.getApi();
    
    //NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
    //NetInfo.isConnected.fetch().done((isConnected) => { this.setState({ isConnected }); });
  }
  getApi = async () => {
    await this.PhoneShowAppRulesApi();
    }
async PhoneShowAppRulesApi() {
      const headers = null;

      const params = {
        // Mobile: this.props.userData.mobileNumber,
        // Code: this.props.userData.codeNumber
      };
      
      try {
        const response = await Repository.PhoneShowAppRulesApi(params, headers);
        this.setState({
          link:response
        });
        
      } catch (error) {
        await this.setState({
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
            apiError: true,
            apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
      }
      }
  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  }

  componentWillUnmount = () => {
    //NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
  }

  onConfirm = () => {
    Keyboard.dismiss();
    const {
      isConnected,
      Mobile,
      isSelected
    } = this.state;
    this.setState({ error: '' });
    if (isConnected) {
      if (isSelected === false) {
        this.setState({ error: 'شرایط  استفاده از خدمات و حریم خصوصی را مطالعه و تایید نمایید' });
      } 
      else if (Mobile.length === 0) {
        this.setState({ error: 'شماره همراه خود را وارد کنید.' });
      } else if (Mobile.length < 11) {
        this.setState({ error: 'شماره همراه خود را کامل وارد کنید.' });
      } else {
        this.setState({
          error: '',
          isLoadingButton: true
        }, () => {
          this.phoneRegisterNaturalPersonApi();
        });
      }
    } else {
      Alert.alert(
        'اینترنت گوشی خود را بررسی کنید',
        '',
        [
          { text: 'انصراف' },
          { text: 'تلاش مجدد', onPress: this.onConfirm },
          { text: 'باشه' },
        ],
        { cancelable: true }
      );
    }
  }
  setSelection(selected){
this.setState({
  isSelected:selected
});
  }
  linkUrl(){
     Linking.openURL(this.state.link);
}
  async phoneRegisterNaturalPersonApi() {
    const deviceId = DeviceInfo.getUniqueID();
    const params = {
      Mobile: Utils.toEnglishDigits(this.state.Mobile),
      DeviceID: deviceId
    };
    //alert(JSON.stringify(params));
    try {
      const response = await Repository.PhoneRegisterNaturalPersonApi(params);
      this.setState({ isLoadingButton: false });
      //alert(JSON.stringify(response));
      if (response.Success === 1) {
        this.props.setMobileNumber(Utils.toEnglishDigits(this.state.Mobile));
        this.props.navigation.navigate('loginConfirm');
        // this.props.navigation.dispatch(
        //   StackActions.reset(
        //     {
        //       index: 0,
        //       key: null,
        //       actions: [NavigationActions.navigate({ routeName: 'loginConfirm' })]
        //     }
        //   )
        // );
      }
      else {
        this.setState({ error: response.Text });
      }
    } catch (error) {
      //alert(JSON.stringify(error));

      this.setState({
        isLoadingButton: false,
        error: 'ارتباط دستگاه خود را بررسی کنید.',
      });
    }
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <Image
          style={styles.logo}
          source={require('./../../assets/image/ic_logo_login.png')}
          resizeMode={'contain'}
        />
        <TextInput
          placeholder={'شماره همراه خود را وارد کنید'}
          placeholderTextColor={Colors.green}
          selectionColor={Colors.green}
          keyboardType={'numeric'}
          autoFocus={false}
          style={styles.input}
          maxLength={11}
          value={this.state.Mobile}
          accessible={false}
          onChangeText={(Mobile) => {
            
            if(Mobile.length==11){ 
            Keyboard.dismiss()
            }
            this.setState({
              Mobile,
              error: ''
            });
          }}
        />
        <Text style={styles.error}>
          {this.state.error}
        </Text>
        <View style={styles.checkboxContainer}>
        <TouchableOpacity
        style={{}}
         onPress={()=>
         this.linkUrl()
      }>
        <Text style={styles.label}>شرایط استفاده از خدمات و حریم خصوصی را می پذیرم</Text>
        </TouchableOpacity>
        <CheckBox
          value={this.state.isSelected}
          style={styles.checkbox}
          onValueChange={(value)=>this.setSelection(value)}
        />
        </View>
        <Button
          buttonText={this.state.isLoadingButton ? 'لطفا منتظر بمانید' : 'دریافت کد رهگیری'}
          isLoading={this.state.isLoadingButton}
          style={styles.button}
          onPress={this.onConfirm}
        />
        
      </ScrollView>
    );
  }

}

const styles = EStyleSheet.create({
  container: {
    backgroundColor: Colors.greenbackground,
  },
 
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 10,
    alignSelf:"center",
    backgroundColor:"#fff",
    borderRadius:5,
    borderColor: '#035e6f',
     borderWidth: 1
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    fontSize: normalize(11),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    margin: 8,
  },
  logo: {
    height: hp('40%'),
    width: wp('50%'),
    alignSelf: 'center',
    resizeMode: 'contain',
    marginTop:  hp('3%'),
    marginBottom:  hp('3%')
  },
  title: {
    fontSize: normalize(12),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    }, 
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: '#035e6f',
    marginBottom: 10
  },
  desc: {
    fontSize: normalize(11),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    color: Colors.white,
    textAlign: 'center'
  },
  separate: {
    marginBottom: 20,
    marginTop: 20,
    height: 0.8,
    width: wp('70%'),
    backgroundColor: Colors.textGray,
    alignSelf: 'center'
  },
  inputTitle: {
    width: wp('75%'),
    alignSelf: 'center',
    fontSize: normalize(11),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    color: Colors.textBlack,
    textAlign: 'right',
    marginBottom: 10
  },
  input: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    height: hp('6%'),
    width: wp('75%'),
     borderColor: '#035e6f',
     borderWidth: 1,
     borderRadius: 5,
    alignSelf: 'center',
    textAlign: 'center',
    backgroundColor: Colors.white,
    marginBottom:0,
  },
  error: {
    fontSize: normalize(11),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    color: Colors.error,
    textAlign: 'center'
  },
 
  button: {
    marginTop:0,
    width: wp('75%'),
    backgroundColor: Colors.buttondark,
    
    borderColor: '#035e6f',
     borderWidth: 1,
     borderRadius: 5,
    //borderRaduisWidth:0,
  }
});

const mapStateToProps = (state) => {
  return {
    userData: state.userData
  };
};

const mapActionToProps = {
  setMobileNumber
};

export default connect(mapStateToProps, mapActionToProps)(LoginScreen);
