import React, { Component } from 'react';
import { Alert, Image, Keyboard, ScrollView, StatusBar, Text, TextInput, TouchableOpacity, View } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import EStyleSheet from 'react-native-extended-stylesheet';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { setCodeNumber, setIsLogin } from '../../redux/actions';
import Repository from '../../repository/Repository';
import Colors from '../../utility/Colors';
import Utils from '../../utility/Utils';
import { Button } from './../../components';
import { hp, normalize, wp } from './../../responsive';

class LoginConfirmScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isConnected: true,
            isLoadingButton: false,
            smsToken: '',
            error: '',
            timeCount: 60,
        };

        this.onConfirm = this.onConfirm.bind(this);
        this.phoneRegisterNaturalPersonApi = this.phoneRegisterNaturalPersonApi.bind(this);
    }

    componentDidMount() {
       // NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
       // NetInfo.isConnected.fetch().done((isConnected) => { this.setState({ isConnected }); });

        this.startCounterDown();
    }

    shouldComponentUpdate(nextProps, nextState) {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount() {
       // NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
        if (this.timer) { clearInterval(this.timer); }
    }

    onConfirm = () => {
        Keyboard.dismiss();
        const {
            isConnected,
            smsToken
        } = this.state;
        this.setState({ error: '' });
        if (isConnected) {
            if (smsToken.length === 0) {
                this.setState({ error: 'کد فعال سازی خود را وارد کنید.' });
            } else if (smsToken.length < 5) {
                this.setState({ error: 'کد فعال سازی خود را کامل وارد کنید.' });
            } else {
                this.setState({
                    error: '',
                    isLoadingButton: true
                }, () => {
                    this.phoneLoginNaturalPersonApi();
                });
            }
        } else {
            Alert.alert(
                'اینترنت گوشی خود را بررسی کنید',
                '',
                [
                    { text: 'انصراف' },
                    { text: 'تلاش مجدد', onPress: this.onConfirm },
                    { text: 'باشه' },
                ],
                { cancelable: true }
            );
        }
    }

    async phoneLoginNaturalPersonApi() {
        const params = {
            Code: Utils.toEnglishDigits(this.state.smsToken),
            Mobile: this.props.userData.mobileNumber
        };
        try {
            const response = await Repository.PhoneLoginNaturalPersonApi(params);
            this.setState({ isLoadingButton: false });
            if (response.Success === 1) {
                this.props.setCodeNumber(Utils.toEnglishDigits(this.state.smsToken));
                if (response.ResultID === 100) {
                    this.props.navigation.navigate('register');
                } else if (response.ResultID === 200) {
                    this.props.setIsLogin();
                    this.props.navigation.dispatch(
                        StackActions.reset(
                            {
                                index: 0,
                                key: null,
                                actions: [NavigationActions.navigate({ routeName: 'dashboard' })]
                            }
                        )
                    );
                }
            }
            else {
                this.setState({ error: response.Text });
            }
        } catch (error) {
            this.setState({
                isLoadingButton: false,
                error: 'ارتباط دستگاه خود را بررسی کنید.',
            });
        }
    }

    async phoneRegisterNaturalPersonApi() {
        const deviceId = DeviceInfo.getUniqueID();
        const params = {
            Mobile: this.props.userData.mobileNumber,
            DeviceID: deviceId
        };
        try {
            const response = await Repository.PhoneRegisterNaturalPersonApi(params);
            this.setState({ isLoadingButton: false });
            if (response.Success === 1) {
                this.startCounterDown();
            }
            else {
                this.setState({ error: response.Text });
            }
        } catch (error) {
            this.setState({
                isLoadingButton: false,
                error: 'ارتباط دستگاه خود را بررسی کنید.',
            });
        }
    }

    startCounterDown() {
        if (!this.timer) {
            this.setState({ timeCount: 60 });
        }
        this.timer = setInterval(() => {
            if (this.state.timeCount < 1) {
                clearInterval(this.timer);
                this.timer = undefined;
                return;
            }
            this.setState((prevState) => {
                return {
                    timeCount: prevState.timeCount - 1
                };
            });
        }, 1000);
    }

    handleConnectionChange = (isConnected) => {
        this.setState({ isConnected });
    };

    render() {
        return (
            <ScrollView style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <Image
                    style={styles.logo}
                    source={require('./../../assets/image/ic_logo_login.png')}
                    resizeMode={'contain'}
                />
               
                <Text style={styles.desc}>
                    {'کد فعال سازی به شماره زیر ارسال خواهد شد'}
                </Text>
                <Text style={styles.desc}>
                    {`مدت زمان دریافت کد فعال سازی : ${this.state.timeCount}`}
                </Text>
                <View style={styles.inputTitleContainer}>
                    {this.state.timeCount === 0 &&
                        <TouchableOpacity
                            activeOpacity={0.6}
                            onPress={this.phoneRegisterNaturalPersonApi}
                        >
                            <Text style={styles.sendCodeAgain}>
                                {'ارسال دوباره کد...'}
                            </Text>
                        </TouchableOpacity>
                        ||
                        <View />
                    }
                   
                </View>
                <TextInput
                    placeholder={'کد رهگیری  دریافتی را وارد کنید'}
                    placeholderTextColor={Colors.green}
                    selectionColor={Colors.green}
                    keyboardType={'numeric'}
                    autoFocus={false}
                    style={styles.input}
                    maxLength={5}
                    value={this.state.smsToken}
                    onChangeText={(smsToken) => {
                        this.setState({
                            smsToken
                        });
                    }}
                />
                <Text style={styles.error}>
                    {this.state.error}
                </Text>
                <Button
                    buttonText={this.state.isLoadingButton ? 'لطفا منتظر بمانید' : 'ورود به نرم افزار'}
                    isLoading={this.state.isLoadingButton}
                    style={styles.button}
                    onPress={this.onConfirm}
                />
            </ScrollView>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        backgroundColor: Colors.greenbackground
    },
    logo: {
        height: hp('35%'),
        width: wp('40%'),
        alignSelf: 'center',
        resizeMode: 'contain',
        marginTop:  hp('2%'),
        marginBottom:  hp('2%')
      },
    title: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
        marginBottom: 10
    },
    desc: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '700',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.white,
        textAlign: 'center',
    },
    separate: {
        marginBottom: 20,
        marginTop: 20,
        height: 0.8,
        width: wp('70%'),
        backgroundColor: Colors.textGray,
        alignSelf: 'center'
    },
    inputTitleContainer: {
        flexDirection: 'row',
        width: wp('70%'),
        //justifyContent: 'space-between',
        //alignSelf: 'center',
        marginBottom: 10
    },
    inputTitle: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.textBlack,
        textAlign: 'center',
    },
    sendCodeAgain: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.red,
        textAlign: 'center',
    },
    input: {
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
        },
        '@media android': {
          fontFamily: '$IR_M',
        },
        height: hp('6%'),
        width: wp('70%'),
       
    borderColor: '#035e6f',
    borderWidth: 1,
    borderRadius: 5,
        alignSelf: 'center',
        textAlign: 'center',
        backgroundColor: Colors.white,
        marginBottom:0,
      },
    error: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.error,
        textAlign: 'center'
    },
    button: {
    marginTop:0,
    width: wp('70%'),
    backgroundColor: Colors.buttondark,
    
    borderColor: '#035e6f',
     borderWidth: 1,
     borderRadius: 5,
  }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {
    setIsLogin,
    setCodeNumber
};

export default connect(mapStateToProps, mapActionToProps)(LoginConfirmScreen);
