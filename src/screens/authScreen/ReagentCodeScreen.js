import React, { Component } from 'react';
import { Alert, Image, Keyboard, NetInfo, ScrollView, StatusBar, Text, TextInput, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { setMobileNumber } from '../../redux/actions';
import Repository from '../../repository/Repository';
import Colors from '../../utility/Colors';
import Utils from '../../utility/Utils';
import { Button } from './../../components';
import { hp, normalize, wp } from './../../responsive';

class ReagentCodeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isConnected: false,
            isLoadingButton: false,
            ReagentCode: ''
        };

        this.onConfirm = this.onConfirm.bind(this);
    }

    componentDidMount = () => {
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
        NetInfo.isConnected.fetch().done((isConnected) => { this.setState({ isConnected }); });
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount = () => {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
    }

    onConfirm = () => {
        Keyboard.dismiss();
        const {
            isConnected,
            ReagentCode
        } = this.state;
        this.setState({ error: '' });
        if (isConnected) {
            if (ReagentCode.length === 0) {
                this.setState({ error: 'شماره همراه خود را وارد کنید.' });
            } else {
                this.setState({
                    error: '',
                    isLoadingButton: true
                }, () => {
                    this.phoneAddReagentCodeHelperApi();
                });
            }
        } else {
            Alert.alert(
                'اینترنت گوشی خود را بررسی کنید',
                '',
                [
                    { text: 'انصراف' },
                    { text: 'تلاش مجدد', onPress: this.onConfirm },
                    { text: 'باشه' },
                ],
                { cancelable: true }
            );
        }
    }

    async phoneAddReagentCodeHelperApi() {
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber,
            ReagentCode: Utils.toEnglishDigits(this.state.ReagentCode)
        };
        try {
            const response = await Repository.PhoneAddReagentCodeHelperApi(params);
            this.setState({ isLoadingButton: false });
            if (response.Success === 1) {
                this.props.navigation.dispatch(
                    StackActions.reset(
                        {
                            index: 0,
                            key: null,
                            actions: [NavigationActions.navigate({ routeName: 'dashboard' })]
                        }
                    )
                );
            }
            else {
                this.setState({ error: response.Text });
            }
        } catch (error) {
            this.setState({
                isLoadingButton: false,
                error: 'ارتباط دستگاه خود را بررسی کنید.',
            });
        }
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <Image
                    style={styles.logo}
                    source={require('./../../assets/image/ic_logo_login.png')}
                    resizeMode={'contain'}
                />
                <Text style={styles.title}>
                    {'کد معرف'}
                </Text>
                <Text style={styles.desc}>
                    {'در کادر زیر معرف خود را وارد کنید'}
                </Text>
                <Text style={styles.desc}>
                    {'تا ایشان از امتیاز معرفی به دوستان بهره مند گردد'}
                </Text>
                <View style={styles.separate} />
                <Text style={styles.inputTitle}>
                    {'کد معرف خود را وارد کنید'}
                </Text>
                <TextInput
                    placeholder={'کد معرف خود را وارد کنید'}
                    placeholderTextColor={Colors.textGray}
                    selectionColor={Colors.textBlack}
                    keyboardType={'default'}
                    autoFocus={false}
                    style={styles.input}
                    value={this.state.ReagentCode}
                    onChangeText={(ReagentCode) => {
                        this.setState({
                            ReagentCode,
                            error: ''
                        });
                    }}
                />
                <Text style={styles.error}>
                    {this.state.error}
                </Text>
                <Button
                    buttonText={this.state.isLoadingButton ? 'لطفا منتظر بمانید' : 'ثبت و ادامه'}
                    isLoading={this.state.isLoadingButton}
                    style={styles.button}
                    onPress={this.onConfirm}
                />
            </ScrollView>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        backgroundColor: Colors.backgroundColor
    },
    logo: {
        height: wp('25%'),
        width: wp('25%'),
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 10
    },
    title: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
        marginBottom: 10
    },
    desc: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.textBlack,
        textAlign: 'center'
    },
    separate: {
        marginBottom: 20,
        marginTop: 20,
        height: 0.8,
        width: wp('70%'),
        backgroundColor: Colors.textGray,
        alignSelf: 'center'
    },
    inputTitle: {
        width: wp('70%'),
        alignSelf: 'center',
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.textBlack,
        textAlign: 'right',
        marginBottom: 10
    },
    input: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        height: hp('6%'),
        width: wp('70%'),
        borderColor: '#035e6f',
        borderWidth: 1,
        borderRadius: 20,
        alignSelf: 'center',
        textAlign: 'center',
    },
    error: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.error,
        textAlign: 'center'
    },
    button: {
        width: wp('70%')
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {
    setMobileNumber
};

export default connect(mapStateToProps, mapActionToProps)(ReagentCodeScreen);
