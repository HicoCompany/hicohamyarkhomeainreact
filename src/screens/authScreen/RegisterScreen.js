import { Icon, Picker } from 'native-base';
import React, { Component } from 'react';
import { Image, NetInfo, ScrollView, StatusBar, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { Portal, Dialog } from 'react-native-paper';
import DeviceInfo from 'react-native-device-info';
import EStyleSheet from 'react-native-extended-stylesheet';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import { setIsLogin } from '../../redux/actions';
import Repository from '../../repository/Repository';
import Colors from '../../utility/Colors';
import Utils from '../../utility/Utils';
import { Button } from './../../components';
import { hp, normalize, wp } from './../../responsive';
import { setMapId } from '../../redux/actions';

class RegisterScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isConnected: true,
            isLoadingPage: true,
            isLoadingButton: false,
            Name: '',
            Family: '',
            NationalCode: '',
            MainAreaID: -1,
            Email: '',
            ShabaNumber: '',
            AccountOwner: '',
            AccountNumber: '',
            BankID: -1,
            Adress: '',
            Sex: 1,
            bankList: [],
            areaList: [],
            birthYear: '',
            birthMonth: '',
            birthDate: '',
            errorName: '',
            errorFamily: '',
            errorBirth: '',
            errorEmail: '',
            errorBank: '',
            errorNationalCode: '',
            errorAdress: '',
            apiSuccess: false,
            apiError: false,
            apiErrorDesc: '',
            MainAreaIDMap:0
        };
        this.mounted = null;
    }
    
    componentDidMount = () => {
        this.mounted = true;
        this.setState({MainAreaIDMap:this.props.navigation.getParam('AreaId',0)});

        // NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);
        // NetInfo.isConnected.fetch().done((isConnected) => { this.setState({ isConnected }); });
        this.getApi();
    }
    componentDidUpdate(prevProps, prevState) {
        // this.setState({
        //     MainAreaIDMap: prevProps.navigation.state.params.AreaId,
        //     AreaName:prevProps.navigation.state.params.AreaName,
        //     MainAreaID:prevProps.navigation.state.params.AreaName
        // });
    }
    
    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount = () => {
        this.mounted = false;
        // NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
    }

    onConfirm = () => {
        const {
            Name,
            Family,
            NationalCode,
            Email,
            ShabaNumber,
            AccountOwner,
            AccountNumber,
            BankID,
            Adress,
            birthYear,
            birthMonth,
            birthDate,
        } = this.state;

        let error = false;

        if (Name.length === 0) {
            error = true;
            this.setState({
                errorName: 'لطفا نام خود را وارد کنید.'
            });
        }

        if (Family.length === 0) {
            error = true;
            this.setState({
                errorFamily: 'لطفا نام خانوادگی خود را وارد کنید.'
            });
        }

        if (NationalCode.length === 0) {
            error = true;
            this.setState({
                errorNationalCode: 'لطفا کد ملی خود را وارد کنید.'
            });
        }

        // if (Email.length === 0) {
        //     error = true;
        //     this.setState({
        //         errorEmail: 'لطفا ایمیل خود را وارد کنید.'
        //     });
        // }

        // if (ShabaNumber.length === 0 || AccountOwner.length === 0) {
        //     error = true;
        //     this.setState({
        //         errorBank: 'اطلاعات بانکی کامل وارد نشده است'
        //     });
        // }

        if (Adress.length === 0) {
            error = true;
            this.setState({
                errorAdress: 'لطفا آدرس خود را وارد کنید'
            });
        }

        if (birthYear.length < 4) {
            error = true;
            this.setState({
                errorBirth: 'سال تولد باید 4 رقم باشد'
            });
        }

        if (!birthYear.startsWith('13')) {
            error = true;
            this.setState({
                errorBirth: 'سال تولد درست وارد نشده است لطفا به صورت شمسی وارد کنید'
            });
        }

        if (parseInt(birthMonth) > 12 && birthMonth.length === 0) {
            error = true;
            this.setState({
                errorBirth: 'ماه تولد به درستی وارد نشده است'
            });
        }

        if (parseInt(birthDate) > 31 && birthDate.length === 0) {
            error = true;
            this.setState({
                errorBirth: 'روز تولد به درستی وارد نشده است'
            });
        }

        if (error) {
            return;
        }

        if (!error) {
            this.setState({
                isLoadingButton: true,
            }, () => {
                this.phoneUpdateNaturalPersonApi();
            });
        }
    }

    getApi = async () => {
        await this.getBankApi();
        await this.getAreaApi();
    }

    async getBankApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber
        };
        try {
            const response = await Repository.PhoneGetBankApi(params, headers);
            if (this.mounted) {
                if (response.Success === 1) {
                    const list = response.Result;
                    this.setState({
                        bankList: list,
                        BankID: list.length > 0 ? list[0].BankID : 1
                    });
                }
                else {
                    this.setState({
                        apiError: true,
                        apiErrorDesc: response.Text
                    });
                }
                this.setState({
                    isLoadingPage: false,
                });
            }
        } catch (error) {
            if (this.mounted) {
                this.setState({
                    isLoadingPage: false,
                    apiError: true,
                    apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
                });
            }
        }
    }

    async getAreaApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber
        };
        try {
            const response = await Repository.PhoneGetMainAreasApi(params, headers);
            if (this.mounted) {
                if (response.Success === 1) {
                    const list = response.Result;
                    this.setState({
                        areaList: list,
                        MainAreaID: list.length > 0 ? list[0].MainAreaCode : 1
                    });
                }
                else {
                    this.setState({
                        apiError: true,
                        apiErrorDesc: response.Text
                    });
                }
                this.setState({
                    isLoadingPage: false,
                });
            }
        } catch (error) {
            if (this.mounted) {
                this.setState({
                    isLoadingPage: false,
                    apiError: true,
                    apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
                });
            }
        }
    }

    async phoneUpdateNaturalPersonApi() {
        const deviceId = DeviceInfo.getUniqueID();
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber,
            Name: this.state.Name,
            Family: this.state.Family,
            NationalCode: this.state.NationalCode,
            MainAreaID: this.props.userData.mapid,
            DeviceID: deviceId,
            Email: this.state.Email,
            ShabaNumber: this.state.ShabaNumber,
            AccountOwner: this.state.AccountOwner,
            AccountNumber: '',
            BankID: this.state.BankID,
            Adress: this.state.Adress,
            Sex: this.state.Sex,
            BirthDay: `${this.state.birthYear}-${this.state.birthMonth}-${this.state.birthDate}`
        };
        try {
            const response = await Repository.PhoneUpdateNaturalPersonApi(params, headers);
            if (response.Success === 1) {
                this.props.setIsLogin();
                this.setState({
                    apiSuccess: true,
                });
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            if (this.mounted) {
                this.setState({
                    isLoadingButton: false,
                });
            }
        } catch (error) {
            if (this.mounted) {
                this.setState({
                    isLoadingButton: false,
                    apiError: true,
                    apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
                });
            }
        }
    }

    handleConnectionChange = (isConnected) => {
        this.setState({ isConnected });
    };

    renderBankPickerItem() {
        return this.state.bankList.map(bank =>
            <Picker.Item key={bank.BankID.toString()} label={`${bank.Title}`} value={bank.BankID} />
        );
    }

    renderAreaPickerItem() {
        return this.state.areaList.map(area =>
            <Picker.Item key={area.MainAreaCode.toString()} label={`${area.Title}`} value={area.MainAreaCode} />
        );
    }
    async PhoneGetDetermineDeliveryTimeApi(mapid) {
        this.props.setMapId(mapid);
    }
    render() {
        return (
            <ScrollView style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <Image
                    style={styles.logo}
                    source={require('./../../assets/image/ic_logo_login.png')}
                    resizeMode={'contain'}
                />
                <Text style={styles.title}>
                    {'تکمیل پروفایل'}
                </Text>
                <Text style={styles.desc}>
                    {'برای ارتباط بهتر با شما لطفا فیلدهای زیر را تکمیل کنید'}
                </Text>
                <View style={styles.separate} />
                <Text style={styles.inputTitle}>
                    {'نام'}
                </Text>
                <TextInput
                    placeholder={'نام خود را وارد کنید'}
                    placeholderTextColor={Colors.textGray}
                    selectionColor={Colors.textBlack}
                    keyboardType={'default'}
                    autoFocus={false}
                    style={styles.input}
                    value={this.state.Name}
                    onChangeText={(Name) => {
                        this.setState({
                            Name,
                            errorName: ''
                        });
                    }}
                />
                {this.state.errorName.length > 0 &&
                    <Text style={styles.error}>
                        {this.state.errorName}
                    </Text>
                }
                <Text style={styles.inputTitle}>
                    {'نام خانوادگی'}
                </Text>
                <TextInput
                    placeholder={'نام خانوادگی خود را وارد کنید'}
                    placeholderTextColor={Colors.textGray}
                    selectionColor={Colors.textBlack}
                    keyboardType={'default'}
                    autoFocus={false}
                    style={styles.input}
                    value={this.state.Family}
                    onChangeText={(Family) => {
                        this.setState({
                            Family,
                            errorFamily: ''
                        });
                    }}
                />
                {this.state.errorFamily.length > 0 &&
                    <Text style={styles.error}>
                        {this.state.errorFamily}
                    </Text>
                }
                <Text style={styles.inputTitle}>
                    {'کد ملی'}
                </Text>
                <TextInput
                    placeholder={'کد ملی خود را وارد کنید'}
                    placeholderTextColor={Colors.textGray}
                    selectionColor={Colors.textBlack}
                    keyboardType={'numeric'}
                    maxLength={11}
                    autoFocus={false}
                    style={styles.input}
                    value={this.state.NationalCode}
                    onChangeText={(NationalCode) => {
                        this.setState({
                            NationalCode,
                            errorNationalCode: ''
                        });
                    }}
                />
                {this.state.errorNationalCode.length > 0 &&
                    <Text style={styles.error}>
                        {this.state.errorNationalCode}
                    </Text>
                }
                <Text style={styles.inputTitle}>
                    {'شماره همراه'}
                </Text>
                <TextInput
                    placeholder={'شماره همراه خود را وارد کنید'}
                    placeholderTextColor={Colors.textGray}
                    selectionColor={Colors.textBlack}
                    keyboardType={'numeric'}
                    autoFocus={false}
                    style={styles.input}
                    maxLength={11}
                    value={this.props.userData.mobileNumber}
                    editable={false}
                />
                <Text style={styles.inputTitle}>
                    {'تاریخ تولد'}
                </Text>
                <View
                    style={{
                        flexDirection: 'row',
                        alignSelf: 'center'
                    }}
                >
                    <TextInput
                        placeholder={'سال : 1345'}
                        placeholderTextColor={Colors.textGray}
                        selectionColor={Colors.textBlack}
                        keyboardType={'numeric'}
                        autoFocus={false}
                        style={styles.inputBirthDay}
                        maxLength={4}
                        value={this.state.birthYear}
                        onChangeText={(birthYear) => {
                            this.setState({
                                birthYear,
                                errorBirth: ''
                            });
                        }}
                    />
                    <TextInput
                        placeholder={'ماه : 01'}
                        placeholderTextColor={Colors.textGray}
                        selectionColor={Colors.textBlack}
                        keyboardType={'numeric'}
                        autoFocus={false}
                        style={styles.inputBirthDay}
                        maxLength={2}
                        value={this.state.birthMonth}
                        onChangeText={(birthMonth) => {
                            this.setState({
                                birthMonth,
                                errorBirth: ''
                            });
                        }}
                    />
                    <TextInput
                        placeholder={'روز: 01'}
                        placeholderTextColor={Colors.textGray}
                        selectionColor={Colors.textBlack}
                        keyboardType={'numeric'}
                        autoFocus={false}
                        style={styles.inputBirthDay}
                        maxLength={2}
                        value={this.state.birthDate}
                        onChangeText={(birthDate) => {
                            this.setState({
                                birthDate,
                                errorBirth: ''
                            });
                        }}
                    />
                </View>
                {this.state.errorBirth.length > 0 &&
                    <Text style={styles.error}>
                        {this.state.errorBirth}
                    </Text>
                }
                <Text style={styles.inputTitle}>
                    {'ایمیل'}
                </Text>
                <TextInput
                    placeholder={'ایمیل خود را وارد کنید'}
                    placeholderTextColor={Colors.textGray}
                    selectionColor={Colors.textBlack}
                    keyboardType={'default'}
                    autoFocus={false}
                    style={styles.input}
                    value={this.state.Email}
                    onChangeText={(Email) => {
                        this.setState({
                            Email,
                            errorEmail: ''
                        });
                    }}
                />
                {this.state.errorEmail.length > 0 &&
                    <Text style={styles.error}>
                        {this.state.errorEmail}
                    </Text>
                }
                <Text style={styles.inputTitle}>
                    {'اطلاعات حساب بانکی'}
                </Text>
                <TextInput
                    placeholder={'شماره شبا'}
                    placeholderTextColor={Colors.textGray}
                    selectionColor={Colors.textBlack}
                    keyboardType={'default'}
                    autoFocus={false}
                    style={styles.input}
                    value={this.state.ShabaNumber}
                    onChangeText={(ShabaNumber) => {
                        this.setState({
                            ShabaNumber,
                            errorBank: ''
                        });
                    }}
                />
                <TextInput
                    placeholder={'نام صاحب حساب'}
                    placeholderTextColor={Colors.textGray}
                    selectionColor={Colors.textBlack}
                    keyboardType={'default'}
                    autoFocus={false}
                    style={styles.input}
                    value={this.state.AccountOwner}
                    onChangeText={(AccountOwner) => {
                        this.setState({
                            AccountOwner,
                            errorBank: ''
                        });
                    }}
                />
                <View style={styles.pickerContainer}>
                    <Picker
                        style={styles.pickerStyle}
                        textStyle={styles.pickerTextStyle}
                        iosHeader="بانک را انتخاب کنید"
                        headerBackButtonText="بستن"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        headerStyle={styles.pickerHeaderStyle}
                        headerTitleStyle={styles.pickerHeaderTextStyle}
                        headerBackButtonTextStyle={styles.pickerBackHeaderTextStyle}
                        itemStyle={styles.pickerItemStyle}
                        itemTextStyle={styles.pickerItemTextStyle}
                        selectedValue={this.state.BankID}
                        onValueChange={(BankID) => {
                            this.setState({ BankID });
                        }}
                        mode={'dropdown'}
                    >
                        {this.renderBankPickerItem()}
                    </Picker>
                </View>
                {this.state.errorBank.length > 0 &&
                    <Text style={styles.error}>
                        {this.state.errorBank}
                    </Text>
                }
                <Text style={styles.inputTitle}>
                    {'جنسیت'}
                </Text>
                <View
                    style={{
                        flexDirection: 'row',
                        alignSelf: 'center'
                    }}
                >
                    <TouchableOpacity
                        style={this.state.Sex === 2 ? styles.sexActive : styles.sexDeactive}
                        activeOpacity={0.6}
                        onPress={() => {
                            this.setState({
                                Sex: 2
                            });
                        }}
                    >
                        <Text style={this.state.Sex === 2 ? styles.sexActiveText : styles.sexDeactiveText}>
                            {'زن'}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={this.state.Sex === 1 ? styles.sexActive : styles.sexDeactive}
                        activeOpacity={0.6}
                        onPress={() => {
                            this.setState({
                                Sex: 1
                            });
                        }}
                    >
                        <Text style={this.state.Sex === 1 ? styles.sexActiveText : styles.sexDeactiveText}>
                            {'مرد'}
                        </Text>
                    </TouchableOpacity>
                </View>
                <Button 
                              buttonText={'انتخاب منطقه'}
                              onPress={() => {
                                  
                                    this.props.navigation.navigate("MapAreaScreen",{type:2});
                              }}
                              style={styles.dialogButton}
                          />
                <Text style={styles.inputTitle}>
                    {'منطقه'}
                </Text>
                <View style={styles.pickerContainer}>
                    <Picker
                        style={styles.pickerStyle}
                        textStyle={styles.pickerTextStyle}
                        iosHeader="منطقه را انتخاب کنید"
                        headerBackButtonText="بستن"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        headerStyle={styles.pickerHeaderStyle}
                        headerTitleStyle={styles.pickerHeaderTextStyle}
                        headerBackButtonTextStyle={styles.pickerBackHeaderTextStyle}
                        itemStyle={styles.pickerItemStyle}
                        itemTextStyle={styles.pickerItemTextStyle}
                        selectedValue={this.props.userData.mapid}
                        onValueChange={(MainAreaID) => {
                            this.setState({ MainAreaID },()=>this.PhoneGetDetermineDeliveryTimeApi(MainAreaID));
                        }}
                        mode={'dropdown'}
                    >
                        {this.renderAreaPickerItem()}
                    </Picker>
                </View>
                <Text style={styles.inputTitle}>
                    {'آدرس'}
                </Text>
                <TextInput
                    placeholder={'آدرس خود را وارد کنید'}
                    placeholderTextColor={Colors.textGray}
                    selectionColor={Colors.textBlack}
                    keyboardType={'default'}
                    autoFocus={false}
                    style={styles.inputAddress}
                    multiline
                    value={this.state.Adress}
                    onChangeText={(Adress) => {
                        this.setState({
                            Adress,
                            errorAdress: ''
                        });
                    }}
                />
                {this.state.errorAdress.length > 0 &&
                    <Text style={styles.error}>
                        {this.state.errorAdress}
                    </Text>
                }
                <Button
                    buttonText={this.state.isLoadingButton ? 'لطفا منتظر بمانید' : 'ورود به نرم افزار'}
                    isLoading={this.state.isLoadingButton}
                    style={styles.button}
                    onPress={() => {
                        this.onConfirm();
                    }}
                />
                <Portal>
                    <Dialog
                        visible={this.state.apiError}
                        dismissable={false}
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            alignSelf: 'center'
                        }}
                    >
                        <Dialog.Content>
                            <Text style={styles.dialogText}>
                                {this.state.apiErrorDesc}
                            </Text>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button
                                buttonText={'تایید'}
                                onPress={() => {
                                    this.setState({
                                        apiError: false
                                    });
                                }}
                                style={{ width: wp('40%') }}
                            />
                        </Dialog.Actions>
                    </Dialog>
                    <Dialog
                        visible={this.state.apiSuccess}
                        dismissable={false}
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            alignSelf: 'center'
                        }}
                    >
                        <Dialog.Content>
                            <Text style={styles.dialogText}>
                                {'ثبت نام شما با موفقیت انجام شد'}
                            </Text>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button
                                buttonText={'کد معرف دارم'}
                                onPress={() => {
                                    this.setState({
                                        apiSuccess: false
                                    }, () => {
                                        this.props.navigation.dispatch(
                                            StackActions.reset(
                                                {
                                                    index: 0,
                                                    key: null,
                                                    actions: [NavigationActions.navigate({ routeName: 'reagentCode' })]
                                                }
                                            )
                                        );
                                    });
                                }}
                                style={{ width: wp('30%') }}
                            />
                            <Button
                                buttonText={'برو به صفحه اصلی'}
                                onPress={() => {
                                    this.setState({
                                        apiSuccess: false
                                    }, () => {
                                        this.props.navigation.dispatch(
                                            StackActions.reset(
                                                {
                                                    index: 0,
                                                    key: null,
                                                    actions: [NavigationActions.navigate({ routeName: 'dashboard' })]
                                                }
                                            )
                                        );
                                    });
                                }}
                                style={{ width: wp('30%') }}
                            />
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </ScrollView>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    logo: {
        height: wp('25%'),
        width: wp('25%'),
        alignSelf: 'center',
        marginTop: 10,
        marginBottom: 10
    },
    title: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
        marginBottom: 10
    },
    desc: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.textBlack,
        textAlign: 'center',
    },
    separate: {
        marginBottom: 20,
        marginTop: 20,
        height: 0.8,
        width: wp('70%'),
        backgroundColor: Colors.textGray,
        alignSelf: 'center'
    },
    inputTitle: {
        width: wp('70%'),
        alignSelf: 'center',
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.textBlack,
        textAlign: 'right',
        marginBottom: 10
    },
    input: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 20,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: 10

    },
    inputBirthDay: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        height: hp('6%'),
        width: wp('21%'),
        marginLeft: 5,
        marginRight: 5,
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 20,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: 10
    },
    inputAddress: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        height: hp('15%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 20,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: 10,
        textAlignVertical: 'top'
    },
    error: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.error,
        textAlign: 'center'
    },

    sexActive: {
        backgroundColor: Colors.green,
        height: hp('6%'),
        width: wp('20%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 20,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5
    },
    sexDeactive: {
        height: hp('6%'),
        width: wp('20%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 20,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5
    },
    sexActiveText: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#ffffff',
    },
    sexDeactiveText: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
    },

    pickerContainer: {
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 20,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
    },
    pickerStyle: {
        // borderBottomWidth: 0.8,
        // borderColor: '#e2e2e2',
        width: wp('50%'),
        // justifyContent: 'center',
    },
    pickerHeaderStyle: {
        backgroundColor: Colors.purple,
    },
    pickerHeaderTextStyle: {
        width: wp('50%'),
        backgroundColor: 'transparent',
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: 'white',
        textAlign: 'right',
    },
    pickerBackHeaderTextStyle: {
        width: wp('50%'),
        backgroundColor: 'transparent',
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: 'white',
        textAlign: 'left',
    },
    pickerTextStyle: {
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: '#34495e',
        textAlign: 'center',
    },
    pickerItemStyle: {
        width: wp('90%'),
        backgroundColor: 'transparent'
    },
    pickerItemTextStyle: {
        fontSize: 16,
        width: wp('65%'),
        backgroundColor: 'transparent',
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: '#34495e',
        textAlign: 'right'
    },
    pickerSeparator: {
        '@media android': {
            height: 1,
            width: wp('90%'),
            backgroundColor: '#e2e2e2'
        }
    },

    button: {
        width: wp('70%')
    },

    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {
    setIsLogin,
    setMapId
};

export default connect(mapStateToProps, mapActionToProps)(RegisterScreen);
