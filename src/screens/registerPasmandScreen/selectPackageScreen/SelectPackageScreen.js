import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar,TextInput, ActivityIndicator, FlatList,BackHandler } from 'react-native';
import { Text,Card, CardItem,Container, Content, } from 'native-base';
import { Dialog, Portal } from 'react-native-paper';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import Repository from '../../../repository/Repository';
import { normalize,wp } from '../../../responsive';
import TabItem from '../../../components/TabItem';
import HeaderBack from '../../../components/HeaderBack';

import Utils from '../../../utility/Utils';
import { Button, Empty, Loading } from './../../../components';


class SelectPackageScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'ارائه پسماند'}
                    source={require('./../../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        };
    };
    constructor(props) {
      super(props);
      this.state = {
          isLoadingPage: true,
          list: [],
          refreshing: false,
          loadingData: false,
          apiSuccess:false,
          apiSuccessDesc:'',
          apiSuccessTitle:'',
          apiError: false,
          apiErrorDesc: ''
      };
  }

 
  componentDidMount = () => {
     this.getApi();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  } 
  shouldComponentUpdate = (nextProps, nextState) => {
      return Utils.shallowCompare(this, nextProps, nextState);
  }

  componentWillUnmount = () => {

  }

  getApi = async () => {
      await this.PhoneGetPackageApi();
  }

  async PhoneGetPackageApi() {
      const headers = null;
      const params = {
          Mobile: this.props.userData.mobileNumber,
          Code: this.props.userData.codeNumber
      };
      try {
          const response = await Repository.PhoneGetPackageApi(params, headers);
        
          if (response.ResultID === 100) {
              const list = response.Result;
              this.setState({
                  list
              });
          }
          else {
              this.setState({
                  apiError: true,
                  apiErrorDesc: response.Text
              });
          }
          this.setState({
              loadingData: false,
              refreshing: false,
              isLoadingPage: false,
          });
      } catch (error) {
          await this.setState({
              loadingData: false,
              refreshing: false,
              isLoadingPage: false,
              apiError: true,
              apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
          });
      }
  }
  async PhoneAddRequestPackage(PackageIdList) {
    this.setState({
        loadingData: true,
        refreshing: false,
        isLoadingPage: true,
       
    });
    const headers = null;
    const params = {
        Mobile: this.props.userData.mobileNumber,
        Code: this.props.userData.codeNumber,
        PackageId:PackageIdList
    };
    try {
        const response = await Repository.PhoneAddRequestPackageApi(params, headers);
        if (response.Success === 1) {
            this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
                apiSuccess:true,
                apiSuccessDesc:response.Text,
                apiSuccessTitle:response.Titel
            });
        }
        else {
            this.setState({
                apiError: true,
                apiErrorDesc: response.Text
            });
        }
        this.setState({ 
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
        });
    } catch (error) {
        await this.setState({
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
            apiError: true,
            apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
    }
}
  handleLoadMore = () => {
      // if (this.state.countList > 0) {
      //     this.setState({
      //         offset: this.state.offset + this.state.pageSize, loadingData: true
      //     }, async () => {
      //         await this.phoneGetNotificationForHelperApi();
      //     });
      // }
  }

  handleRefresh = () => {
      this.setState({ 
          refreshing: true, 
          isLoadingPage: true,
          list: [],
      }, () => {
          this.PhoneGetPackageApi();
      });
  }

  renderEmpty = () => {
      return this.state.isLoadingPage ? (
          <View style={{ flex: 1 }}>
              <Loading
                  message={'در حال دریافت اطلاعات'}
                  messageColor={Colors.green}
              />
          </View>
      ) : (
              <View style={{ flex: 1 }}>
                  <Empty />
              </View>
          );
  }

  renderFooter = () => {
      if (!this.state.loadingData) {
          return null;
      }
      return (<ActivityIndicator size={'small'} color={'black'} />);
  }

  renderItem(item) {
      const {
        ImagePath,
          Title,
          Price,
          PackageID
      } = item;
      return (
        <View style={{flex:1,flexDirection:'column',alignItems:'center'}} key={String(PackageID)} >
        <TouchableOpacity
        style={[styles.Btn]}
        onPress={async () => {
            await this.PhoneAddRequestPackage(PackageID)}}
      >
      <Card style={styles.BoxProduct}>
          <CardItem header style={{borderBottomWidth:0.5,borderBottomColor:'#50b3ae'}}>
             <Image source={{uri:ImagePath}}  style={styles.ImgProduct}/>
             </CardItem>
      <CardItem  style={{flex:1,flexDirection:'column'}}>
      <View style={{alignItems: 'center',}}>
      <Text style={styles.textProduct}>{Title}</Text>
     
      
      </View>
      </CardItem>
    </Card>
    </TouchableOpacity>
    </View>
      );
  }

  render() {
      return (
        <Container>
               
        <Content>
        <StatusBar
                 backgroundColor={Colors.statusBar}
                 barStyle="light-content"
             />
             <HeaderBack
                 onBackPress={() => this.props.navigation.goBack()}
                 headerTitle={'  ثبت پکیج '}
             />
             
              <FlatList
                  data={this.state.list}
                  renderItem={({ item }) => this.renderItem(item)}
                  ListEmptyComponent={() => this.renderEmpty()}
                  ListFooterComponent={() => this.renderFooter()}
                  numColumns={2}
                  keyExtractor={(item, index) => item.PackageID.toString()}
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.handleRefresh()}
                  onEndReached={() => this.handleLoadMore()}
                  onEndReachedThreshold={0.08}
                  contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
              />
           
              <Portal>
                  <Dialog
                      visible={this.state.apiError}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                              apiError: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.apiErrorDesc}
                          </Text>
                      </Dialog.Content>
                      <Dialog.Actions>
                          <Button 
                              buttonText={'تلاش مجدد'}
                              onPress={() => {
                                  this.setState({
                                      apiError: false,
                                      isLoadingPage: true,
                                      list: []
                                  }, () => {
                                      this.getApi();
                                  });
                              }}
                              style={styles.dialogButton}
                          />
                      </Dialog.Actions>
                  </Dialog>
                  <Dialog
                            visible={this.state.apiSuccess}
                            style={styles.dialogContainer}
                            onDismiss={() => {
                                this.setState({
                                    apiSuccess: false,
                                });
                            }}
                        >
                         <Dialog.Title style={styles.dialogTitleText}>
                            {this.state.apiSuccessTitle}
                         </Dialog.Title>
                            <Dialog.Content style={styles.dialogContent}>

                               
                                <Text style={styles.dialogText}>
                                    {this.state.apiSuccessDesc}
                                </Text>
                                <Button
                                    buttonText={'بستن'}
                                    onPress={() => {
                                        this.setState({
                                            apiSuccess: false,
                                        });
                                    }}
                                    style={styles.dialogButton}
                                />
                            </Dialog.Content>
                           
                        </Dialog>
              </Portal>
                
          </Content> 
      </Container>
      );
  }
    

}

export 
const styles = EStyleSheet.create({
    
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    },
  IRANSansNormal: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_M',
  },
   },
   IRANSansBold: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_B',
  },
   },
   header:{
    backgroundColor:'#50b3ae',
   },
   textHeader:{
    color:'white',
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_B',
  },
    fontSize: 14,
   },
    text:{
    color:'black',
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_B',
  },
    fontSize: 12,
     },
     BoxProduct:{
      width:160,
     // height:230,
      backgroundColor: 'white',
      alignItems: 'center',
      borderRadius: 5,
    },
    ImgProduct:{
      width:'85%',
      height: 80,
      resizeMode:'contain',
     
    },
    textGreen:{
      color: '#50b3ae',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
      fontSize: 12,
    },
    textRed:{
      color: '#e53935',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
      textDecorationLine: 'line-through',
      textDecorationStyle: 'solid',
      textDecorationColor: "#000"
    },
    textProduct: {
      color: 'black',
      fontSize: 10,
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
    },
 });



const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(SelectPackageScreen);
