import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar,ScrollView, ActivityIndicator, 
    FlatList ,Dimensions,BackHandler} from 'react-native';
import { Text,Card, CardItem,Container, Content, Icon,Footer, FooterTab} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import { normalize,wp } from '../../../responsive';
import TabItem from '../../../components/TabItem';
import HeaderBack from '../../../components/HeaderBack';
import { Dialog, Portal } from 'react-native-paper';
import Repository from '../../../repository/Repository';
import Utils from '../../../utility/Utils';
import { Button, Empty, Loading } from './../../../components';




class DeliveryTimeScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'زمان تحویل پسماند '}
                    source={require('./../../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        };
    };

    constructor(props) {
      super(props);
      this.state = {
          isLoadingPage: true,
          list: [],
          refreshing: false,
          loadingData: false,
          laps:[],
          apiError: false,
          apiErrorDesc: ''
      };
    }
    

componentDidMount = () => {
    var list1 = this.props.navigation.getParam('list',0);
    var list = list1.filter(el=>el.count>0)
    this.setState({list:list});
    this.getApi();

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  } 
shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
}

componentWillUnmount = () => {
   
}

getApi = async () => {
    await this.PhoneGetDetermineDeliveryTimeApi();
}

async PhoneGetDetermineDeliveryTimeApi() {
    const headers = null;
    const params = {
        Mobile: this.props.userData.mobileNumber,
        Code: this.props.userData.codeNumber
    };
    
    try {
      
        const response = await Repository.PhoneGetDetermineDeliveryTimeApi(params, headers);
      
        if (response.ResultID === 100) {
            const laps = response.Result;
            this.setState({
                laps
            });
        }
        else {
            this.setState({
                apiError: true,
                apiErrorDesc: response.Text
            });
        }
        this.setState({
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
        });
    } catch (error) {
        await this.setState({
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
            apiError: true,
            apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
    }
}
handleLoadMore = () => {
    // if (this.state.countList > 0) {
    //     this.setState({
    //         offset: this.state.offset + this.state.pageSize, loadingData: true
    //     }, async () => {
    //         await this.phoneGetNotificationForHelperApi();
    //     });
    // }
}

handleRefresh = () => {
    this.setState({ 
        refreshing: true, 
        isLoadingPage: true,
        list: [],
    }, () => {
        this.PhoneGetDetermineDeliveryTimeApi();
    });
}

renderEmpty = () => {
    return this.state.isLoadingPage ? (
        <View style={{ flex: 1 }}>
            <Loading
                message={'در حال دریافت اطلاعات'}
                messageColor={Colors.green}
            />
        </View>
    ) : (
            <View style={{ flex: 1 }}>
                <Empty />
            </View>
        );
}
async PhoneAddRequestDeliverWaste(SettringCollectID) {
    this.setState({
        ApiSuccsess: false,
        ApiSuccessDesc: ''
    });
    const headers = null;
    var list = this.state.list.map((item) => {
        
          return {WasteTypeID:item.WasteTypeID,
            WasteTypeCount:item.count};
        
      });
    //   this.props.navigation.navigate("DeliveryTimeScreen",{laps:this.state.laps})
    const params = {
        Mobile: this.props.userData.mobileNumber,
        Code: this.props.userData.codeNumber,
        SettringCollectID:SettringCollectID,
        DeliveredWasteList:list
    };
    // alert(JSON.stringify(params));

    try {
        const response = await Repository.PhoneAddRequestDeliverWasteApi(params, headers);
        if (response.Success === 1) {
            this.setState({
                ApiSuccsess: true,
                ApiSuccessDesc: response.Text
            });
           this.props.navigation.navigate("RequestDetailsScreen",{DeliveryWasteID:response.ResultID,apiSuccess:1});
            
        }
        else {
            this.setState({
                apiError: true,
                apiErrorDesc: response.Text
            });
        }
        this.setState({
            isLoadingPage: false,
        });
    } catch (error) {
        this.setState({
            isLoadingPage: false,
            apiError: true,
            apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
    }
}
renderFooter = () => {
    if (!this.state.loadingData) {
        return null;
    }
    return (<ActivityIndicator size={'small'} color={'black'} />);
}

renderItem(item) {
    const {
      DayStr,
      FromTime,
      SettringCollectID,
      ToTime
        
    } = item;
    return (
      <TouchableOpacity
      style={[styles.Btn]}
      onPress={() => {
        this.setState({
            apiError: false,
            isLoadingPage: true,
        }, 
         async () => {
           await  this.PhoneAddRequestDeliverWaste(SettringCollectID);
                });
    }}
    >
  <Card style={styles.BoxDaysWeek}>
    <View  style={[styles.TotalDate]}>
    <View style={[styles.Week]}>
    <Text style={styles.textDaysWeek}>{DayStr} </Text>
    </View>
    <View style={[styles.Date]}>
    <Text style={styles.textDaysWeek}>{FromTime} تا {ToTime} </Text>
    </View>
    </View>
  </Card>
  </TouchableOpacity>
    );
}

    render() {
        return (
            <Container>
              <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                        onBackPress={() => this.props.navigation.goBack()}
                        headerTitle={' انتخاب زمان تحویل '}
                    />
           <Content>
          <View style={[styles.Title]}>
               <View style={[styles.line]}>

               </View>
               <View>
                <Text style={[styles.textTitle]}>تعیین زمان مراجعه</Text>
            </View>
            <View>
               <Icon type="AntDesign" name="calendar" style={{color:'#50b3ae'}}/>
            </View>
          </View>
      <View style={{flex:1,flexDirection:'row',alignItems:'space-between',justifyContent:'space-between',paddingHorizontal:10,paddingTop:13}}>
           
           <FlatList
                  data={this.state.laps}
                  renderItem={({ item }) => this.renderItem(item)}
                  ListEmptyComponent={() => this.renderEmpty()}
                  ListFooterComponent={() => this.renderFooter()}
                  numColumns={2}
                  keyExtractor={(item, index) => item.SettringCollectID.toString()}
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.handleRefresh()}
                  onEndReached={() => this.handleLoadMore()}
                  onEndReachedThreshold={0.08}
                  contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
              />
          </View>
         
        </Content>
          <Footer style={{height:70,width:"100%"}}>
          <FooterTab  style={{backgroundColor:'#50b3ae'}}>
           <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <TouchableOpacity 
                style={[styles.BtnFooter]}
                onPress={() => this.props.navigation.navigate("DeliveredwasteScreen")}
                                     >
                <Icon type="AntDesign" name="doubleleft" style={{color:'white',paddingRight:25,fontSize:14,}}/>
               
                <Text style={[styles.textFooter,{color:'white',}]}>
                    بازگشت
                </Text>
            </TouchableOpacity>
            </View>
          </FooterTab>
        </Footer>
            <Portal>
                  <Dialog
                      visible={this.state.apiError}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                              apiError: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.apiErrorDesc}
                          </Text>
                      </Dialog.Content>
                      <Dialog.Actions>
                          <Button 
                              buttonText={'تلاش مجدد'}
                              onPress={() => {
                                  this.setState({
                                      apiError: false,
                                      isLoadingPage: true,
                                      list: []
                                  }, () => {
                                      this.getApi();
                                  });
                              }}
                              style={styles.dialogButton}
                          />
                      </Dialog.Actions>
                  </Dialog>
                  <Dialog
                      visible={this.state.ApiSuccess}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                            ApiSuccess: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.ApiTitleSuccess}
                          </Text>
                      </Dialog.Content>
                      
                  </Dialog>
              </Portal>
           
      </Container>
        );
    }

}
   


  export 
  const styles = EStyleSheet.create({
    IRANSansNormal: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     IRANSansBold: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     header:{
      backgroundColor:'#50b3ae',
     },
      Btn:{
        padding:8
        
      },
      text:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
       },
       textTitle:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
       },
       Title:{
        flex:1,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'space-around',
        paddingTop:15
       },
       line:{
          width:'50%',
         borderTopWidth:1.5,
         borderColor:'#50b3ae',
       },
       BoxDaysWeek: {
        width: Dimensions.get('window').width * 0.40,
        height: 100,
        backgroundColor: '#e0e0e0',
        // alignItems: 'space-between',
        borderRadius: 10,
        marginHorizontal: Dimensions.get('window').width * 0.01
      },
      textDaysWeek: {
        color: 'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
      },
      TotalDate:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'transparent',
        width:'100%',
      },
      Week:{
        alignItems: 'center',
        justifyContent:'center',
        flex:1
      },
      Date:{
        alignItems: 'center',
        justifyContent:'center',
        flex:1,
        borderTopWidth:1,
        borderColor:'white'
      },
      textBtnProduct:{
       color:'white',
       '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
      },
      BtnFooter:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center'
      },
      inputFooter:{
        backgroundColor:"white",
        width:200,
        borderTopRightRadius:5,
        borderBottomRightRadius:5,
        paddingRight:10,
      },
      textFooter:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
       },
       container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
      },
     
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    }
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(DeliveryTimeScreen);
