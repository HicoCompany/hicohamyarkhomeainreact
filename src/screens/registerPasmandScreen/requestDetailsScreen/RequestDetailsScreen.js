import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar,ScrollView,BackHandler } from 'react-native';
import { Text,Card, CardItem,Container, Content, Icon,Footer, FooterTab} from 'native-base';

import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import { normalize,wp } from '../../../responsive';
import { Dialog, Portal } from 'react-native-paper';

import TabItem from '../../../components/TabItem';
import HeaderBack from '../../../components/HeaderBack';
import Repository from '../../../repository/Repository';
import { Button, Empty, Loading } from './../../../components';

class RequestDetailsScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'نوع پسماند'}
                    source={require('./../../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        };
    };
    constructor(props) {
      super(props);
      this.state = {
          isLoadingPage: true,
          list: [],
          CreateDateStr:'',
          DeliverDateCreate:'',
          CreateTimeStr:'',
          DeliveryDateStr:'',
          DeliveryTimeStr:'',
          DeliveryWasteID:0,
          StatusStr:'',
          refreshing: false,
          loadingData: false,
          laps:[],
          apiError: false,
          apiErrorDesc: '',
          apiSuccess: true,
          apiSuccessDesc: 'درخواست شما با موفقیت ثبت شد'

      };
    }
  
  componentDidMount = async () => {
    var DeliveryWasteID = this.props.navigation.getParam('DeliveryWasteID',0);
    var apiSuccess = this.props.navigation.getParam('apiSuccess',0);
      if(apiSuccess==1){
        this.setState({apiSuccess:true});
      }
      else{
        this.setState({apiSuccess:false});

      }
    await this.setState({DeliveryWasteID:DeliveryWasteID});
     this.getApi();

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

  }
  
  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  } 
  
getApi = async () => {
  await this.PhoneGetDeliverWasteByDeliveryWasteIDApi();
}
async PhoneGetDeliverWasteByDeliveryWasteIDApi() {
  const headers = null;
  
  const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber,
      DeliveryWasteID:this.state.DeliveryWasteID
  };
 
  try {
    
      const response = await Repository.PhoneGetDeliverWasteByDeliveryWasteIDApi(params, headers);
    //alert(JSON.stringify(response));
      if (response.Success === 1) {
          const laps = response.Result.Result;
          this.setState({
            laps:laps,
            StatusStr:response.Result.StatusStr,
            CreateDateStr:response.Result.CreateDateStr,
            DeliverDateCreate:response.Result.DeliverDateCreate,
            CreateTimeStr:response.Result.CreateTimeStr,
            DeliveryDateStr:response.Result.DeliveryDateStr,
            DeliveryTimeStr:response.Result.DeliveryTimeStr,
            DeliveryWasteID:response.Result.DeliveryWasteID,
            Address:response.Result.Address2,
            MainAreaTitle:response.Result.MainAreaTitle2,
            DeliverDateCreate:response.Result.DeliverDateCreate
          });
      }
      else {
          this.setState({
              apiError: true,
              apiErrorDesc: response.Text
          });
      }
      this.setState({
          loadingData: false,
          refreshing: false,
          isLoadingPage: false,
      });
  } catch (error) {
      await this.setState({
          loadingData: false,
          refreshing: false,
          isLoadingPage: false,
          apiError: true,
          apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
  }
}

lapsList() {
  return this.state.laps.map((data) => {
    return (
    <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
       <Card style={[styles.CardSelect]}>
          <Text style={[styles.text]}>{data.Points} امتیاز - {data.HelperPrice} تومان</Text>
            <Text style={[styles.text]}> {data.WasteTypeCount} کیلوگرم </Text>
            <Text style={[styles.text]}>{data.WasteTypeStr}</Text>
        </Card>
   </View>
    )
  })
  
  }
    render() {
        return (
            <Container>
              <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                 <HeaderBack
                        onBackPress={() =>this.props.navigation.navigate("MainPageScreen")}
                        headerTitle={'مشاهده جزئیات سفارش '}
                    />
          <Content>
          <View style={[styles.Title]}>
            <View  style={{flex:9}}>
                <Text style={[styles.textTitle]}> نوع پسماند </Text>
            </View>
            <View style={{flex:1,flexDirection:'row',justifyContent:"flex-end",alignItems:'center',}}>
               <Icon type="Entypo" name="shopping-bag" style={{color:'#50b3ae'}}/>
            </View>
          </View>
          <View style={[styles.TotalCard]}>
          {this.lapsList()}
              
          <View style={[styles.Title]}>
            <View  style={{flex:9}}>
                <Text style={[styles.textTitle]}> زمان تحویل و آدرس</Text>
            </View>
            <View style={{flex:1,flexDirection:'row',justifyContent:"flex-end",alignItems:'center',}}>
               <Icon type="Entypo" name="shopping-bag" style={{color:'#50b3ae'}}/>
            </View>
          </View>
          <View style={[styles.TotalCard]}>
            <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
              <Card style={[styles.CardTime]}>
              <TouchableOpacity
                style={[styles.BtntTime]}
                onPress={() => this.props.navigation}
              >
            </TouchableOpacity>
            <View style={{flexDirection:'column',alignItems: 'center',justifyContent:'center'}}>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.DeliverDateCreate}</Text>
                <Text style={[styles.text]}>{this.state.DeliveryDateStr}</Text>
               </View>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:5}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.DeliveryTimeStr}</Text>
               </View>
           </View>
              </Card>
              </View>
              </View>
              <View style={[styles.TotalCard]}>
            <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
              <Card style={[styles.CardTime]}>
              <TouchableOpacity
                style={[styles.BtntTime]}
                onPress={() => this.props.navigation}
              >
            </TouchableOpacity>
           <View style={{flexDirection:'column',alignItems: 'center',justifyContent:'center'}}>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <Text style={[styles.text ,{paddingRight:8}]}>{this.state.MainAreaTitle}</Text>
               </View>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:5}}>
                <Text style={[styles.text,{paddingRight:8}]}>{this.state.Address}</Text>
               </View>
           </View>
           </Card>
              </View>
              </View>
             </View>
             <Portal>
                 
                  <Dialog
                            visible={this.state.apiSuccess}
                            style={styles.dialogContainer}
                            onDismiss={() => {
                                this.setState({
                                    apiSuccess: false,
                                });
                            }}
                        >
                         <Dialog.Title style={styles.dialogTitleText}>
                            {this.state.apiSuccessTitle}
                         </Dialog.Title>
                            <Dialog.Content style={styles.dialogContent}>

                               
                                <Text style={styles.dialogText}>
                                    {this.state.apiSuccessDesc}
                                </Text>
                                <Button
                                    buttonText={'تایید'}
                                    onPress={() => {
                                        this.setState({
                                            apiSuccess: false,
                                        });
                                    }}
                                    style={styles.dialogButton}
                                />
                            </Dialog.Content>
                           
                        </Dialog>
              </Portal>
        </Content>
        <Footer style={{height:70,width:"100%"}}>
          <FooterTab  style={{backgroundColor:'#50b3ae'}}>
           <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
                <TouchableOpacity 
                style={[styles.BtnFooter]}
                onPress={() => this.props.navigation.navigate("MainPageScreen")}
                                     >
                <Text style={[styles.textFooter,{color:'white',}]}>
                      بازگشت به صفحه اصلی
                </Text>
            </TouchableOpacity>
            </View>
          </FooterTab>
          </Footer>
      </Container>
        );
    }

}

  export 
  const styles = EStyleSheet.create({
        
    dialogContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
      padding:0
      
  },
  dialogContent:{
      margin:0
  },
  dialogTitleText:{
      width: '100%',
      borderBottomColor: Colors.green,
      borderBottomWidth: 2,
      color:Colors.green,
      '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
  },
  dialogText: {
      margin:0,
      fontSize: normalize(13),
      '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': { 
          fontFamily: '$IR_M',
      },
      textAlign: 'center',
      color: Colors.textBlack,
  },
  dialogButton: {
      width: wp('30%')
  },
    IRANSansNormal: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     IRANSansBold: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     header:{
      backgroundColor:'#50b3ae',
     },
      textHeader:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
       },
      text:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
       },
       textTitle:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
       },
       Title:{
        flex:1,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'flex-end',
        paddingTop:15,
        paddingHorizontal:8
       },
       TotalCard:{
        flex:1,
        flexDirection:'column',
        alignItems: 'center',
        justifyContent:'center',
        paddingHorizontal:10,
        paddingTop:10
      },
      CardSelect:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-around',
        borderRadius:5,
        padding:8,
        paddingVertical:12
      },
      IconGreen:{
        color:'#50b3ae', 
        fontSize: 20
      },
      IconBag:{
        color:'#50b3ae',
        fontSize:25
      },
      textFooter:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
       },
       BtntTime:{
           justifyContent:'center',
           alignItems:'center',
           
       },
       CardTime:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-between',
        borderRadius:5,
        paddingHorizontal:12,
        paddingVertical:10
      },
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(RequestDetailsScreen);
