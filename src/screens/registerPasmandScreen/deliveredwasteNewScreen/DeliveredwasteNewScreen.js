import React, { Component } from 'react';
import { Alert,View,Image,TouchableOpacity,StatusBar,ScrollView, ActivityIndicator, 
  FlatList,Dimensions,BackHandler,TextInput } from 'react-native';
import { Text,Card, CardItem,Container, Content, Icon} from 'native-base';
import { Dialog, Portal } from 'react-native-paper';

import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import Repository from '../../../repository/Repository';

import { normalize,wp } from '../../../responsive';
import TabItem from '../../../components/TabItem';
import HeaderBack from '../../../components/HeaderBack';

import Utils from '../../../utility/Utils';
import { Button, Empty, Loading } from './../../../components';

class DeliveredwasteNewScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'تحویل پسماند'}
                    source={require('./../../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        }; 
    };
    constructor(props) {
      super(props);
      this.state = {
          isLoadingPage: true,
          list: [],
          refreshing: false,
          loadingData: false,
          laps: [],
          SumTotal:0,
          apiError: false, 
          apiErrorDesc: '',
          ApiSuccess:false,
          ApiSuccessDesc:'',
          ApiDescription:false,
          ApiTitleDescription:'',
          
      };
    }
  componentDidMount = () => {
    this.getApi();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}
handleBackButton = () => {
 Alert.alert(
     'خارج شدن  از برنامه',
     'آیا با خروج از برنامه موافق هستید؟', [{
         text: 'نه',
         onPress: () => console.log('کنسل'),
         style: 'cancel'
     }, {
         text: 'بله',
         onPress: () => BackHandler.exitApp()
     }, ], {
         cancelable: false
     }
  )
  return true;
} 
shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
}

componentWillUnmount = () => {
  BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);

}

getApi = async () => {
    await this.PhoneGetWastTypeApi();
}

async PhoneGetWastTypeApi() {
    const headers = null;
    const params = {
        Mobile: this.props.userData.mobileNumber,
        Code: this.props.userData.codeNumber
    };
    
    try {
        const response = await Repository.PhoneGetWastTypeApi(params, headers);
        if (response.ResultID === 100) {
            const list = response.Result.map((x) => {
              return ({ Description: x.Description, HelperPrice: x.HelperPrice, ImagePath: x.ImagePath,
                Pictuer:x.Pictuer,Point:x.Point,Title:x.Title,WasteTypeID:x.WasteTypeID,Selected:0,count:0})
              });
            this.setState({
                list
            });
        }
        else {
            this.setState({
                apiError: true,
                apiErrorDesc: response.Text
            });
        }
        this.setState({
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
        });
    } catch (error) {
        await this.setState({
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
            apiError: true,
            apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
    }
}
handleLoadMore = () => {
}

handleRefresh = () => {
    // this.setState({ 
    //     refreshing: true, 
    //     isLoadingPage: true,
    //     list: [],
    // }, () => {
    //     this.PhoneGetWastTypeApi();
    // });
}

renderEmpty = () => {
    return this.state.isLoadingPage ? (
        <View style={{ flex: 1 }}>
            <Loading
                message={'در حال دریافت اطلاعات'}
                messageColor={Colors.green}
            />
        </View>
    ) : (
            <View style={{ flex: 1 }}>
                <Empty />
            </View>
        );
}

renderFooter = () => {
    if (!this.state.loadingData) {
        return null;
    }
    return (<ActivityIndicator size={'small'} color={'black'} />);
}
    PhoneGetWastTypeDeleted(WasteTypeID) {
      var list = this.state.list.map((item) => {
        if (item.WasteTypeID === this.state.list.filter(el=>el.WasteTypeID==WasteTypeID)[0].WasteTypeID) {
            item.count = 0;
            item.Selected=0;
          return item;
        } else {
          return item;
        }
      });
    this.setState({list:list});
    }
    PhoneGetWastTypePlus(WasteTypeID) {
        var list = this.state.list.map((item) => {
            if (item.WasteTypeID === this.state.list.filter(el=>el.WasteTypeID==WasteTypeID)[0].WasteTypeID) {
                item.count = item.count+1;
              return item;
            } else {
              return item;
            }
          });
        this.setState({list:list});
    }
    PhoneGetWastTypeCount(WasteTypeID,count) {
     
      var list = this.state.list.map((item) => {
          if (item.WasteTypeID === this.state.list.filter(el=>el.WasteTypeID==WasteTypeID)[0].WasteTypeID) {
              item.count = (Math.round(count * 100) / 100).toFixed(1);
            return item;
          } else {
            return item;
          }
        });
      this.setState({list:list});
  }
    PhoneGetWastTypeMinus(WasteTypeID) {
        if(this.state.list.filter(el=>el.WasteTypeID==WasteTypeID)[0].count>1){
        var list = this.state.list.map((item) => {
            if (item.WasteTypeID === this.state.list.filter(el=>el.WasteTypeID==WasteTypeID)[0].WasteTypeID) {
                item.count = item.count-1;
              return item;
            } else {
              return item;
            }
          });
        this.setState({list:list});
    }
    }
     PhoneGetWastTypeSelectedApi(WasteTypeID,Title,HelperPrice,Point,Selected) {
      var list = this.state.list.map((item) => {
        if (item.WasteTypeID === this.state.list.filter(el=>el.WasteTypeID==WasteTypeID)[0].WasteTypeID) {
            item.Selected=1;
          return item;
        } else {
          return item;
        }
      });
    this.setState({list:list});
    }
SubmitRequest(){
 // alert(JSON.stringify(this.state.list.filter(el=>el.Selected==1 && el.count<=0).length));
  if(this.state.list.filter(el=>el.Selected==1 && el.count<=0).length>0){
    this.setState({
      ErrorSubmit:true
    })
  }
  else{
    this.props.navigation.navigate("DeliveryTimeNewScreen",{list:this.state.list})
  }
}
    renderRequest(){
     if(this.state.list.filter(el=>el.count>0).length>0)
return(
  <TouchableOpacity 
  style={[styles.BtnSubmit1]}
  onPress={() => this.SubmitRequest()}
                       >
  <Icon type="AntDesign" name="doubleleft" style={{color:'white',fontSize:18,paddingRight:20}}/>
  <Text style={[styles.textFooter]}>
درخواست جمع آوری  و ادامه </Text>
</TouchableOpacity>
)
else
return(
<TouchableOpacity 
  style={[styles.BtnSubmit1]}
                       >
  <Icon type="AntDesign" name="doubleleft" style={{color:'white',fontSize:18,paddingRight:20}}/>
  <Text style={[styles.textFooter]}>
   تمامی وزن ها به کیلوگرم می باشد </Text>
</TouchableOpacity>
)
    }
renderItem(item) {
    const {
      ImagePath,
        Title,
        HelperPrice,
        Point,
        WasteTypeID,
        Description,
        Selected,
        Count
    } = item;
    if(Selected){
    return (
     
  <Card style={styles.BoxWasteSelected}>
    <View  style={[styles.TotalWaste]}>
   
    {/* <Icon type="Entypo" name="shopping-bag" style={[styles.IconBag]}/> */}

     <Image source={{uri:ImagePath}} style={styles.IconBag}/>
    <View style={[styles.Waste]}>
    <Text style={styles.textWaste}> {Title}</Text>
    </View>
    <View style={[styles.Waste]}>
       
        <View visible={!Selected} style={{flexDirection:'row',alignItems:'center'}}>
        <Icon type="AntDesign" name="close" 
        onPress={() => {
          this.setState({
              apiError: false,
              isLoadingPage: true,
          }, 
            () => {
               this.PhoneGetWastTypeDeleted(WasteTypeID);
                  });
      }}
      
      style={[styles.IconGreen,{color:'red'}]}/>
        <Text> کیلوگرم </Text>
        <TextInput 
        maxLength={5}
        placeholder={'وزن'}
        keyboardType={'numeric'}
        value={Count}
        onChangeText={(Count) => {
            this.PhoneGetWastTypeCount(WasteTypeID,Count);
                                }}
                            />
                            
                            </View>
    </View>
    </View>
  </Card>
  
    );
  }
  else{
    return (
     
      <Card style={styles.BoxWaste}>
        <View  style={[styles.TotalWaste]}>
       
        {/* <Icon type="Entypo" name="shopping-bag" style={[styles.IconBag]}/> */}
    
         <Image source={{uri:ImagePath}} style={styles.IconBag}/>
        <View style={[styles.Waste]}>
        <Text style={styles.textWaste}> {Title} </Text>
        </View>
        <View style={[styles.Waste]}>
            <View visible={Selected} style={{flexDirection:'row',alignItems:'center'}}>
            <Icon type="AntDesign" name="pluscircleo" 
              onPress={() => {
                this.setState({
                    apiError: false,
                    isLoadingPage: true,
                }, 
                  () => {
                     this.PhoneGetWastTypeSelectedApi(WasteTypeID,Title,HelperPrice,Point,1);
                        });
            }}
             style={[styles.IconBadge1]}/>
            </View>
            
        </View>
        </View>
      </Card>
      
        );
  }
}
ListSelected() {
  return this.state.list.filter(el=>el.Selected==1).map((data) => {
    return (
<Card style={styles.BoxWaste}>
  <View  style={[styles.TotalWaste]}>
 
  {/* <Icon type="Entypo" name="shopping-bag" style={[styles.IconBag]}/> */}

   <Image source={{uri:data.ImagePath}} style={styles.IconBag}/>

 
  <View style={[styles.Waste]}>
  <Text style={styles.textWaste}> {data.Title}</Text>
  </View>
  <View style={[styles.Waste]}>
 
      <View style={{flexDirection:'row',alignItems:'center'}}>
      <TextInput
     placeholder={'وزن مورد نظر را وارد نمایید'}
          keyboardType={'numeric'}
          value={data.count}
          onChangeText={(count) => {
            this.PhoneGetWastTypeCount(data.WasteTypeID,count);
                                }}
                            />
      <Icon type="AntDesign" name="close" 
        onPress={() => {
          this.setState({
              apiError: false,
              isLoadingPage: true,
          }, 
            () => {
               this.PhoneGetWastTypeSelectedApi(data.WasteTypeID,data.Title,data.HelperPrice,data.Point,0);
                  });
      }}
      
      style={[styles.IconGreen,{color:'red'}]}/>
      
      </View>
  </View>
  </View>
</Card>
  );
})
}

    render() {
        return (
            <Container>
              <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                  <HeaderBack
                        onBackPress={() => this.props.navigation.goBack()}
                        headerTitle={'تحویل پسماند'}
                    />
           <Content style={{flex: 1}}>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:10}}>
            {this.renderRequest()}
            </View>
            {/* {this.lapsList()} */}
          <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:10}}>
                <TouchableOpacity 
                style={[styles.BtnSubmit]}
                // onPress={() => this.props.navigation.navigate("DeliveryTimeScreen",{laps:this.state.laps})}
                                     >
                <Text style={[styles.textFooter]}>
                مجموع امتیاز این درخواست: {this.state.list.reduce((a, v) => a + (v.Point*v.count) , 0)}
                </Text>
            </TouchableOpacity>
            </View>
              {/* <View style={[styles.Title]}>
               <View style={[styles.line]}>
               </View>
            <View>
                <Text style={[styles.textTitle]}>پسماندهای انتخاب شده</Text>
            </View>
            <View>
               <Icon type="Entypo" name="shopping-bag" style={{color:'#50b3ae',fontSize:20}}/>
            </View>
          </View> */}
          {/* <ScrollView  style={{flex: 1}} horizontal={true} contentContainerStyle={{flexGrow: 1}} >
            <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingHorizontal:10,paddingTop:10}}>
            {this.ListSelected()}
            </View>
            </ScrollView> */}
          {/* <View style={[styles.Title]}>
               <View style={[styles.line]}>
               </View>
            <View>
                <Text style={[styles.textTitle]}>پسماندهای انتخاب نشده</Text>
            </View>
            <View>
               <Icon type="Entypo" name="shopping-bag" style={{color:'#50b3ae',fontSize:20}}/>
            </View>
          </View> */}
          <ScrollView style={{flex: 1}} >
      <View style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'center',paddingHorizontal:10,paddingTop:13}}>
           <FlatList
                  data={this.state.list}
                  renderItem={({ item }) => this.renderItem(item)}
                  ListEmptyComponent={() => this.renderEmpty()}
                  ListFooterComponent={() => this.renderFooter()}
                  numColumns={3}
                 // horizontal={true}
                  keyExtractor={(item, index) => item.WasteTypeID.toString()}
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.handleRefresh()}
                  onEndReached={() => this.handleLoadMore()}
                  onEndReachedThreshold={0.08}
                  contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
              />
         
         </View>
         </ScrollView>
          
         
          <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:10}}>
          
            <Portal>
                  <Dialog
                      visible={this.state.apiError}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                              apiError: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.apiErrorDesc}
                          </Text>
                      </Dialog.Content>
                      <Dialog.Actions>
                          <Button 
                              buttonText={'تلاش مجدد'}
                              onPress={() => {
                                  this.setState({
                                      apiError: false,
                                      isLoadingPage: true,
                                      list: []
                                  }, () => {
                                      this.getApi();
                                  });
                              }}
                              style={styles.dialogButton}
                          />
                      </Dialog.Actions>
                  </Dialog>
                  <Dialog
                      visible={this.state.ApiSuccess}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                            ApiSuccess: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.ApiTitleSuccess}
                          </Text>
                      </Dialog.Content>
                      
                  </Dialog>
                  <Dialog
                      visible={this.state.ErrorSubmit}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                            ErrorSubmit: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {"تمام آیتم های انتخابی باید دارای وزن باشند"}
                          </Text>
                      </Dialog.Content>
                      
                  </Dialog>
                  <Dialog
                      visible={this.state.ApiDescription}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                            ApiDescription: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.ApiTitleDescription}
                          </Text>
                      </Dialog.Content>
                      
                  </Dialog>
              </Portal>
            </View>
        </Content>
      </Container>
        );
    }

}

  export 
  const styles = EStyleSheet.create({
    IRANSansNormal: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     IRANSansBold: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     header:{
      backgroundColor:'#50b3ae',
     },
     
     textHeader:{
      color:'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
      text:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
      },
      ImgProduct:{
        width:120,
        height: 180,
        resizeMode:'contain',      
      },
       textTitle:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
       },
       Title:{
        flex:1,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'space-around',
        paddingTop:15
       },
       line:{
          width:'50%',
         borderTopWidth:1.5,
         borderColor:'#50b3ae',
       },
       BoxWaste: {
        width: Dimensions.get('window').width * 0.30,
        backgroundColor: 'white',
        alignItems: 'center',
        borderRadius: 10,
        paddingVertical: 1,
        marginHorizontal: Dimensions.get('window').width * 0.01
      },
      BoxWasteSelected: {
        width: Dimensions.get('window').width * 0.30,
        backgroundColor: '#dddddd',
        alignItems: 'center',
        borderRadius: 10,
        paddingVertical: 1,
        marginHorizontal: Dimensions.get('window').width * 0.01
      },
      
      textWaste: {
        alignItems: 'center',
        fontSize: 10,
        color: 'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '400',
          justifyContent:'center'
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
      },
      TotalWaste:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'transparent',
        width:'100%',
        justifyContent:'space-between',
        alignItems: 'center',
      },
      Waste:{
        alignItems: 'center',
        justifyContent:'center',
        flex:1,
        paddingBottom:1
      },
      IconGreen:{
        color:'#50b3ae', 
        fontSize: 20
      },
      IconBadge:{
        color:'#50b3ae', 
        fontSize: 28,
        paddingRight:10,
        marginRight:10
        
      },
      IconBadge1:{
        color:'#50b3ae', 
        fontSize: 20,
        //paddingRight:10,
        //marginRight:10
      },

      IconBag:{
        color:'#50b3ae',
        fontSize:25,
        width:'90%',
        height:50,
        resizeMode: 'contain'
      },
      textBtnProduct:{
       color:'white',
       '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
      },
      BtnSubmit:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor:'#50b3ae',
        borderRadius:10,
        padding:7,
        width:'60%'
      },
      BtnSubmit1:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor:'#50b3ae',
        borderRadius:10,
        padding:7,
        width:'90%'
      },
      textFooter:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
        
       },
       container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
      },
      TotalCard:{
        flex:1,
        flexDirection:'column',
        alignItems: 'center',
        justifyContent:'center',
        paddingHorizontal:10,
        paddingTop:10
      },
      CardSelect:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center',
        borderRadius:5,
        padding:8,
        paddingVertical:12
      },
      BtnFooter:{
        flex:1,
        flexDirection:'column',
        alignItems: 'center',
        justifyContent:'center',
      },
      IconFooter:{
        color:'white',
        fontSize:18,
      },
      
    dialogContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
      padding:0
      
  },
  dialogContent:{
      margin:0
  },
  dialogTitleText:{
      width: '100%',
      borderBottomColor: Colors.green,
      borderBottomWidth: 2,
      color:Colors.green,
      '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
  },
  dialogText: {
      margin:0,
      fontSize: normalize(13),
      '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': { 
          fontFamily: '$IR_M',
      },
      textAlign: 'center',
      color: Colors.textBlack,
  },
  dialogButton: {
      width: wp('30%')
  }
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(DeliveredwasteNewScreen);
