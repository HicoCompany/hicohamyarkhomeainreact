
import React, {Component} from 'react';
import {View, Image,TouchableOpacity,Text,Button,BackHandler,Alert} from 'react-native';
import MapboxGL from "@react-native-mapbox-gl/maps";
import {IS_ANDROID} from '../../../../utils';
MapboxGL.setAccessToken("pk.eyJ1IjoicGFyYWRhaW0iLCJhIjoiY2thMDFiNTI1MDRxdTNvdDZuajhndmNwNSJ9.aP4OPY40CCsfyGbgKOwyzw");
import { connect } from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';
import { setMapId } from '../../../redux/actions';
import { setMapAddress} from '../../../redux/actions';
import Geolocation from '@react-native-community/geolocation';

const coordinates = [
  [48.6731253,31.318703]];

  const AnnotationContent = ({title}) => (
    <View style={{ width: 60,
      backgroundColor: 'blue',
      width: 60,
      height: 60,
      borderRadius: 30,
      alignItems: 'center',
      justifyContent: 'center',}}>
        <Text
          style={{
            color: 'white',
            fontWeight: 'bold',
          }}>
          موقعیت 
        </Text>
    </View>
  );
class MapAreaScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
        tabBarIcon: ({ tintColor }) =>
            <TabItem
                name={'انتخاب مناطق '}
                source={require('./../../../assets/image/ic_residue.png')}
                color={tintColor}
            />,
    };
};
  constructor (props) {
    super(props);
    this.state = {
       url:"",
       api_key:"",
      coordinates: coordinates,
      isFetchingAndroidPermission: IS_ANDROID,
      isAndroidPermissionGranted: false,
      activeExample: -1,
      location:coordinates,
      AreaId:0,
      screenCoords: [],
      selectedGeoJSON: null,
      Address:'',
      Status:false
    }; 
  }
  async  componentDidMount(){
    Geolocation.getCurrentPosition(
      info =>{
      console.log(info)
      this.setState({
        Status:true
      })
    },error => {this.setState({
      Status:false
    })
  alert("لطفا gps را روشن نمایید")},
    {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000});
    if(IS_ANDROID) {
      const isGranted = await MapboxGL.requestAndroidLocationPermissions();
      this.setState({
        isAndroidPermissionGranted: isGranted,
        isFetchingAndroidPermission: false,
      }); 
    }
    if(!this.state.Status){
       
    }
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    if(this.props.navigation.getParam('type',0)==1){
    this.setState({list:this.props.navigation.getParam('list',0).filter(el=>el.count>0),type:this.props.navigation.getParam('type',0)});
  }
  else{
    this.setState({type:this.props.navigation.getParam('type',0)});
  }
  if(this.state.api_key==""){
    fetch("http://sdi.ahvaz.ir/geoapi/user/login/",{
      method: 'post',
      headers: {
          'Content-Type': 'application/json'
      },
      body: JSON.stringify({"appId":"mobilegis","password":"123456","username":"zistdoost"})
  }).then(response => response.json())
  .then(response => {
    this.setState({
      api_key:response.api_key,
       url:"http://sdi.ahvaz.ir/wms/?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=GeoTajak:boundary97_project_1203&STYLES=boundary97_project_12034199&authkey="+ response.api_key,
          });
  });
}
  //this.moveToCurrentLocation();
}
handleBackButton = () => {
  this.props.navigation.goBack();
  return true;
} 
async moveToCurrentLocation() {
  const  coords = Object.assign([], this.props.markers);
  let coords1 = await this._map.getCenter();
  coords.push(coords1);
  this._map.setCamera({
    centerCoordinate: coords,
    location:coords1,
    zoomLevel: 13
});
}
async onAnnotationSelected(activeIndex, feature) {
  const  coords = Object.assign([], this.props.markers);
  let coords1 = await this._map.getCenter();
  coords.push(coords1);
  this.setState({
    coordinates: coords,
    centerCoordinate:coords
  }); 
}
renderAnnotations1(){
  const isLoggedIn = this.state.url;
  if (isLoggedIn!='') {
  return (
    
      <MapboxGL.RasterSource  id="osm" tileSize={256}  url={this.state.url}>
        <MapboxGL.RasterLayer id="osm"/>
      </MapboxGL.RasterSource>
  );}
  return null;
}

 renderAnnotation () {
    this.onUserLocationUpdate1();
    return (
      <MapboxGL.PointAnnotation
        id="pt-ann"
        coordinate={this.state.coordinates[0]}
        >
        <AnnotationContent title={'this is a point annotation'} />
       
      </MapboxGL.PointAnnotation>
    );
  }
  SetStateAreaId (AreaId,AreaName) {
    this.props.setMapId(AreaId);
    this.props.setMapAddress(AreaName);
    if(this.state.type==1){
    this.props.navigation.navigate('DeliveryTimeNewScreen',{list:this.state.list,AreaId:AreaId,AreaName:AreaName});
  }
  else{
    this.props.navigation.navigate('register',{AreaId:AreaId,AreaName:AreaName});

  }
  }
  async onUserLocationUpdate1() {
    if(this.state.Status){
    const  coords = Object.assign([], this.props.markers);
  let coords1 = await this._map.getCenter();
  coords.push(coords1);
  this.setState({
    coordinates: coords,
    centerCoordinate:coords1,
  }); 
}
  }
  getBoundingBox(screenCoords) {
    const maxX = Math.max(screenCoords[0][0], screenCoords[1][0]);
    const minX = Math.min(screenCoords[0][0], screenCoords[1][0]);
    const maxY = Math.max(screenCoords[0][1], screenCoords[1][1]);
    const minY = Math.min(screenCoords[0][1], screenCoords[1][1]);
    return [maxY, maxX, minY, minX];
  }
async onUserLocationUpdate(location) {
  if(this.state.Status){
    const  coords = Object.assign([], this.props.markers);
    let coords1 = await this._map.getCenter();
    coords.push(coords1);
    this.setState({
      coordinates: coords,
      centerCoordinate:coords1,
    }); 
    var TILE_SIZE = 256;
    var siny = Math.sin(location.geometry.coordinates[1] * Math.PI / 180);
    siny = Math.min(Math.max(siny, -0.9999), 0.9999);
    var pointX = TILE_SIZE * (0.5 + location.geometry.coordinates[0] / 360);
    var pointY = TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI));
    //Get BBOX pixel in range 266 tileSize
    var r = 6378137 * Math.PI * 2;
    //console.log("r:"+parseInt( pointX));
    var x = (location.geometry.coordinates[0] / 360) * r;
    var sin = Math.sin(location.geometry.coordinates[1] * Math.PI / 180);
    var y = 0.25 * Math.log((1 + sin) / (1 - sin)) / Math.PI * r;
    var bbox = (x - 10) + "," + (y - 10) + "," + (x + 10) + "," + (y + 10);
      
    const {screenPointX, screenPointY} = location.properties;
    const screenCoords = Object.assign([], this.state.screenCoords);
    screenCoords.push([screenPointX-10, screenPointY-10]);
    screenCoords.push([screenPointX+10, screenPointY+10]);
    if (screenCoords.length === 2) {
    const featureCollection = await this._map.queryRenderedFeaturesInRect(
      this.getBoundingBox(screenCoords)
    );
    var WordAddress="";
    for(let i = 0; i < featureCollection.features.length; i++){
      if(featureCollection.features[i].properties.name.length>0){
      WordAddress = WordAddress + "-" +  featureCollection.features[i].properties.name;
      var WordAddress=WordAddress.split('-').filter(function(allItems,i,a){
        return i==a.indexOf(allItems);
    }).join('-');
    }
    }
    this.setState({
      screenCoords: [],
      selectedGeoJSON: featureCollection.features.length
        ? featureCollection
        : null,
        Address:WordAddress
    });
 
    alert(WordAddress);
 }
else{
  this.setState({screenCoords});
}

var address = this.state.Address;
var urltest = "http://sdi.ahvaz.ir/wms/?format=image/png&service=WMS&version=1.1.1&request=GetFeatureInfo&srs=EPSG:3857&transparent=true&width=256&height=256&layers=GeoTajak:boundary97_project_1203&STYLES=boundary97_project_12034199&authkey="+this.state.api_key+"&FEATURE_COUNT=50&Y="+parseInt( pointY)+"&X="+parseInt( pointX)+"&INFO_FORMAT=application/json&QUERY_LAYERS=GeoTajak:boundary97_project_1203&TILED=true&BBOX=" + bbox;
  var req = new XMLHttpRequest();
  let _this = this;
  req.responseType = 'json';
  req.open('GET', urltest, true);
  req.onload  = function() {
    var jsonResponse = req.response;
  if(jsonResponse!=null)
   if(jsonResponse.features.length>0){
    _this.SetStateAreaId(jsonResponse.features[0].properties.name,address);
  }
  else{
    alert("منطقه انتخابی خارج از اهواز می باشد")
  }
  };
  req.send(null);
}
    }
  render () {
    return (
      <MapboxGL.MapView
      ref={(c) => this._map = c}
      style={{flex: 1}}
      zoomLevel={13}
      onPress={feature => this.onUserLocationUpdate(feature)}
      // showUserLocation={true}
      userTrackingMode={1}
      userTrackingMode={MapboxGL.UserTrackingModes.Follow}
      >
         <MapboxGL.Camera
          zoomLevel={13}
          animationMode={'flyTo'}
          animationDuration={6000}
          followUserLocation
          followUserMode={'normal'}
        />
          
        {this.renderAnnotations1()}
        {this.renderAnnotation()}

        <View style={{width:30, backgroundColor:"transparent",position:'absolute',top:"50%",left:"50%",zIndex:10}}>
      <Button title="Button"></Button>
  </View>
      </MapboxGL.MapView>
    
  
      );
  }
}

export 
  const styles = EStyleSheet.create({
      BtnSubmit:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor:'#50b3ae',
        borderRadius:10,
        padding:7,
        width:'60%',
        top:'20%'
      },
     
      textFooter:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
        
       },
      
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {
  setMapId,
  setMapAddress
};
export default connect(mapStateToProps, mapActionToProps)(MapAreaScreen);
