import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar,ScrollView, ActivityIndicator, 
    FlatList ,Dimensions,BackHandler,TextInput} from 'react-native';
import { Text,Card, CardItem,Container, Content, Icon,Footer, FooterTab,Picker} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import { normalize,wp,hp } from '../../../responsive';
import TabItem from '../../../components/TabItem';
import HeaderBack from '../../../components/HeaderBack';
import { Dialog, Portal } from 'react-native-paper';
import Repository from '../../../repository/Repository';
import Utils from '../../../utility/Utils';
import { Button, Empty, Loading } from './../../../components';
import { setMapId } from '../../../redux/actions';
import { setMapAddress } from '../../../redux/actions';

class DeliveryTimeNewScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'زمان تحویل پسماند '}
                    source={require('./../../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        };
    };
constructor(props) {
      super(props);
      this.state = {
          isLoadingPage: true,
          list: [],
          areaList: [],
          refreshing: false,
          loadingData: false,
          laps:[],
          lapsTime:[],
          timeID:0,
          timeChangeID:0,
          WeekId:0,
          WeekDate:'',
          apiError: false,
          apiErrorDesc: '',
          address:'',
          MainAreaIDMap:0,
          MainAreaIDChangeMap:0,
          MainAreaIDSelected:0
      };
      this.mounted = null;
    }
    
componentDidMount = () => {
    this.setState({list:this.props.navigation.getParam('list',0).filter(el=>el.count>0),MainAreaIDMap:this.props.navigation.getParam('AreaId',0),address:this.props.navigation.getParam('AreaName',0)});
    this.props.setMapId('-1');
    this.props.setMapAddress('');
    // this.interval = setInterval(() => this.PhoneGetDetermineDeliveryTimeApi(this.props.userData.mapid,this.props.userData.address), 1000);
    this.getApi();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  handleBackButton = () => {
    this.props.navigation.goBack();
    clearInterval(this.interval);
    return true;
  } 
  
  componentWillUnmount() {
    clearInterval(this.interval);
  }
shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
}
getApi = async () => {
    this.mounted = true;
    await this.phoneGetInfoNaturalPersonApi();
    await this.getAreaApi();
}

renderAreaPickerItem() {
    return this.state.areaList.map(area =>
        <Picker.Item key={area.MainAreaCode.toString()} label={`${area.Title}`} value={area.MainAreaCode} />
    );
}
renderAreaPickerItemWeek() {
    return this.state.laps.map(week =>
        <Picker.Item key={week.DeliverDateStr.toString()} label={`${week.DayStr} - ${week.DeliverDateStr} `} value={week.SettringCollectID} />
    );
}
renderAreaPickerItemTime() {
    return this.state.lapsTime.map(week =>
        <Picker.Item key={week.SettringCollectID.toString()} label={`${week.FromTime} - ${week.ToTime} ` } value={week.SettringCollectID} />
    );
}
async getAreaApi() {
    const headers = null;
    const params = {
        Mobile: this.props.userData.mobileNumber,
        Code: this.props.userData.codeNumber
    };
    try {
        const response = await Repository.PhoneGetMainAreasApi(params, headers);
        if (this.mounted) {
            if (response.Success === 1) {
                const list = response.Result;
                this.setState({
                    areaList: list.filter(el=>el.MainAreaCode!=null),
                    MainAreaID: list.filter(el=>el.Selected==1).length > 0 ? list.filter(el=>el.Selected==1)[0].MainAreaCode : 1,
                    MainAreaIDSelected:list.filter(el=>el.Selected==1).length > 0 ? list.filter(el=>el.Selected==1)[0].MainAreaCode : 1
                });
              
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                isLoadingPage: false,
            });
        }
    } catch (error) {
        if (this.mounted) {
            this.setState({
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }
}
async phoneGetInfoNaturalPersonApi() {
    const headers = null;
    const params = {
        Mobile: this.props.userData.mobileNumber,
        Code: this.props.userData.codeNumber,
    };
    try {
        const response = await Repository.PhoneGetInfoNaturalPersonApi(params, headers);
        if (response.Success === 1) {
            this.setState({address:response.Result.Adress});
            this.props.setMapAddress(response.Result.Adress);
        }
        else {
            this.setState({
                apiError: true,
                apiErrorDesc: response.Text
            });
        }
        this.setState({
            isLoadingPage: false,
        });
    } catch (error) {
        await this.setState({
            isLoadingPage: false,
            apiError: true,
            apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
    }
}
async PhoneGetDetermineDeliveryTimeApi(mapid,mapAddress) {
    

    const headers = null;
    const params = {
        Mobile: this.props.userData.mobileNumber,
        Code: this.props.userData.codeNumber,
        MainAreaID:this.state.MainAreaIDChangeMap
    };
    try {
        const response = await Repository.PhoneGetMoreDetermineDeliveryTime(params, headers);
        if (response.ResultID === 100) {
           
            const laps1 = response.Result;
            this.setState({
                laps:response.Result
            });
        }
        else {
            this.setState({
                apiError: true,
                apiErrorDesc: response.Text,
                laps:[],
                lapsTime:[]
            });
        }
        this.setState({
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
        });
    } catch (error) {
        await this.setState({
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
            apiError: true,
            apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
    }
}


async PhoneGetTimeOfSettringCollectApi(SettringCollectID) {

    this.setState({
        lapsTime:[],
        WeekDate:this.state.laps.filter(el=>el.SettringCollectID==SettringCollectID)[0].DeliverDateStr
    });
    
    const headers = null;
    const params = {
        SettringCollectID: SettringCollectID
    };
    try {
        const response = await Repository.PhoneGetTimeOfSettringCollect(params, headers);
        if (response.ResultID === 100) {
            const lapsTime = response.Result;
            this.setState({
                lapsTime:lapsTime
            });
        }
        else {
            this.setState({
                apiError: true,
                apiErrorDesc: response.Text,
                lapsTime:[]
            });
        }
        this.setState({
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
        });
    } catch (error) {
        await this.setState({
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
            apiError: true,
            apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
    }
}
handleLoadMore = () => {
}

handleRefresh = () => {
    this.setState({ 
        refreshing: true, 
        isLoadingPage: true,
        laps: [],
    }, () => {
    });
}

renderEmpty = () => {
    return this.state.isLoadingPage ? (
        <View style={{ flex: 1 }}>
            <Loading
                message={'در حال دریافت اطلاعات'}
                messageColor={Colors.green}
            />
        </View>
    ) : (
            <View style={{ flex: 1 }}>
                <Empty />
            </View>
        );
}
async PhoneAddRequestDeliverWaste() {
    this.setState({
        ApiSuccsess: false,
        ApiSuccessDesc: ''
    });
    const headers = null;
    var list = this.state.list.map((item) => {
          return {WasteTypeID:item.WasteTypeID,
            WasteTypeCount:item.count};
      });
      
    const params = {
        Mobile: this.props.userData.mobileNumber,
        Code: this.props.userData.codeNumber,
        SettringCollectID:this.state.timeID,
        Adress2:this.state.address,
        MainAreaID2:this.state.MainAreaIDChangeMap,
        DeliveredWasteList:list,
        PDeliveryDate:this.state.WeekDate
    };
    try {
        // alert(JSON.stringify(params));
        const response = await Repository.PhoneAddRequestDeliverWasteApi(params, headers);
        if (response.Success === 1) {
            this.setState({
                ApiSuccsess: true,
                ApiSuccessDesc: response.Text
            });
           this.props.navigation.navigate("RequestDetailsScreen",{DeliveryWasteID:response.ResultID,apiSuccess:1});
        }
        else {
            this.setState({
                apiError: true,
                apiErrorDesc: response.Text
            });
        }
        this.setState({
            isLoadingPage: false,
        });
    } catch (error) {
        this.setState({
            isLoadingPage: false,
            apiError: true,
            apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
    }
}
lapsList() {
    return this.state.list.map((data) => {
      return (
        <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
           <Card style={[styles.CardSelect]}>
      <Text style={[styles.text]}>{data.Point} امتیاز - {data.HelperPrice} تومان</Text>
                <Text style={[styles.text]}> {data.count} کیلوگرم </Text>
                <Text style={[styles.text]}>{data.Title}</Text>
            </Card>
       </View>
      )
    })
}
renderFooter = () => {
    if (!this.state.loadingData) {
        return null;
    }
    return (<ActivityIndicator size={'small'} color={'black'} />);
}
renderItem1() {
    return this.state.laps.map((data) => {
        return (
            <TouchableOpacity
            style={[styles.Btn]}
            onPress={() => {
              this.setState({
                  apiError: false,
                  isLoadingPage: true,
              }, 
               async () => {
                 await  this.PhoneAddRequestDeliverWaste(data.SettringCollectID,data.DeliverDateStr);
                      });
          }}
          >
        <Card style={styles.BoxDaysWeek}>
          <View  style={[styles.TotalDate]}>
          <View style={[styles.Week]}>
          <Text style={styles.textDaysWeek}>{data.DayStr}-{data.DeliverDateStr} </Text>
          </View>
          <View style={[styles.Date]}>
          <Text style={styles.textDaysWeek}>{data.FromTime} تا {data.ToTime} </Text>
          </View>
          </View>
        </Card>
        </TouchableOpacity>
        )
      })
}
renderItem(item) {
    const {
      DayStr,
      FromTime,
      SettringCollectID,
      ToTime,
      DeliverDateStr
    } = item;
    return (
      <TouchableOpacity
      style={[styles.Btn]}
      onPress={() => {
        this.setState({
            apiError: false,
            isLoadingPage: true,
        }, 
         async () => {
           await  this.PhoneAddRequestDeliverWaste(SettringCollectID);
                });
    }}
    >
  <Card style={styles.BoxDaysWeek}>
    <View  style={[styles.TotalDate]}>
    <View style={[styles.Week]}>
    <Text style={styles.textDaysWeek}>{DayStr}-{DeliverDateStr} </Text>
    </View>
    <View style={[styles.Date]}>
    <Text style={styles.textDaysWeek}>{FromTime} تا {ToTime} </Text>
    </View>
    </View>
  </Card>
  </TouchableOpacity>
    );
}

    render() {
        return (
            <Container>
              <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                        onBackPress={() => this.props.navigation.goBack()}
                        headerTitle={' انتخاب زمان تحویل '}
                    />
           <Content>
           <View style={[styles.Title]}>
               <View style={[styles.line]}>

               </View>
               <View>
                <Text style={[styles.textTitle]}>آیتم های انتخابی</Text>
            </View>
            <View>
               <Icon type="AntDesign" name="calendar" style={{color:'#50b3ae'}}/>
            </View>
          </View>
          <View style={[styles.TotalCard]}>
          {this.lapsList()}
          <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:10}}>
          <TouchableOpacity 
  style={[styles.BtnSubmit1]}
                       >
                <Text style={[styles.textFooter]}>
                مجموع امتیاز این درخواست: {this.state.list.reduce((a, v) => a + (v.Point*v.count) , 0)}
                </Text>
                </TouchableOpacity>
            </View> 
            <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:10}}>
          <TouchableOpacity 
  style={[styles.BtnSubmit1]}
                       >
                <Text style={[styles.textFooter]}>
                مجموع مبلغ این درخواست : {this.state.list.reduce((a, v) => a + (v.HelperPrice*v.count) , 0)} تومان
                </Text>
                </TouchableOpacity>
            </View> 
          </View>
         
          <View style={[styles.Title]}>
               <View style={[styles.line]}>

               </View>
               <View>
        <Text style={[styles.textTitle]}>آدرس مکان جمع آوری</Text>
            </View>
            <View>
               <Icon type="AntDesign" name="calendar" style={{color:'#50b3ae'}}/>
            </View>
          </View>
          <View style={[styles.TotalCard]}>
          
          {/* <Button 
            buttonText={'انتخاب منطقه'}
            onPress={() => {
                this.props.navigation.navigate("MapAreaScreen",{list:this.state.list,type:1});
            }}
            style={styles.dialogButton}
                          /> */}
          <Text style={styles.inputTitle}>
                    {'منطقه مورد نظر را انتخاب نمایید'}
                   
                </Text>
                </View>
                <View style={styles.pickerContainer}>
              
                    <Picker
                        style={styles.pickerStyle}
                        textStyle={styles.pickerTextStyle}
                        iosHeader="منطقه را انتخاب کنید"
                        headerBackButtonText="بستن"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        headerStyle={styles.pickerHeaderStyle}
                        headerTitleStyle={styles.pickerHeaderTextStyle}
                        headerBackButtonTextStyle={styles.pickerBackHeaderTextStyle}
                        itemStyle={styles.pickerItemStyle}
                        itemTextStyle={styles.pickerItemTextStyle}
                        selectedValue={this.state.MainAreaIDChangeMap}
                        onValueChange={(MainAreaID) => {
                            this.setState({apiError:false, MainAreaIDChangeMap:MainAreaID },()=>this.PhoneGetDetermineDeliveryTimeApi(MainAreaID,''));
                        }}
                        mode={'dropdown'}
                    >
                        {this.renderAreaPickerItem()}
                    </Picker>
                </View>
          <View style={[styles.TotalCard]}>
          <Text style={styles.inputTitle}>
                    {'در صورت مغایرت آدرس را اصلاح نمایید'}
                </Text>
          <TextInput
           style={styles.input}
     placeholder={'ادرس خود را وارد نمایید'}
          value={this.state.address}
          onChangeText={(NewAddress) => {
           this.setState({
            address:NewAddress
           });
        }} />
          </View>
          <View style={[styles.Title]}>
               <View style={[styles.line]}>
               </View>
               <View>
                <Text style={[styles.textTitle]}>تعیین زمان مراجعه</Text>
            </View>
            <View>
               <Icon type="AntDesign" name="calendar" style={{color:'#50b3ae'}}/>
            </View>
          </View>
         
        
              <View style={{flex:1,flexDirection:'row',alignItems:'space-between',justifyContent:'space-between',paddingHorizontal:10,paddingTop:13}}>
              <Picker
                        style={styles.pickerStyle}
                        textStyle={styles.pickerTextStyle}
                        iosHeader="زمان مورد نظر را انتخاب کنید"
                        headerBackButtonText="بستن"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        headerStyle={styles.pickerHeaderStyle}
                        headerTitleStyle={styles.pickerHeaderTextStyle}
                        headerBackButtonTextStyle={styles.pickerBackHeaderTextStyle}
                        itemStyle={styles.pickerItemStyle}
                        itemTextStyle={styles.pickerItemTextStyle}
                        selectedValue={this.state.timeID}
                        onValueChange={(timeID) => {
                            this.setState({ 
                                timeID:timeID
                             });
                           
                        }}
                        mode={'dropdown'}
                    >
                      
                        {this.renderAreaPickerItemTime()}
                    </Picker>
           <Picker
                        style={styles.pickerStyle}
                        textStyle={styles.pickerTextStyle}
                        iosHeader="تاریخ مورد نظر را انتخاب کنید"
                        headerBackButtonText="بستن"
                        iosIcon={<Icon name="ios-arrow-down-outline" />}
                        headerStyle={styles.pickerHeaderStyle}
                        headerTitleStyle={styles.pickerHeaderTextStyle}
                        headerBackButtonTextStyle={styles.pickerBackHeaderTextStyle}
                        itemStyle={styles.pickerItemStyle}
                        itemTextStyle={styles.pickerItemTextStyle}
                        selectedValue={this.state.WeekId}
                        onValueChange={(WeekId) => {
                            this.setState({ WeekId:WeekId}
                                ,()=> 
                                 this.PhoneGetTimeOfSettringCollectApi(WeekId)
                                );
                                
                        }}
                        mode={'dropdown'}
                    >
                        {this.renderAreaPickerItemWeek()}
                    </Picker>
                   
                  
          </View>
          <View style={[styles.TotalCard]}>
          <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:10,paddingBottom:10}}>
          <TouchableOpacity 
          onPress={() => {
              this.PhoneAddRequestDeliverWaste();
                    }}
  style={[styles.BtnSubmit1]}
                       >
                <Text style={[styles.textFooter]}>
                ثبت درخواست
                </Text>
                </TouchableOpacity>
            </View> 
            
          </View>
        </Content>
            <Portal>
                  <Dialog
                      visible={this.state.ApiSuccess}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                            ApiSuccess: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.ApiTitleSuccess}
                          </Text>
                      </Dialog.Content>
                      
                  </Dialog>
                  <Dialog
                      visible={this.state.apiError}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                            apiError: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.apiErrorDesc}
                          </Text>
                      </Dialog.Content>
                      
                  </Dialog>
              </Portal>
           
      </Container>
        );
    }

}
  export 
  const styles = EStyleSheet.create({
    IRANSansNormal: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     IRANSansBold: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     header:{
      backgroundColor:'#50b3ae',
     },
      Btn:{
        padding:8
        
      },
      text:{
        color:'#000000',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
       },
       textTitle:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
       },
       Title:{
        flex:1,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'space-around',
        paddingTop:15
       },
       line:{
          width:'50%',
         borderTopWidth:1.5,
         borderColor:'#50b3ae',
       },
       BoxDaysWeek: {
        width: Dimensions.get('window').width * 0.40,
        height: 100,
        backgroundColor: '#e0e0e0',
        // alignItems: 'space-between',
        borderRadius: 10,
        marginHorizontal: Dimensions.get('window').width * 0.01
      },
      textDaysWeek: {
        color: 'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
      },
      TotalDate:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'transparent',
        width:'100%',
      },
      Week:{
        alignItems: 'center',
        justifyContent:'center',
        flex:1
      },
      Date:{
        alignItems: 'center',
        justifyContent:'center',
        flex:1,
        borderTopWidth:1,
        borderColor:'white'
      },
      textBtnProduct:{
       color:'white',
       '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
      },
      BtnFooter:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center'
      },
      inputFooter:{
        backgroundColor:"white",
        width:200,
        borderTopRightRadius:5,
        borderBottomRightRadius:5,
        paddingRight:10,
      },
      textFooter:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
       },
       container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
      },
     
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    },
    TotalCard:{
        flex:1,
        flexDirection:'column',
        alignItems: 'center',
        justifyContent:'center',
        paddingHorizontal:10,
        paddingTop:10
      },
      CardSelect:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-around',
        borderRadius:5,
        padding:8,
        paddingVertical:12
      },
      BtnSubmit1:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor:'#50b3ae',
        borderRadius:10,
        padding:7,
        width:'90%'
      },
      
    pickerContainer: {
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 20,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
    },
    pickerStyle: {
        // borderBottomWidth: 0.8,
        // borderColor: '#e2e2e2',
        width: wp('50%'),
        // justifyContent: 'center',
    },
    pickerHeaderStyle: {
        backgroundColor: Colors.purple,
    },
    pickerHeaderTextStyle: {
        width: wp('50%'),
        backgroundColor: 'transparent',
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: 'white',
        textAlign: 'right',
    },
    pickerBackHeaderTextStyle: {
        width: wp('50%'),
        backgroundColor: 'transparent',
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: 'white',
        textAlign: 'left',
    },
    pickerTextStyle: {
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: '#34495e',
        textAlign: 'center',
    },
    pickerItemStyle: {
        width: wp('90%'),
        backgroundColor: 'transparent'
    },
    pickerItemTextStyle: {
        fontSize: 16,
        width: wp('65%'),
        backgroundColor: 'transparent',
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: '#34495e',
        textAlign: 'right'
    },
    pickerSeparator: {
        '@media android': {
            height: 1,
            width: wp('90%'),
            backgroundColor: '#e2e2e2'
        }
    },
    inputTitle: {
        width: wp('70%'),
        alignSelf: 'center',
        fontSize: normalize(14),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.textBlack,
        textAlign: 'right',
        marginBottom: 10
    },
    input: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 20,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: 10

    }
   });
   const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};
const mapActionToProps = {
    setMapId,
    setMapAddress
};

export default connect(mapStateToProps, mapActionToProps)(DeliveryTimeNewScreen);
