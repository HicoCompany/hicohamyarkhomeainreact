import React, { Component } from 'react';
import { ActivityIndicator, ImageBackground, StatusBar, Text, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import Colors from '../../utility/Colors';
import { normalize } from './../../responsive';

class SplashScreen extends Component {

    componentDidMount() {
        this.getApi();
        setTimeout(() => {
            const {
                isLogin,
                isSeenIntroduce
            } = this.props.userData;
            if (isSeenIntroduce) {
                if (isLogin) {
                    this.props.navigation.dispatch(
                        StackActions.reset(
                            {
                                index: 0,
                                key: null,
                                actions: [NavigationActions.navigate({ routeName: 'dashboard' })]
                            }
                        )
                    );
                } else {
                    this.props.navigation.dispatch(
                        StackActions.reset(
                            {
                                index: 0,
                                key: null,
                                actions: [NavigationActions.navigate({ routeName: 'login' })]

                            }
                        )
                    );
                }
            }
            else {
                this.props.navigation.dispatch(
                    StackActions.reset(
                        {
                            index: 0,
                            key: null,
                            actions: [NavigationActions.navigate({ routeName: 'intro' })]
                        }
                    )
                );
            }
        }, 3000);
    }

    getApi = async () => {

    };

    render() {
        return (
            <ImageBackground
                style={styles.container}
                source={require('./../../assets/image/splash_background.jpg')}
            >
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <View style={styles.loadingContainer}>
                    <ActivityIndicator
                        size={'small'}
                        color={Colors.white}
                        style={styles.loading}
                    />
                    <Text style={styles.loadingText}>
                        {'لطفا صبر کنید'}
                    </Text>
                </View>
            </ImageBackground>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    loadingContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20
    },
    loading: {
        marginRight: 10
    },
    loadingText: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.white,

    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(SplashScreen);
