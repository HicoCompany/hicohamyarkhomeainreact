import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Image, TouchableOpacity, StatusBar, ScrollView, ActivityIndicator, FlatList, BackHandler } from 'react-native';
import Colors from '../../../utility/Colors';
import HeaderBack from '../../../components/HeaderBack';
import Repository from '../../../repository/Repository';
import {
  Container, Header, Content, Footer, FooterTab,
  Icon, Text, Card, CardItem, Badge
} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Dialog, Portal } from 'react-native-paper';
import TabItem from '../../../components/TabItem';
import Utils from '../../../utility/Utils';
import { hp, normalize, wp } from '../../../responsive';

import { Button, Empty, Loading } from './../../../components';

class RequestProductScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      tabBarIcon: ({ tintColor }) =>
        <TabItem
          name={'لیست درخواست کالا'}
          source={require('./../../../assets/image/ic_notice.png')}
          color={tintColor}
        />,
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoadingPage: true,
      list: [],
      listRequest: [],
      refreshing: false,
      loadingData: false,
      apiError: false,
      apiErrorDesc: ''
    };
  }

  componentDidMount = () => {
    this.getApi();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    this.props.navigation.goBack();
    return true;
  }

  getApi = async () => {
    await this.PhoneShowAllReciveWareApi();
    await this.PhoneShowAllWareDetailsHelerApi();
  }

  async PhoneShowAllReciveWareApi() {
    const headers = null;
    const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber
    };
    try {
      const response = await Repository.PhoneShowAllReciveWareApi(params, headers);
      if (response.ResultID === 100) {
          const list = response;
          this.setState({
              list
          });
      }
      else if(response.ResultID===-100){
        
        this.setState({
          apiError: true,
          apiErrorDesc: 'هیچ درخواستی ثبت نشده است'
      });
      }
      else {
          this.setState({
              apiError: true,
              apiErrorDesc: response.Text
          });
      }
      this.setState({
          loadingData: false,
          refreshing: false,
          isLoadingPage: false,
      });
    } catch (error) {
      await this.setState({
          loadingData: false,
          refreshing: false,
          isLoadingPage: false,
          apiError: true,
          apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
    }
    }
  async PhoneShowAllWareDetailsHelerApi() {
    const headers = null;
    const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber
    };

    try {

      const response = await Repository.PhoneShowAllWareDetailsHelerApi(params, headers);

      if (response.ResultID === 100) {
        const listRequest = response.Result;
        this.setState({
          listRequest
        });
      }
      else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text
        });
      }
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
      });
    } catch (error) {
      await this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
    }
  }
  async PhoneGetInfoCooperationApi() {
    const headers = null;
    const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber
    };

    try {

      const response = await Repository.PhoneGetInfoCooperationApi(params, headers);

      if (response.ResultID === 100) {
        const list = response.Result;

        this.setState({
          list
        });
        await this.PhoneGetDeliverWasteHelperApi();

      }
      else if (response.ResultID === -100) {

        this.setState({
          apiError: true,
          apiErrorDesc: 'هیچ درخواستی ثبت نشده است'
        });
      }
      else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text
        });
      }
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
      });
    } catch (error) {
      await this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
    }
  }
  async PhoneDeleteRequestWareHelperApi(RequestWareHelperId) {
    const headers = null;
    const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber,
      RequestWareHelperId: RequestWareHelperId
    };

    try {

      const response = await Repository.PhoneDeleteRequestWareHelperApi(params, headers);

      if (response.Success === 1) {
        this.setState({
          ApiSuccess: true,
          ApiTitleSuccess: response.Text
        });

        this.handleRefresh();
      }
      else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text
        });
      }
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
      });
    } catch (error) {
      await this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
    }
  }
  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  }
  handleLoadMore = () => {
    // if (this.state.countList > 0) {
    //     this.setState({
    //         offset: this.state.offset + this.state.pageSize, loadingData: true
    //     }, async () => {
    //         await this.phoneGetNotificationForHelperApi();
    //     });
    // }
  }

  handleRefresh = () => {
    this.setState({
      refreshing: true,
      isLoadingPage: true,

    }, () => {
      this.getApi();
    });
  }

  renderEmpty = () => {
    return this.state.isLoadingPage ? (
      <View style={{ flex: 1 }}>
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
        <View style={{ flex: 1 }}>
          <Empty />
        </View>
      );
  }

  renderFooter = () => {
    if (!this.state.loadingData) {
      return null;
    }
    return (<ActivityIndicator size={'small'} color={'black'} />);
  }


  renderCancel(Status, RequestWareHelperId) {
    if (Status == 0) {
      return (
        <TouchableOpacity
          style={[styles.BtnRed]}

          onPress={() => {
            this.setState({
              ApiSuccess: false,
              isLoadingPage: true,
            },
              async () => {
                await this.PhoneDeleteRequestWareHelperApi(RequestWareHelperId);
              });
          }}
        >
          <Text style={[styles.text, { color: 'white' }]}> لغو</Text>
        </TouchableOpacity>
      );
    }

  }

  renderEdit(Status, RequestWareHelperId) {
    if (Status == 0) {
      return (
        <TouchableOpacity
          style={[styles.BtnGreen]}
          onPress={() => this.props.navigation.navigate("EditProductScreen", { RequestWareHelperId: RequestWareHelperId })}
        >
          <Text style={[styles.text, { color: '#50b3ae' }]}> ویرایش</Text>
        </TouchableOpacity>
      );
    }

  }

  renderItem(item) {
    const {
      RequestDateStr,
      RequestWareHelperId,
      Status,
      StatusCode,
      WarePoint
    } = item;
    return (
      <Card>
        <CardItem>
          <View style={[styles.TotalContent]}>
            <View style={styles.SubContent}>
              <Text style={[styles.text, { paddingRight: 12 }]}>{RequestDateStr}</Text>
              <Text style={[styles.text, { paddingRight: 12 }]}>{"تاریخ درخواست: "}</Text>

              <Icon type="AntDesign" name="calendar" style={{ color: '#50b3ae', fontSize: 20 }} />
            </View>
            <View style={styles.SubContent}>
              <Text style={[styles.text, { paddingRight: 12 }]}>{WarePoint}</Text>
              <Text style={[styles.text, { paddingRight: 12 }]}>{"امتیاز جمع آوری: "}</Text>

              <Icon type="AntDesign" name="calendar" style={{ color: '#50b3ae', fontSize: 20 }} />
            </View>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', marginTop: 10 }}>
              <Text style={[styles.text]}> {Status} </Text>
              <Icon type="FontAwesome5" name="shuttle-van" style={{ color: '#50b3ae', fontSize: 20 }} />
            </View>
            <View style={[styles.SubContent]}>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                {this.renderCancel(StatusCode, RequestWareHelperId)}
                {this.renderEdit(StatusCode, RequestWareHelperId)}

                <TouchableOpacity
                  style={[styles.BtnGreen, { marginLeft: 3 }]}
                  onPress={() => this.props.navigation.navigate("SelectedProductScreen", { ResultID: RequestWareHelperId })}
                >
                  <Text style={[styles.text, { color: '#50b3ae' }]}> مشاهده</Text>
                </TouchableOpacity>

              </View>

            </View>
          </View>
        </CardItem>
      </Card>
    );
  }

  render() {
    return (
      <Container>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack
          onBackPress={() => this.props.navigation.navigate("DeliveryTypeScreen")}
          headerTitle={'لیست درخواست کالا'}
        />
        <Content>
          <View style={[styles.Center]}>
            <Card style={{ width: '90%' }}>
              <CardItem>
                <View style={[styles.TotalContent]}>
                  {/* <View style={styles.SubContent}>
                 <Text style={[styles.text,{paddingRight:12}]}>{this.state.list.PackageCount}</Text>
                 <Text style={[styles.text ,{paddingRight:12}]}>تعداد پکیج درخواست شده :</Text>
                 <Icon type="FontAwesome5" name="file-signature" style={{color:'#50b3ae', fontSize: 20}}/>
           </View> */}
                  <View style={styles.SubContent}>
                    <Text style={[styles.text, { paddingRight: 12 }]}>{this.state.list.RequestWareCount}</Text>
                    <Text style={[styles.text, { paddingRight: 12 }]}>  تعداد کالاهای درخواستی   :</Text>
                    <Icon type="Entypo" name="shopping-bag" style={{ color: '#50b3ae', fontSize: 20 }} />
                  </View>
                  <View style={styles.SubContent}>
                    <Text style={[styles.text, { paddingRight: 12 }]}>{this.state.list.ReceivedWareCount}</Text>
                    <Text style={[styles.text, { paddingRight: 12 }]}> تعداد کالا های تحویلی: </Text>
                    <Icon type="Entypo" name="shopping-bag" style={{ color: '#50b3ae', fontSize: 20 }} />
                  </View>
                  <View style={styles.SubContent}>
                    <Text style={[styles.text, { paddingRight: 12 }]}>{this.state.list.TotalPointNotRecived}</Text>
                    <Text style={[styles.text, { paddingRight: 12 }]}>  امتیاز کالاهای دریافت نشده: </Text>
                    <Icon type="Entypo" name="star-outlined" style={{ color: '#50b3ae', fontSize: 20 }} />
                  </View>
                  <View style={styles.SubContent}>
                    <Text style={[styles.text, { paddingRight: 12 }]}>{this.state.list.TotalPointRecived}</Text>
                    <Text style={[styles.text, { paddingRight: 12 }]}>  امتیاز کالاهای دریافت شده: </Text>
                    <Icon type="Entypo" name="star-outlined" style={{ color: '#50b3ae', fontSize: 20 }} />
                  </View>
                </View>
              </CardItem>
            </Card>
          </View>

          <View style={[styles.Center]}>

            <FlatList
              data={this.state.listRequest}
              renderItem={({ item }) => this.renderItem(item)}
              ListEmptyComponent={() => this.renderEmpty()}
              ListFooterComponent={() => this.renderFooter()}
              numColumns={1}
              keyExtractor={(item, index) => item.RequestWareHelperId.toString()}
              refreshing={this.state.refreshing}
              onRefresh={() => this.handleRefresh()}
              onEndReached={() => this.handleLoadMore()}
              onEndReachedThreshold={0.08}
              contentContainerStyle={this.state.listRequest.length > 0 ? {} : { flex: 1 }}
            />

          </View>
          <Portal>
            <Dialog
              visible={this.state.apiError}
              style={styles.dialogContainer}
              dismissable
              onDismiss={() => {
                this.setState({
                  apiError: false,
                });
              }}
            >
              <Dialog.Content>
                <Text style={styles.dialogText}>
                  {this.state.apiErrorDesc}
                </Text>
              </Dialog.Content>
              <Dialog.Actions>
                <Button
                  buttonText={'تلاش مجدد'}
                  onPress={() => {
                    this.setState({
                      apiError: false,
                      isLoadingPage: true,
                      list: []
                    }, () => {
                      this.getApi();
                    });
                  }}
                  style={styles.dialogButton}
                />
                <Button
                  buttonText={'کنسل'}
                  onPress={() => {
                    this.setState({
                      apiError: false,
                    });
                  }}
                  style={styles.dialogButton}
                />
              </Dialog.Actions>
            </Dialog>
            <Dialog
              visible={this.state.ApiSuccess}
              style={styles.dialogContainer}
              dismissable
              onDismiss={() => {
                this.setState({
                  ApiSuccess: false,
                });
              }}
            >
              <Dialog.Content>
                <Text style={styles.dialogText}>
                  {this.state.ApiTitleSuccess}
                </Text>
              </Dialog.Content>

            </Dialog>
          </Portal>
        </Content>
      </Container>
    );
  }

}


const styles = EStyleSheet.create({
  IRANSansNormal: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
  },
  IRANSansBold: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
  },
  header: {
    backgroundColor: '#50b3ae',
  },
  textHeader: {
    color: 'white',
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
  },
  text: {
    color: 'black',
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
  },
  TotalContent: {
    width: "100%",
    flexDirection: 'column',
  },
  SubContent: {
    width: "100%",
    justifyContent: 'flex-end',
    paddingVertical: 10,
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: "#f0f0f0",
  },
  Center: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10
  },
  BtnGreen: {
    borderWidth: 1,
    borderColor: '#50b3ae',
    borderRadius: 5,
    padding: 4,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  BtnRed: {
    borderRadius: 5,
    padding: 4,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    flex: 1,
    marginRight: 5
  },
  BtnBlue: {
    borderRadius: 5,
    padding: 4,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0099e5',
    flex: 1,
    marginRight: 5
  },

  dialogContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 0

  },
  dialogContent: {
    margin: 0
  },
  dialogTitleText: {
    width: '100%',
    borderBottomColor: Colors.green,
    borderBottomWidth: 2,
    color: Colors.green,
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_B',
    },
  },
  dialogText: {
    margin: 0,
    fontSize: normalize(13),
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
    },
    '@media android': {
      fontFamily: '$IR_M',
    },
    textAlign: 'center',
    color: Colors.textBlack,
  },
  dialogButton: {
    width: wp('30%')
  }

});

const mapStateToProps = (state) => {
  return {
    userData: state.userData
  };
};

const mapActionToProps = {

};
export default connect(mapStateToProps, mapActionToProps)(RequestProductScreen);
