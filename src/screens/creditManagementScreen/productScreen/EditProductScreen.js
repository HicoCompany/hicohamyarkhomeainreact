import React, { Component } from 'react';
import {
  Alert, View, Image, TouchableOpacity, StatusBar, ScrollView, ActivityIndicator,
  FlatList, Dimensions, BackHandler, TextInput
} from 'react-native';
import { Text, Card, CardItem, Container, Content, Icon, Picker } from 'native-base';
import { Dialog, Portal } from 'react-native-paper';
import { normalize, wp, hp } from '../../../responsive';

import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import Repository from '../../../repository/Repository';

import TabItem from '../../../components/TabItem';
import HeaderBack from '../../../components/HeaderBack';

import Utils from '../../../utility/Utils';
import { Button, Empty, Loading } from './../../../components';


class EditProductScreen extends Component {


  static navigationOptions = ({ navigation }) => {
    return {
      tabBarIcon: ({ tintColor }) =>
        <TabItem
          name={' ویرایش کالای تشویقی'}
          source={require('./../../../assets/image/ic_residue.png')}
          color={tintColor}
        />,
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoadingPage: true,
      list: [],
      ListSelected: [],
      listPoint: [],
      areaList: [],
      refreshing: false,
      loadingData: false,
      laps: [],
      SumTotal: 0,
      apiError: false,
      apiErrorDesc: '',
      ApiSuccess: false,
      ApiSuccessDesc: '',
      ApiDescription: false,
      ApiTitleDescription: '',
      MainAreaIDSelected: '-1'
    };
    this.mounted = null;
  }
  componentDidMount = () => {
    this.mounted = true;
    this.setState({ RequestWareHelperId: this.props.navigation.getParam('RequestWareHelperId', 0) })
    this.getApi();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  async getAreaApi() {
    const headers = null;
    const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber
    };
    try {
      const response = await Repository.PhoneGetAllBoothesApi(params, headers);
      if (this.mounted) {
        if (response.Success === 1) {
          const list = response.Result;
          this.setState({
            areaList: list
          });

        }
        else {
          this.setState({
            apiError: true,
            apiErrorDesc: response.Text
          });
        }
        this.setState({
          isLoadingPage: false,
        });
      }
    } catch (error) {
      if (this.mounted) {
        this.setState({
          isLoadingPage: false,
          apiError: true,
          apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
      }
    }
  }
  renderAreaPickerItem() {
    return this.state.areaList.map(area =>
      <Picker.Item key={area.BoothId.toString()} label={`${area.Title}`} value={area.BoothId} />
    );
  }
  handleBackButton = () => {
    Alert.alert(
      'خارج شدن  از برنامه',
      'آیا با خروج از برنامه موافق هستید؟', [{
        text: 'نه',
        onPress: () => console.log('کنسل'),
        style: 'cancel'
      }, {
        text: 'بله',
        onPress: () => BackHandler.exitApp()
      },], {
      cancelable: false
    }
    )
    return true;
  }
  shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
  }

  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);

  }

  getApi = async () => {
    await this.PhoneShowWareDetailsHelerApi();

  }

  async PhoneShowWareDetailsHelerApi() {
    const headers = null;
    const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber,
      RequestWareHelperId: this.props.navigation.getParam('RequestWareHelperId', 0)
    };
    try {
      const response = await Repository.PhoneShowRequestWareDetialApi(params, headers);
      if (response.Success === 1) {

        this.setState({
          ListSelected: response.Result.Result,
          Address: response.Result.Address,
          Status: response.Result.Status,
          RequestDateStr: response.Result.RequestDateStr,
          BoothWorkTime: response.Result.BoothWorkTime,
          BoothTitle: response.Result.BoothTitle,
          MainAreaIDSelected: response.Result.BoothId
        },
          () => {
            this.getAreaApi();
            this.PhoneGetInfoCooperationApi()
          });

      }
      else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text
        });
      }
      this.setState({
        isLoadingPage: false,
      });
    } catch (error) {
      await this.setState({
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
    }
  }
  async PhoneGetInfoCooperationApi() {
    const headers = null;
    const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber
    };

    try {
      const response = await Repository.PhoneGetInfoCooperationApi(params, headers);
      if (response.ResultID === 100) {
        await this.PhoneGetWastTypeApi(0);
        const listPoint = response.Result;
        this.setState({
          listPoint
        });

      }
      else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text
        });
      }
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
      });
    } catch (error) {
      await this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
    }
  }
  async PhoneGetWastTypeApi(change) {
    if (change == 1) {
      this.setState({ list: [],Address:"آدرس غرفه : "+this.state.areaList.filter(el=>el.BoothId==this.state.MainAreaIDSelected)[0].Address });

    }
    const headers = null;
    const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber,
      BoothId: this.state.MainAreaIDSelected
    };

    try {
      const response = await Repository.PhoneGetWareOfBoothApi(params, headers);
      if (response.ResultID === 100) {
        const list = response.Result.map((x) => {
          console.log(this.state.ListSelected.filter(el => el.WareId == x.WareId))
          if (this.state.ListSelected.filter(el => el.WareId == x.WareId).length > 0) {
            return ({
              ImagePath: x.ImagePath, Price: x.Price, Title: x.Title, WarePoint: x.WarePoint,
              WareId: x.WareId, Selected: 1, count: this.state.ListSelected.filter(el => el.WareId == x.WareId)[0].Count
            })

          }
          else {
            return ({
              ImagePath: x.ImagePath, Price: x.Price, Title: x.Title, WarePoint: x.WarePoint,
              WareId: x.WareId, Selected: 0, count: 0
            })
          }
        });
        this.setState({
          list
        });
      }
      else {
        const list = [];
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text,
          list: list
        });
      }
      this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
      });
    } catch (error) {
      await this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
    }
  }
  handleLoadMore = () => {
  }

  handleRefresh = () => {
    // this.setState({ 
    //     refreshing: true, 
    //     isLoadingPage: true,
    //     list: [],
    // }, () => {
    //     this.PhoneGetWastTypeApi();
    // });
  }

  renderEmpty = () => {
    return this.state.isLoadingPage ? (
      <View style={{ flex: 1 }}>
        <Loading
          message={'در حال دریافت اطلاعات'}
          messageColor={Colors.green}
        />
      </View>
    ) : (
        <View style={{ flex: 1 }}>
          <Empty />
        </View>
      );
  }

  renderFooter = () => {
    if (!this.state.loadingData) {
      return null;
    }
    return (<ActivityIndicator size={'small'} color={'black'} />);
  }
  PhoneGetWastTypeDeleted(WareId) {
    var list = this.state.list.map((item) => {
      if (item.WareId === this.state.list.filter(el => el.WareId == WareId)[0].WareId) {
        item.count = 0;
        item.Selected = 0;
        return item;
      } else {
        return item;
      }
    });
    this.setState({ list: list });
  }
  PhoneGetWastTypePlus(WasteTypeID) {
    var list = this.state.list.map((item) => {
      if (item.WasteTypeID === this.state.list.filter(el => el.WasteTypeID == WasteTypeID)[0].WasteTypeID) {
        item.count = item.count + 1;
        return item;
      } else {
        return item;
      }
    });
    this.setState({ list: list });
  }
  PhoneGetWastTypeCount(WareId, count) {
      var list = this.state.list.map((item) => {
        if (item.WareId === this.state.list.filter(el => el.WareId == WareId)[0].WareId) {
          item.count = (Math.round(count * 100) / 100).toFixed(1);
          return item;
        } else {
          return item;
        }
      });
      this.setState({ list: list });
  }
  PhoneGetWastTypeMinus(WasteTypeID) {
    if (this.state.list.filter(el => el.WasteTypeID == WasteTypeID)[0].count > 1) {
      var list = this.state.list.map((item) => {
        if (item.WasteTypeID === this.state.list.filter(el => el.WasteTypeID == WasteTypeID)[0].WasteTypeID) {
          item.count = item.count - 1;
          return item;
        } else {
          return item;
        }
      });
      this.setState({ list: list });
    }
  }
  PhoneGetWastTypeSelectedApi(WareId, Title, Price, WarePoint, Selected) {
    var list = this.state.list.map((item) => {
      if (item.WareId === this.state.list.filter(el => el.WareId == WareId)[0].WareId) {
        item.Selected = 1;
        return item;
      } else {
        return item;
      }
    });
    this.setState({ list: list });
  }
  SubmitRequest() {
    if (this.state.list.filter(el => el.Selected == 1 && el.count <= 0).length > 0) {
      alert("لطفا غرفه مورد نظر و محصول مورد نظر را انتخاب نمایید")
      this.setState({
        ErrorSubmit: true
      })
    }
    else {

      this.PhoneEditRequestWareHelperByHelper();
      //this.props.navigation.navigate("SelectedProductScreen",{list:this.state.list,BoothId:this.state.MainAreaIDSelected})
    }
  }

  async PhoneEditRequestWareHelperByHelper() {
    this.setState({
      ApiSuccsess: false,
      ApiSuccessDesc: ''
    });
    const headers = null;
    var list = this.state.list.filter(el => el.count > 0).map((item) => {
      return {
        WareId: item.WareId,
        WareCount: item.count
      };
    });
    const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber,
      BoothId: this.state.MainAreaIDSelected,
      RequestWareHelperList: list,
      RequestWareHelperId:this.props.navigation.getParam('RequestWareHelperId', 0)
    };
    try {
      // alert(JSON.stringify(params));
      const response = await Repository.PhoneEditRequestWareHelperByHelperApi(params, headers);
     
      if (response.Success === 1) {
        this.setState({
          ApiSuccsess: true,
          ApiSuccessDesc: response.Text
        });
        this.props.navigation.navigate("SelectedProductScreen", { ResultID: response.ResultID });
      }
      else {
        this.setState({
          apiError: true,
          apiErrorDesc: response.Text
        });
      }
      this.setState({
        isLoadingPage: false,
      });
    } catch (error) {
      this.setState({
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
    }
  }
  renderRequest() {
    if (this.state.list.filter(el => el.count > 0).length > 0 && (this.state.listPoint.Points - this.state.list.reduce((a, v) => a + (v.WarePoint * v.count), 0)) >= 0)
      return (

        <TouchableOpacity
          style={[styles.BtnSubmit1]}
          onPress={() => this.SubmitRequest()}
        >
          <Icon type="AntDesign" name="doubleleft" style={{ color: 'white', fontSize: 18, paddingRight: 20 }} />

          <Text style={[styles.textFooter]}>
            انتخاب کالا و تایید </Text>
        </TouchableOpacity>
      )
    else if ((this.state.listPoint.Points - this.state.list.reduce((a, v) => a + (v.WarePoint * v.count), 0)) < 0)
      return (


        <Text style={[styles.BtnSubmit2]}>
          مجموع امتیاز کالاهای انتخابی شما از مجموع امتیازات کیف پول شما بیشتر است </Text>
      )
    else
      return (
        <TouchableOpacity
          style={[styles.BtnSubmit1]}
        >
          <Icon type="AntDesign" name="doubleleft" style={{ color: 'white', fontSize: 18, paddingRight: 20 }} />
          <Text style={[styles.textFooter]}>
            کالای مورد نظر را انتخاب نمایید </Text>
        </TouchableOpacity>
      )
  }
  renderItem(item) {
    const {
      ImagePath,
      Title,
      Price,
      WarePoint,
      WareId,
      Description,
      Selected,
      Count
    } = item;
    if (Selected) {
      return (

        <Card style={styles.BoxWasteSelected}>
          <View style={[styles.TotalWaste]}>

            {/* <Icon type="Entypo" name="shopping-bag" style={[styles.IconBag]}/> */}

            <Image source={{ uri: ImagePath }} style={styles.IconBag} />
            <View style={[styles.Waste]}>
              <Text style={styles.textWaste}> {Title}</Text>
              <Text style={styles.textWaste}> امتیاز {WarePoint}</Text>
              <Text style={styles.textWaste}> تعداد انتخابی: {item.count} </Text>
            </View>
            <View style={[styles.Waste]}>

              <View visible={!Selected} style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon type="AntDesign" name="close"
                  onPress={() => {
                    this.setState({
                      apiError: false,
                      isLoadingPage: true,
                    },
                      () => {
                        this.PhoneGetWastTypeDeleted(WareId);
                      });
                  }}
                  style={[styles.IconGreen, { color: 'red' }]} />
               
                <TextInput
                  maxLength={5}
                  placeholder={'تعداد جدید'}
                  keyboardType={'numeric'}
                  value={Count}
                  onChangeText={(Count) => {
                    this.PhoneGetWastTypeCount(WareId, Count);
                  }}
                />

              </View>
            </View>
          </View>
        </Card>

      );
    }
    else {
      return (

        <Card style={styles.BoxWaste}>
          <View style={[styles.TotalWaste]}>

            {/* <Icon type="Entypo" name="shopping-bag" style={[styles.IconBag]}/> */}

            <Image source={{ uri: ImagePath }} style={styles.IconBag} />
            <View style={[styles.Waste]}>
              <Text style={styles.textWaste}> {Title} </Text>
              <Text style={styles.textWaste}> امتیاز {WarePoint}</Text>
            </View>
            <View style={[styles.Waste]}>
              <View visible={Selected} style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon type="AntDesign" name="pluscircleo"
                  onPress={() => {
                    this.setState({
                      apiError: false,
                      isLoadingPage: true,
                    },
                      () => {
                        this.PhoneGetWastTypeSelectedApi(WareId, Title, Price, 1, 1);
                      });
                  }}
                  style={[styles.IconBadge1]} />
              </View>

            </View>
          </View>
        </Card>

      );
    }
  }
  ListSelected() {
    return this.state.list.filter(el => el.Selected == 1).map((data) => {
      return (
        <Card style={styles.BoxWaste}>
          <View style={[styles.TotalWaste]}>

            {/* <Icon type="Entypo" name="shopping-bag" style={[styles.IconBag]}/> */}

            <Image source={{ uri: data.ImagePath }} style={styles.IconBag} />


            <View style={[styles.Waste]}>
              <Text style={styles.textWaste}> {data.Title}</Text>
            </View>
            <View style={[styles.Waste]}>

              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <TextInput
                  placeholder={'وزن مورد نظر را وارد نمایید'}
                  keyboardType={'numeric'}
                  value={data.count}
                  onChangeText={(count) => {
                    this.PhoneGetWastTypeCount(data.WasteTypeID, count);
                  }}
                />
                <Icon type="AntDesign" name="close"
                  onPress={() => {
                    this.setState({
                      apiError: false,
                      isLoadingPage: true,
                    },
                      () => {
                        this.PhoneGetWastTypeSelectedApi(data.WasteTypeID, data.Title, data.HelperPrice, data.WarePoint, 0);
                      });
                  }}

                  style={[styles.IconGreen, { color: 'red' }]} />

              </View>
            </View>
          </View>
        </Card>
      );
    })
  }

  render() {
    return (
      <Container>
        <StatusBar
          backgroundColor={Colors.statusBar}
          barStyle="light-content"
        />
        <HeaderBack
          onBackPress={() => this.props.navigation.goBack()}
          headerTitle={'  ویرایش کالاهای تشویقی و انتخاب مکان تحویل'}
        />
        <Content style={{ flex: 1 }}>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingTop: 10 }}>
            {this.renderRequest()}
          </View>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingTop: 10 }}>
            <TouchableOpacity
              style={[styles.BtnSubmit1]}
            // onPress={() => this.props.navigation.navigate("DeliveryTimeScreen",{laps:this.state.laps})}
            >
              <Text style={[styles.textFooter]}>
                مجموع امتیاز همیار: {this.state.listPoint.Points - this.state.list.reduce((a, v) => a + (v.WarePoint * v.count), 0)}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingTop: 10 }}>
            <TouchableOpacity
              style={[styles.BtnSubmit1]}
            // onPress={() => this.props.navigation.navigate("DeliveryTimeScreen",{laps:this.state.laps})}
            >
              <Text style={[styles.textFooter]}>
                مجموع امتیاز این درخواست: {this.state.list.reduce((a, v) => a + (v.WarePoint * v.count), 0)}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingTop: 20 }}>
            <View style={styles.pickerContainer}>
              <Text style={styles.inputTitle}>
                {'غرفه مورد نظر را انتخاب نمایید'}

              </Text>
              <Picker
                style={styles.pickerStyle}
                textStyle={styles.pickerTextStyle}
                iosHeader="غرفه ها را انتخاب کنید"
                headerBackButtonText="بستن"
                iosIcon={<Icon name="ios-arrow-down-outline" />}
                headerStyle={styles.pickerHeaderStyle}
                headerTitleStyle={styles.pickerHeaderTextStyle}
                headerBackButtonTextStyle={styles.pickerBackHeaderTextStyle}
                itemStyle={styles.pickerItemStyle}
                itemTextStyle={styles.pickerItemTextStyle}
                selectedValue={this.state.MainAreaIDSelected}
                onValueChange={(MainAreaID) => {
                  this.setState({ MainAreaIDSelected: MainAreaID }, () => { this.PhoneGetWastTypeApi(1); });

                }}
                mode={'dropdown'}
              >
                <Picker.Item key={"-1"} label={`انتخاب غرفه `} value={"-1"} />
                {this.renderAreaPickerItem()}
              </Picker>
            </View>
          </View>
          <View  style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:10}}>
            <View style={styles.pickerContainer}>
            <Text style={styles.inputTitle}>
                 {this.state.Address}
                </Text>
              </View>
              </View>
          <ScrollView style={{ flex: 1 }} >
            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingHorizontal: 10, paddingTop: 13 }}>
              <FlatList
                data={this.state.list}
                renderItem={({ item }) => this.renderItem(item)}
                ListFooterComponent={() => this.renderFooter()}
                numColumns={3}
                // horizontal={true}
                keyExtractor={(item, index) => item.WareId.toString()}
                refreshing={this.state.refreshing}
                onRefresh={() => this.handleRefresh()}
                onEndReached={() => this.handleLoadMore()}
                onEndReachedThreshold={0.08}
                contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
              />

            </View>
          </ScrollView>


          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingTop: 10 }}>

            <Portal>
              <Dialog
                visible={this.state.apiError}
                style={styles.dialogContainer}
                dismissable
                onDismiss={() => {
                  this.setState({
                    apiError: false,
                  });
                }}
              >
                <Dialog.Content>
                  <Text style={styles.dialogText}>
                    {this.state.apiErrorDesc}
                  </Text>
                </Dialog.Content>
                <Dialog.Actions>
                  <Button
                    buttonText={'بستن '}
                    onPress={() => {
                      this.setState({
                        apiError: false,
                      });
                    }}
                    style={styles.dialogButton}
                  />
                </Dialog.Actions>
              </Dialog>
              <Dialog
                visible={this.state.ApiSuccess}
                style={styles.dialogContainer}
                dismissable
                onDismiss={() => {
                  this.setState({
                    ApiSuccess: false,
                  });
                }}
              >
                <Dialog.Content>
                  <Text style={styles.dialogText}>
                    {this.state.ApiTitleSuccess}
                  </Text>
                </Dialog.Content>

              </Dialog>
              {/* <Dialog
                      visible={this.state.ErrorSubmit}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                            ErrorSubmit: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {"تمام آیتم های انتخابی باید دارای وزن باشند"}
                          </Text>
                      </Dialog.Content>
                      
                  </Dialog> */}
              <Dialog
                visible={this.state.ApiDescription}
                style={styles.dialogContainer}
                dismissable
                onDismiss={() => {
                  this.setState({
                    ApiDescription: false,
                  });
                }}
              >
                <Dialog.Content>
                  <Text style={styles.dialogText}>
                    {this.state.ApiTitleDescription}
                  </Text>
                </Dialog.Content>

              </Dialog>
            </Portal>
          </View>
        </Content>
      </Container>
    );
  }

}

export
  const styles = EStyleSheet.create({
    IRANSansNormal: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_M',
      },
    },
    IRANSansBold: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_B',
      },
    },
    header: {
      backgroundColor: '#50b3ae',
    },

    textHeader: {
      color: 'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_B',
      },
    },
    text: {
      color: 'black',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_M',
      },
    },
    ImgProduct: {
      width: 120,
      height: 180,
      resizeMode: 'contain',
    },
    textTitle: {
      color: 'black',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_M',
      },
    },
    Title: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-around',
      paddingTop: 15
    },
    line: {
      width: '50%',
      borderTopWidth: 1.5,
      borderColor: '#50b3ae',
    },
    BoxWaste: {
      width: Dimensions.get('window').width * 0.30,
      backgroundColor: 'white',
      alignItems: 'center',
      borderRadius: 10,
      paddingVertical: 1,
      marginHorizontal: Dimensions.get('window').width * 0.01
    },
    BoxWasteSelected: {
      width: Dimensions.get('window').width * 0.30,
      backgroundColor: '#dddddd',
      alignItems: 'center',
      borderRadius: 10,
      paddingVertical: 1,
      marginHorizontal: Dimensions.get('window').width * 0.01
    },

    textWaste: {
      alignItems: 'center',
      fontSize: 10,
      color: 'black',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '400',
        justifyContent: 'center'
      },
      '@media android': {
        fontFamily: '$IR_M',
      },
    },
    TotalWaste: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: 'transparent',
      width: '100%',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    Waste: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
      paddingBottom: 1
    },
    IconGreen: {
      color: '#50b3ae',
      fontSize: 20
    },
    IconBadge: {
      color: '#50b3ae',
      fontSize: 28,
      paddingRight: 10,
      marginRight: 10

    },
    IconBadge1: {
      color: '#50b3ae',
      fontSize: 20,
      //paddingRight:10,
      //marginRight:10
    },

    IconBag: {
      color: '#50b3ae',
      fontSize: 25,
      width: '90%',
      height: 50,
      resizeMode: 'contain'
    },
    textBtnProduct: {
      color: 'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_M',
      },
    },
    BtnSubmit: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#50b3ae',
      borderRadius: 10,
      padding: 7,
      width: '60%'
    },
    BtnSubmit1: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#50b3ae',
      borderRadius: 10,
      padding: 7,
      width: '90%'
    },
    BtnSubmit2: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#F1C40F',
      borderRadius: 10,
      padding: 7,
      width: '90%'
    },
    textFooter: {
      color: 'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_B',
      },

    },
    container: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: '#F5FCFF',
    },
    TotalCard: {
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      paddingHorizontal: 10,
      paddingTop: 10
    },
    CardSelect: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
      borderRadius: 5,
      padding: 8,
      paddingVertical: 12
    },
    BtnFooter: {
      flex: 1,
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    IconFooter: {
      color: 'white',
      fontSize: 18,
    },

    dialogContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
      padding: 0

    },
    dialogContent: {
      margin: 0
    },
    dialogTitleText: {
      width: '100%',
      borderBottomColor: Colors.green,
      borderBottomWidth: 2,
      color: Colors.green,
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_B',
      },
    },
    dialogText: {
      margin: 0,
      fontSize: normalize(13),
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_M',
      },
      textAlign: 'center',
      color: Colors.textBlack,
    },
    dialogButton: {
      width: wp('30%')
    },
    pickerContainer: {
      height: hp('6%'),
      width: wp('70%'),
      alignSelf: 'center',
      textAlign: 'center',
      justifyContent: 'center',
    },
    pickerStyle: {
      // borderBottomWidth: 0.8,
      // borderColor: '#e2e2e2',
      width: wp('50%'),
      justifyContent: 'center',
    },
    pickerHeaderStyle: {
      backgroundColor: Colors.purple,
    },
    pickerHeaderTextStyle: {
      width: wp('50%'),
      backgroundColor: 'transparent',
      fontSize: 16,
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '300',
      },
      '@media android': {
        fontFamily: '$IR_L',
      },
      color: 'white',
      textAlign: 'right',
    },
    pickerBackHeaderTextStyle: {
      width: wp('50%'),
      height: hp('50%'),
      backgroundColor: 'transparent',
      fontSize: 16,
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '300',
      },
      '@media android': {
        fontFamily: '$IR_L',
      },
      color: 'white',
      textAlign: 'left',
    },
    pickerTextStyle: {
      fontSize: 16,
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '300',
      },
      '@media android': {
        fontFamily: '$IR_L',
      },
      color: '#34495e',
      textAlign: 'center',
      height: hp('50%'),
    },
    pickerItemStyle: {
      width: wp('90%'),
      height: hp('50%'),
      backgroundColor: 'transparent'
    },
    pickerItemTextStyle: {
      fontSize: 16,
      width: wp('65%'),
      backgroundColor: 'transparent',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: 'bold',
      },
      '@media android': {
        fontFamily: '$IR_B',
      },
      color: '#34495e',
      textAlign: 'right',
      height: hp('50%'),
    },
    pickerSeparator: {
      '@media android': {
        height: 0.5,
        width: wp('90%'),
        backgroundColor: '#e2e2e2'
      }
    },
  });


const mapStateToProps = (state) => {
  return {
    userData: state.userData
  };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(EditProductScreen);
