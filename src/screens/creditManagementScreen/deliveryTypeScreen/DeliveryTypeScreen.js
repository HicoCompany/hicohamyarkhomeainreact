import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StatusBar, Alert, BackHandler } from 'react-native';
import { Text, Card, CardItem, Container, Content, } from 'native-base';

import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import { hp, normalize, wp } from '../../../responsive';

import TabItem from '../../../components/TabItem';
import HeaderMain2 from '../../../components/HeaderMain2';

class DeliveryTypeScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      tabBarIcon: ({ tintColor }) =>
        <TabItem
          name={'مدیریت اعتبار'}
          source={require('./../../../assets/image/ic_waste.png')}
          color={tintColor}
        />,
    };
  };
  componentDidMount = () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

  }


  handleBackButton = () => {
    Alert.alert(
      'خارج شدن از برنامه',
      'آیا با خروج از برنامه موافق هستید؟', [{
        text: 'نه',
        onPress: () => console.log('کنسل'),
        style: 'cancel'
      }, {
        text: 'بله',
        onPress: () => BackHandler.exitApp()
      },], {
      cancelable: false
    }
    )
    return true;
  }
  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);

  }
  render() {
    return (
      <Container>
        <Content>

          <View style={styles.container}>
            <StatusBar
              backgroundColor={Colors.statusBar}
              barStyle="light-content"
            />
            <HeaderMain2
              onMenuPress={() => BackHandler.exitApp()}
              onPinPress={() => this.props.navigation.navigate('boothDynamic')}
              headerTitle={'مدیریت اعتبار '}
            />
            {/* <HeaderMain
                    onMenuPress={() => this.props.navigation.toggleDrawer()}
                    onPinPress={() => this.props.navigation.navigate('boothDynamic')}
                /> */}

            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>

              {/* <View style={{flex:1,flexDirection:'column',alignItems:'center'}}  >
            <TouchableOpacity
                style={[styles.BtnDetails]}
                onPress={() => this.props.navigation.navigate("SupportingHicoScreen")}
              >
            <View>
              <Card style={styles.BoxProduct}>
                  <CardItem header style={{borderBottomWidth:1,borderBottomColor:'gray'}}>
                     <Image source={require('./../../../assets/image/ic_hico.png')} style={styles.ImgProduct}/>
                     </CardItem>
                     <Text style={styles.textProduct}>{"  حمایت از هیکو"}</Text>
              
            </Card>
              </View>
            </TouchableOpacity>
            </View> */}
              <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}  >
                <TouchableOpacity
                  style={[styles.BtnDetails]}
                  onPress={() => this.props.navigation.navigate("CashWithdrawalScreen")}
                >
                  <View>
                    <Card style={styles.BoxProduct}>
                      <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                        <Image source={require('./../../../assets/image/RemainCash.png')} style={styles.ImgProduct} />
                      </CardItem>
                      <Text style={styles.textProduct}>{"مانده حساب"}</Text>
                    </Card>
                  </View>
                </TouchableOpacity>


              </View>
              <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}  >
                <TouchableOpacity
                  style={[styles.BtnDetails]}
                  onPress={() => this.props.navigation.navigate("CharityScreen")}
                >
                  <View>
                    <Card style={styles.BoxProduct}>
                      <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                        <Image source={require('./../../../assets/image/HelpToKheyrieh.png')} style={styles.ImgProduct} />
                      </CardItem>
                      <Text style={styles.textProduct}>{" کمک به خیریه"}</Text>
                    </Card>
                  </View>
                </TouchableOpacity>


              </View>
            </View>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>

              <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}  >
                <TouchableOpacity
                  style={[styles.BtnDetails]}
                  onPress={() => this.props.navigation.navigate("ProductScreen")}
                >
                  <View>
                    <Card style={styles.BoxProduct}>
                      <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                        <Image source={require('./../../../assets/image/Kala.png')} style={styles.ImgProduct} />
                      </CardItem>
                      <Text style={styles.textProduct}>{"   ارائه کالا "}</Text>

                    </Card>
                  </View>
                </TouchableOpacity>

              </View>
              <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}  >

                <TouchableOpacity
                  style={[styles.BtnDetails]}
                  onPress={() => this.props.navigation.navigate("SpecialServiceScreen")}
                >
                  <View>
                    <Card style={styles.BoxProduct}>
                      <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                        <Image source={require('./../../../assets/image/SpecialService.png')} style={styles.ImgProduct} />
                      </CardItem>
                      <Text style={styles.textProduct}>{"  خدمات ویژه "}</Text>

                    </Card>
                  </View>
                </TouchableOpacity>
              </View>

            </View>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>


<View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}  >

  <TouchableOpacity
    style={[styles.BtnDetails]}
    onPress={() => this.props.navigation.navigate("RequestProductScreen")}
  >
    <View>
      <Card style={styles.BoxProduct}>
        <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
          <Image source={require('./../../../assets/image/RKala.png')} style={styles.ImgProduct} />
        </CardItem>
        <Text style={styles.textProduct}>{"لیست درخواست ها"}</Text>

      </Card>
    </View>
  </TouchableOpacity>
</View>

</View>
          </View>
        </Content>
      </Container>
    );
  }

}

export
  const styles = EStyleSheet.create({
    IRANSansNormal: {
      fontSize: normalize(10),
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_M',
      },
    },
    IRANSansBold: {
      fontSize: normalize(10),
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_B',
      },
    },
    header: {
      backgroundColor: '#1a237e',
    },
    badge: {
      position: 'absolute',
      top: 8,
      right: 10,
    },
    BtnImg: {
      width: '100%',
      height: 50,
      alignItems: 'center',
      justifyContent: 'center',
    },
    textBtn: {
      color: 'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_B',
      },
      fontSize: 14,
    },
    bgProduct: {
      paddingHorizontal: 10,
      paddingVertical: 10,
      backgroundColor: '#fafafa',
      borderBottomWidth: 1,
      borderBottomColor: "#f0f0f0"
    },
    BoxProduct: {
      width: 160,
      height: 160,
      backgroundColor: 'white',
      alignItems: 'center',
    },
    ImgProduct: {
      width: '85%',
      height: 80,
      resizeMode: 'contain',

    },
    textProduct: {
      color: 'black',
      fontSize: 14,
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_B',
      },
    },
    textGreen: {
      color: '#2e7d32',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_B',
      },
    },
    textRed: {
      color: '#e53935',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_B',
      },
      textDecorationLine: 'line-through',
      textDecorationStyle: 'solid',
      textDecorationColor: "#000"
    },
    BtnProduct: {
      borderWidth: 1,
      borderColor: '#f0f0f0',
      height: 40,
      justifyContent: "center",
      alignItems: "center"
    },
    BtnProductOne: {
      flex: 2,
      justifyContent: "center",
      alignItems: "center",


      backgroundColor: 'green'
    },
    BtnProductTwo: {
      flex: 1, backgroundColor: 'white',
      justifyContent: "center",
      alignItems: "center",
      borderBottomRightRadius: 10,
      borderTopRightRadius: 10,
    },
    textBtnProduct: {
      color: 'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
      },
      '@media android': {
        fontFamily: '$IR_B',
      },
      fontSize: 11,
    },
    BoxBtnSort: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: "center",
      alignItems: "center",
      borderWidth: 0.5,
      borderColor: "#bdbbbb",
      width: 330,
    },
    BtnSort: {
      width: 160,
      height: 40,
      paddingHorizontal: 4,
      backgroundColor: '#fafafa',
      alignItems: "center",
    }
  });


const mapStateToProps = (state) => {
  return {
    userData: state.userData
  };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(DeliveryTypeScreen);
