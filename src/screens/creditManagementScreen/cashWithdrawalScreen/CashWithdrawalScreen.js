import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar,TextInput,BackHandler } from 'react-native';
import { Text,Card, CardItem,Container, Content,Footer, FooterTab,Icon } from 'native-base';
import { Portal, Dialog } from 'react-native-paper';

import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import Repository from '../../../repository/Repository';
import { hp, normalize, wp } from '../../../responsive';

import TabItem from '../../../components/TabItem';
import HeaderBack from '../../../components/HeaderBack';

import { Button, Loading } from '../../../components';


class CashWithdrawalScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'ماندهحساب '}
                    source={require('./../../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        };
    };
    constructor(props) {
      super(props);
      const {
        
        Payment,
    } = this.props.userData.user;
      this.state = {
        Payment:0,
        apiSuccess: false,
        apiSuccessDesc:'',
        apiSuccessTitle:'',
        apiError: false,
        apiErrorDesc: '',
        isLoadingButton:false,
        list:[]
      };
    }

    componentDidMount = () => {
        this.getApi();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      }
      
      handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
      } 
    getApi = async () => {
        await this.PhoneGetInfoCreditManagementApi();
    }
    
    async PhoneGetInfoCreditManagementApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber
        };
        
        try {
          
            const response = await Repository.PhoneGetInfoCreditManagementApi(params, headers);
          
            if (response.ResultID === 100) {
                this.setState({
                    list:response.Result
                });
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
            });
        } catch (error) {
            await this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }
    onConfirm = () => {

      const {
        Payment
      } = this.state;
    
      let error = false;
    
      if (Payment.length === 0) {
          error = true;
          this.setState({
              errorPayment: 'لطفا مبلغ مورد نظر را وارد نمایید.'
          });
      }
    
      if (error) {
          return;
      }
      if (!error) {
         
          this.setState({
              isLoadingButton: true,
              
          },async () => {
              this.PhoneAddCashpaymentApi();
          });
      }
    }
    async PhoneAddCashpaymentApi() {
            
    
      const headers = null;
      const params = {
          Mobile: this.props.userData.mobileNumber,
          Code: this.props.userData.codeNumber,
          Payment: this.state.Payment,
         
      };
      try {
          const response = await Repository.PhoneAddCashpaymentApi(params, headers);
          if (response.Success === 1) {
            
              this.setState({
                  isLoadingButton:false,
                  apiSuccess:true,
                  apiSuccessDesc:response.Text,
                  apiSuccessTitle:response.Titel,
                  Payment:''
              });
          }
          else {
              this.setState({
                  apiError: true,
                  apiErrorDesc: response.Text
              });
          }
         
      } catch (error) {
          if (this.mounted) {
              this.setState({
                  isLoadingButton: false,
                  apiError: true,
                  apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
              });
          }
      }
    }
     
    render() {
        return (
            <Container>
               
          <Content>
          <StatusBar
                        backgroundColor={Colors.statusBar}
                        barStyle="light-content"
                    />
                    <HeaderBack
                        onBackPress={() => this.props.navigation.goBack()}
                        headerTitle={'مانده حساب '}
                    />
          {/* <View style={[styles.Center]}>
              <View style= {styles.inputTotal}>
              <Text style={[styles.text,{color:'#50b3ae'}]}>تومان</Text>
                <TextInput   
                    placeholder="مبلغ مورد نظر "  
                    style={[styles.input ]} placeholderTextColor={'#50b3ae'}
                    value={this.state.Payment}
                    keyboardType='numeric'
                    onChangeText={(Payment) => {
                        this.setState({
                          Payment,
                          errorPayment: ''
                        });
                      }}
                />
                <Icon type="Entypo" name="database" style={{color:'#50b3ae'}}/>
           </View>
            </View> */}
            <View style={[styles.TotalText]}>
            <Text style={[styles.text,{color:'#50b3ae'}]}>تومان</Text>
            <Text style={[styles.text,{color:'#50b3ae'}]}>{this.state.list.Remain}</Text>
            <Text style={[styles.text,{color:'#50b3ae'}]}>موجودی :</Text>
            </View>
            <Portal>
                        <Dialog
                            visible={this.state.apiError}
                            style={styles.dialogContainer}
                            dismissable
                            onDismiss={() => {
                                this.setState({
                                    apiError: false,
                                });
                            }}
                        >
                            <Dialog.Content>
                                <Text style={styles.dialogText}>
                                    {this.state.apiErrorDesc}
                                </Text>
                            </Dialog.Content>
                           
                        </Dialog>
                        <Dialog
                            visible={this.state.apiSuccess}
                            style={styles.dialogContainer}
                            onDismiss={() => {
                                this.setState({
                                    apiSuccess: false,
                                });
                            }}
                        >
                         <Dialog.Title style={styles.dialogTitleText}>
                            {this.state.apiSuccessTitle}
                         </Dialog.Title>
                            <Dialog.Content style={styles.dialogContent}>

                               
                                <Text style={styles.dialogText}>
                                    {this.state.apiSuccessDesc}
                                </Text>
                                <Button
                                    buttonText={'بستن'}
                                    onPress={() => {
                                        this.setState({
                                            apiSuccess: false,
                                        });
                                    }}
                                    style={styles.dialogButton}
                                />
                            </Dialog.Content>
                           
                        </Dialog>
                    </Portal>
        </Content>
        {/* <Footer style={{height:70,width:"100%"}}>
          <FooterTab  style={{backgroundColor:'#50b3ae'}}>
          <TouchableOpacity  
                style={[styles.BtnFooter]}
                onPress={() => {
                  this.onConfirm();
              }}
                                     >
                <Text style={[styles.textFooter]}>
                {this.state.isLoadingButton ? 'لطفا منتظر بمانید' : 'مبلغ برداشتی  '} 
                </Text>
            </TouchableOpacity>
          </FooterTab>
        </Footer> */}
      </Container>
        );
    }

}

  export 
  const styles = EStyleSheet.create({
    IRANSansNormal: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     IRANSansBold: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     header:{
      backgroundColor:'#50b3ae',
     },
     textHeader:{
      color:'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
      fontSize: 14,
     },
      text:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
        fontSize: 12,
       },

    inputTotal:{
        borderColor:'#b3afaf',
        backgroundColor:'white',
        borderRadius: 5,
        borderWidth: 1,
        justifyContent:'space-around',
        alignItems: 'center',
        flexDirection:'row',
        flex:1,
        marginTop:20,
        paddingHorizontal: 10,
        width:'90%',
      },
      input:{
        fontSize:12,
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
        flex:1,
        paddingHorizontal: 10,
        textAlign:'center'
      },
       textFooter:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
        fontSize: 14,
        
       },
       BtnFooter:{
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
       },
       Center:{
        alignItems: 'center',
        justifyContent:'center',
       },
       TotalText:{
        flexDirection:'row',
        flex:1,  
        alignItems: 'center',
        justifyContent:'space-around',
        marginTop:20,
       },
       
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    }
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(CashWithdrawalScreen);
