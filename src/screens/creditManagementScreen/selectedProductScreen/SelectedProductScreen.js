import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar,ScrollView, ActivityIndicator, 
    FlatList ,Dimensions,BackHandler,TextInput} from 'react-native';
import { Text,Card, CardItem,Container, Content, Icon,Footer, FooterTab,Picker} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import { normalize,wp,hp } from '../../../responsive';
import TabItem from '../../../components/TabItem';
import HeaderBack from '../../../components/HeaderBack';
import { Dialog, Portal } from 'react-native-paper';
import Repository from '../../../repository/Repository';
import Utils from '../../../utility/Utils';
import { Button, Empty, Loading } from './../../../components';
import { setMapId } from '../../../redux/actions';
import { setMapAddress } from '../../../redux/actions';

class SelectedProductScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'مشاهده جزئیات کالاهای درخواستی '}
                    source={require('./../../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        };
    };
constructor(props) {
      super(props);
      this.state = {
          isLoadingPage: true,
          list: [],
          areaList: [],
          refreshing: false,
          loadingData: false,
          laps:[],
          apiError: false,
          apiErrorDesc: '',
          address:'',
          MainAreaIDMap:0,
          MainAreaIDChangeMap:0,
          MainAreaIDSelected:0
      };
      this.mounted = null;
    }
    
componentDidMount = () => {
    this.setState({ResultID:this.props.navigation.getParam('ResultID',0)});
    this.getApi();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  handleBackButton = () => {
    this.props.navigation.navigate("RequestProductScreen");
    clearInterval(this.interval);
    return true;
  } 
  componentWillReceiveProps(nextProps) {
    this.setState({ResultID:this.props.navigation.getParam('ResultID',0)});
    this.getApi();
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
shouldComponentUpdate = (nextProps, nextState) => {
    return Utils.shallowCompare(this, nextProps, nextState);
}
getApi = async () => {
    this.mounted = true;
    await this.PhoneShowWareDetailsHelerApi();
    //await this.getAreaApi();

}

// renderAreaPickerItem() {
//     return this.state.areaList.map(area =>
//         <Picker.Item key={area.MainAreaCode.toString()} label={`${area.Title}`} value={area.MainAreaCode} />
//     );
// }

renderEdit(Status, RequestWareHelperId) {
    if (Status == 0) {
      return (
        <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:10,paddingBottom:10}}>
        <TouchableOpacity 
        onPress={() => this.props.navigation.navigate("EditProductScreen",{RequestWareHelperId:this.props.navigation.getParam('ResultID',0)})}
style={[styles.BtnSubmit1]}
                     >
              <Text style={[styles.textFooter]}>
              ویرایش
              </Text>
              </TouchableOpacity>
          </View> 
      );
    }

  }

async PhoneShowWareDetailsHelerApi() {
    const headers = null;
    const params = {
        Mobile: this.props.userData.mobileNumber,
        Code: this.props.userData.codeNumber,
        RequestWareHelperId:this.props.navigation.getParam('ResultID',0)
    };
    try {
        const response = await Repository.PhoneShowRequestWareDetialApi(params, headers);
       
        if (response.Success === 1) {
            this.setState({list:response.Result.Result,
                Address:response.Result.Address,
                Status:response.Result.Status,
                RequestDateStr:response.Result.RequestDateStr,
                BoothWorkTime:response.Result.BoothWorkTime,
                BoothTitle:response.Result.BoothTitle,
                StatusCode:response.Result.StatusCode});
        }
        else {
            this.setState({
                apiError: true,
                apiErrorDesc: response.Text
            });
        }
        this.setState({
            isLoadingPage: false,
        });
    } catch (error) {
        await this.setState({
            isLoadingPage: false,
            apiError: true,
            apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
    }
}

handleLoadMore = () => {
}

handleRefresh = () => {
    this.setState({ 
        refreshing: true, 
        isLoadingPage: true,
        laps: [],
    }, () => {
    });
}

renderEmpty = () => {
    return this.state.isLoadingPage ? (
        <View style={{ flex: 1 }}>
            <Loading
                message={'در حال دریافت اطلاعات'}
                messageColor={Colors.green}
            />
        </View>
    ) : (
            <View style={{ flex: 1 }}>
                <Empty />
            </View>
        );
}

lapsList() {
    return this.state.list.map((data) => {
      return (
        <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center'}}>
           <Card style={[styles.CardSelect]}>
      <Text style={[styles.text]}>{data.WarePoint} امتیاز</Text>
                <Text style={[styles.text]}> {data.Count} تعداد </Text>
                <Text style={[styles.text]}>{data.Title}</Text>
            </Card>
       </View>
      )
    })
}
renderFooter = () => {
    if (!this.state.loadingData) {
        return null;
    }
    return (<ActivityIndicator size={'small'} color={'black'} />);
}

    render() {
        return (
            <Container>
              <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                        onBackPress={() => this.props.navigation.navigate("RequestProductScreen")}
                        headerTitle={'مشاهده جزئیات کالاهای درخواستی'}
                    />
           <Content>
               <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:10}}>
          <TouchableOpacity 
  style={[styles.BtnSubmit1]}
                       >
                <Text style={[styles.textFooter]}>
                مجموع امتیاز کالاهای انتخابی: {this.state.list.reduce((a, v) => a + (v.WarePoint*v.Count) , 0)}
                </Text>
                </TouchableOpacity>
            </View> 
           <View style={[styles.Title]}>
               <View style={[styles.line]}>

               </View>
               <View>
        <Text style={[styles.textTitle]}>نوع کالا - {this.state.Status}</Text>
            </View>
            <View>
               <Icon type="AntDesign" name="calendar" style={{color:'#50b3ae'}}/>
            </View>
          </View>
          <View style={[styles.TotalCard]}>
          {this.lapsList()}
          
            <View style={{flex:1,flexDirection:'row',alignItems: 'center',justifyContent:'center',paddingTop:10}}>
          <TouchableOpacity 
  style={[styles.BtnSubmit1]}
                       >
                <Text style={[styles.textFooter]}>
               سفارش شما با موفقیت ثبت گردیده است
                </Text>
                </TouchableOpacity>
            </View> 
          </View>
          
          <View style={[styles.Title]}>
               <View style={[styles.line]}>

               </View>
               <View>
        <Text style={[styles.textTitle]}> مشخصات غرفه</Text>
            </View>
            <View>
               <Icon type="AntDesign" name="calendar" style={{color:'#50b3ae'}}/>
            </View>
          </View>
           <View style={[styles.TotalCard]}>
           <Text style={styles.textbold}>
                    {'عنوان غرفه: '}
           </Text> 
           <Text style={styles.inputTitle}>
                  {this.state.BoothTitle}
           </Text> 
           <Text style={styles.textbold}>
                    {'آدرس غرفه: '}
           </Text> 
           <Text style={styles.inputTitle}>
                    {this.state.Address}
           </Text> 
           <Text style={styles.textbold}>
                    {'ساعات کاری غرفه: '}
           </Text> 
           <Text style={styles.inputTitle}>
                   {this.state.BoothWorkTime}
           </Text> 
           <Text style={styles.textbold}>
                    {' تاریخ درخواست: '}
           </Text> 
           <Text style={styles.inputTitle}>
                   {this.state.RequestDateStr}
           </Text> 
                </View>
                {this.renderEdit(this.state.StatusCode, this.props.navigation.getParam('ResultID',0))}
             
        </Content>
            <Portal>
                  <Dialog
                      visible={this.state.ApiSuccess}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                            ApiSuccess: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.ApiTitleSuccess}
                          </Text>
                      </Dialog.Content>
                      
                  </Dialog>
                  <Dialog
                      visible={this.state.apiError}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                            apiError: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.apiErrorDesc}
                          </Text>
                      </Dialog.Content>
                      
                  </Dialog>
              </Portal>
           
      </Container>
        );
    }

}
  export 
  const styles = EStyleSheet.create({
    IRANSansNormal: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     IRANSansBold: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     header:{
      backgroundColor:'#50b3ae',
     },
      Btn:{
        padding:8
        
      },
      text:{
        color:'#000000',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
       },
       textTitle:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
       },
       Title:{
        flex:1,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'space-around',
        paddingTop:15
       },
       line:{
          width:'50%',
         borderTopWidth:1.5,
         borderColor:'#50b3ae',
       },
       BoxDaysWeek: {
        width: Dimensions.get('window').width * 0.40,
        height: 100,
        backgroundColor: '#e0e0e0',
        // alignItems: 'space-between',
        borderRadius: 10,
        marginHorizontal: Dimensions.get('window').width * 0.01
      },
      textDaysWeek: {
        color: 'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
      },
      TotalDate:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'transparent',
        width:'100%',
      },
      Week:{
        alignItems: 'center',
        justifyContent:'center',
        flex:1
      },
      Date:{
        alignItems: 'center',
        justifyContent:'center',
        flex:1,
        borderTopWidth:1,
        borderColor:'white'
      },
      textBtnProduct:{
       color:'white',
       '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
      },
      BtnFooter:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center'
      },
      inputFooter:{
        backgroundColor:"white",
        width:200,
        borderTopRightRadius:5,
        borderBottomRightRadius:5,
        paddingRight:10,
      },
      textFooter:{
          
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
       },
       container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
      },
     
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    },
    TotalCard:{
        flex:1,
        flexDirection:'column',
        alignItems: 'center',
        justifyContent:'center',
        paddingHorizontal:10,
        paddingTop:10
      },
      CardSelect:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-around',
        borderRadius:5,
        padding:8,
        paddingVertical:12
      },
      BtnSubmit1:{
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center',
        backgroundColor:'#50b3ae',
        borderRadius:10,
        padding:7,
        width:'90%'
      },
      
    pickerContainer: {
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 20,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
    },
    pickerStyle: {
        // borderBottomWidth: 0.8,
        // borderColor: '#e2e2e2',
        width: wp('50%'),
        // justifyContent: 'center',
    },
    pickerHeaderStyle: {
        backgroundColor: Colors.purple,
    },
    pickerHeaderTextStyle: {
        width: wp('50%'),
        backgroundColor: 'transparent',
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: 'white',
        textAlign: 'right',
    },
    pickerBackHeaderTextStyle: {
        width: wp('50%'),
        backgroundColor: 'transparent',
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: 'white',
        textAlign: 'left',
    },
    pickerTextStyle: {
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: '#34495e',
        textAlign: 'center',
    },
    pickerItemStyle: {
        width: wp('90%'),
        backgroundColor: 'transparent'
    },
    pickerItemTextStyle: {
        fontSize: 16,
        width: wp('65%'),
        backgroundColor: 'transparent',
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: '#34495e',
        textAlign: 'right'
    },
    pickerSeparator: {
        '@media android': {
            height: 1,
            width: wp('90%'),
            backgroundColor: '#e2e2e2'
        }
    },
    inputTitle: {
        width: wp('70%'),
        alignSelf: 'center',
        fontSize: normalize(14),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.textBlack,
        textAlign: 'center',
        marginBottom: 10
    },
    textbold:{
        width: wp('70%'),
        alignSelf: 'center',
        fontSize: normalize(16),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
            fontWeight:'bold'
        },
        color: Colors.textBlack,
        textAlign: 'right',
        marginBottom: 10,
        
    },
    input: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 20,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: 10

    }
   });
   const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};
const mapActionToProps = {
    setMapId,
    setMapAddress
};

export default connect(mapStateToProps, mapActionToProps)(SelectedProductScreen);
