import React, {Component} from 'react';
import { connect } from 'react-redux';
import { StatusBar } from 'react-native';
import Colors from '../../utility/Colors';
import HeaderBack from '../../components/HeaderBack';

import { View,Image,TouchableOpacity,ScrollView,} from 'react-native';
import { Container, Header,Content, Footer, FooterTab, Button, 
   Icon, Text,Card, CardItem ,Badge} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';

class ConfirmScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      horizontal: false,
       
    };
  }
  render() {
    return (
        <Container>
           <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                    onBackPress={() => this.props.navigation.goBack()}
                    headerTitle={'وضعیت درخواست'}
                />
          <Content>
              <View style={[styles.center,{paddingTop:20}]}>
                 <Image source={require('./../../assets/image/Confirm.png')} style={styles.ImgProduct}/>  
              </View>
                 <View style={[styles.center,{paddingTop:20}]}>
                   <Card style={styles.Box}>
                    <CardItem>
                      <Text style={styles.text}> با معرفی به دوستان و آشنایان خود , به حفظ محیط زیست کمک کنید</Text>
                   </CardItem>
                  </Card>
              </View>
              <View style={[styles.center,{paddingTop:20}]}>
                 <TouchableOpacity
                   style={[styles.BtnContent]}
                     onPress={() => this.props.navigation}
                     >
                     <Text style={[styles.textBtn]}>بازگشت</Text>
                  </TouchableOpacity>
                 </View>
        </Content>
      </Container>
    );
  }

}


const styles = EStyleSheet.create({
  IRANSansNormal: {
    '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
   },
   IRANSansBold: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_B',
  },
   },
   header:{
    backgroundColor:'#50b3ae',
   },
    textHeader:{
      color:'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
    textBtn:{
      color:'#e2f3ba',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     text:{
      color:'black',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     ImgProduct: {
      width: '85%',
      height: 80,
      resizeMode: 'contain',
     
    },
     center:{
         alignItems:'center',
         justifyContent:'center'
     },
     BtnContent:{
       backgroundColor:'#50b3ae',
         width:'90%',
         padding:10,
         borderRadius: 5,
         alignItems:'center',
         justifyContent:'center',
      
     },
     IconTotal:{
      flexDirection:'row',
      justifyContent:"center",
      alignItems:'center'
    },
    Box:{
      width:'90%',
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent:'center',
      borderRadius: 5,
      borderColor:'#8a987f'
    },
    textGreen:{
      color: '#50b3ae',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
    },
});
 
const mapStateToProps = (state) => {
  return {
      userData: state.userData
  };
};

const mapActionToProps = {

};
export default connect(mapStateToProps, mapActionToProps)(ConfirmScreen);
