import React, { Component } from 'react';
import { ScrollView, StatusBar, Text, View,TouchableOpacity,
    BackHandler } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import FastImage from 'react-native-fast-image';
import { Dialog, Portal } from 'react-native-paper';
import { connect } from 'react-redux';
import { Button, Loading } from '../../../components';
import Repository from '../../../repository/Repository';
import { hp, normalize, wp } from '../../../responsive';
import Colors from '../../../utility/Colors';
import Utils from '../../../utility/Utils';
import HeaderBack from './../../../components/HeaderBack';
import { ViewPagerAndroid } from 'react-native-gesture-handler';
import {Icon} from 'native-base';


class NoticeDetailScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoadingPage: true,
            detail: {
                CreateDateStr: '',
                ImagePath: '',
                NoticesID: 1,
                Text: '',
                Title: '',
                Like:0
            },
            apiSuccess:false,
            apiSuccessDesc:'',
            apiSuccessTitle:'',
            apiError: false,
            apiErrorDesc: '',
            Like:0
        };
    }

    componentDidMount = () => {
        this.getApi();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
      } 
    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

   
      
    getApi = async () => {
        await this.phoneGetNotificationByIDApi();
    }
    async PhoneAddLikeNotificationForHelperApi() { 
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber,
            NoticesID: this.props.navigation.getParam('item').NoticesID
        };
        try {
            const response = await Repository.PhoneAddLikeNotificationForHelperApi(params, headers);
            if (response.Success === 1) {
                this.setState({
                    Like:this.state.Like==1?0:1,
                    apiSuccess: true,
                    apiSuccessDesc: response.Text,
                    apiSuccessTitle:response.Titel
                }); 
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                isLoadingPage: false,
            });
        } catch (error) {
            alert(error);
            await this.setState({
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }
    async phoneGetNotificationByIDApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber,
            NoticesID: this.props.navigation.getParam('item').NoticesID
        };
        try {
            const response = await Repository.PhoneGetNotificationByIDApi(params, headers);
            if (response.Success === 1) {
                this.setState({
                    detail: response.Result,
                    Like:response.Result.Like
                    
                });
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                isLoadingPage: false,
            });
        } catch (error) {
            await this.setState({
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }

    render() {
        const {
            CreateDateStr,
            ImagePath,
            NoticesID,
            Title,
            Like,
        } = this.props.navigation.getParam('item');
        return this.state.isLoadingPage ? (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                    onBackPress={() => this.props.navigation.goBack()}
                    headerTitle={Title}
                />
                <Loading
                    message={'در حال دریافت اطلاعات'}
                    messageColor={Colors.green}
                />
            </View>
        ) : (
                <View style={styles.container}>
                    <StatusBar
                        backgroundColor={Colors.statusBar}
                        barStyle="light-content"
                    />
                    <HeaderBack
                        onBackPress={() => this.props.navigation.goBack()}
                        headerTitle={Title}
                    />
                    <ScrollView>
                        <FastImage
                            style={styles.image}
                            resizeMode={FastImage.resizeMode.cover}
                            source={{ uri: ImagePath }}
                        />
                           <Text style={styles.Title}>
                            {this.state.detail.Title}
                        </Text>
                        <TouchableOpacity
                      onPress={() => this.PhoneAddLikeNotificationForHelperApi()}
                    >

                        <Icon type="Entypo" name="heart" style={this.state.Like==1 ? styles.IconRed : styles.IconGray}/>
                        </TouchableOpacity>
                        <Text style={styles.desc}>
                            {this.state.detail.Text}
                        </Text>
                    </ScrollView>
                    <Portal>
                    <Dialog
                        visible={this.state.apiError}
                        style={styles.dialogContainer}
                        dismissable
                        onDismiss={() => {
                            this.setState({
                                apiError: false,
                            });
                        }}
                    >
                        <Dialog.Content>
                            <Text style={styles.dialogText}>
                                {this.state.apiErrorDesc}
                            </Text>
                        </Dialog.Content>
                      
                    </Dialog>
                    <Dialog
                            visible={this.state.apiSuccess}
                            style={styles.dialogContainer}
                            onDismiss={() => {
                                this.setState({
                                    apiSuccess: false,
                                });
                            }}
                        >
                         <Dialog.Title style={styles.dialogTitleText}>
                            {this.state.apiSuccessTitle}
                         </Dialog.Title>
                            <Dialog.Content style={styles.dialogContent}>

                               
                                <Text style={styles.dialogText}>
                                    {this.state.apiSuccessDesc}
                                </Text>
                                <Button
                                    buttonText={'بستن'}
                                    onPress={() => {
                                        this.setState({
                                            apiSuccess: false,
                                        });
                                    }}
                                    style={styles.dialogButton}
                                />
                            </Dialog.Content>
                           
                        </Dialog>
                </Portal>
                </View>
            );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    image: {
        height: hp('40%'),
        width: wp('90%'),
        margin: wp('5%'),
        marginBottom: wp('2%')
    },
    Title: {
        fontSize: normalize(15),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '700',
        },
        '@media android': {
            fontFamily: '$IR_B',
        }, 
        textAlign: 'center',
        color: Colors.green,
        margin: wp('5%'),
        marginTop:wp('2%')
    },
    desc: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'right',
        color: Colors.textGray,
        margin: wp('5%'),
        marginTop: wp('2%')

    },
   
    IconRed:{ 
        textAlign: 'center',
        color:Colors.red,
        borderRadius:5
        },
        IconGray:{
           
                textAlign: 'center',
                color:Colors.gray,
                borderRadius:5
        },
   
        dialogContainer: {
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            padding:0
            
        },
        dialogContent:{
            margin:0
        },
        dialogTitleText:{
            width: '100%',
            borderBottomColor: Colors.green,
            borderBottomWidth: 2,
            color:Colors.green,
            '@media ios': {
                fontFamily: '$IR',
                fontWeight: '500',
            },
            '@media android': {
                fontFamily: '$IR_B',
            },
        },
        dialogText: {
            margin:0,
            fontSize: normalize(13),
            '@media ios': {
                fontFamily: '$IR',
                fontWeight: '500',
            },
            '@media android': { 
                fontFamily: '$IR_M',
            },
            textAlign: 'center',
            color: Colors.textBlack,
        },
        dialogButton: {
            width: wp('30%')
        }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(NoticeDetailScreen);
