import React, { Component } from 'react';
import { FlatList, Image, StatusBar, Text, View,BackHandler } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Dialog, Portal } from 'react-native-paper';
import { connect } from 'react-redux';
import { Button, Empty, Loading } from '../../../components';
import HeaderBack from '../../../components/HeaderBack';
import Repository from '../../../repository/Repository';
import { hp, normalize, wp } from '../../../responsive';
import Colors from '../../../utility/Colors';
import Utils from '../../../utility/Utils';

let id = -1;

class WareReportDetailScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoadingPage: true,
            list: [],
            refreshing: false,

            apiError: false,
            apiErrorDesc: ''
        };

        id = this.props.navigation.getParam('DeliveryWasteID');
    }

  
    componentDidMount = () => {
        this.getApi();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      }
      
      handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
      } 
    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount = () => {

    }

    getApi = async () => {
        await this.phoneGetReceivedWareHelperReportItemApi();
    }

    async phoneGetReceivedWareHelperReportItemApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber,
            DeliveryWasteID: id
        };
        try {
            const response = await Repository.PhoneGetReceivedWareHelperReportItemApi(params, headers);
            if (response.Success === 1) {
                this.setState({
                    list: response.Result
                });
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
            });
        } catch (error) {
            await this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }

    handleRefresh = () => {
        this.setState({
            refreshing: true,
            isLoadingPage: true,
            list: [],
        }, () => {
            this.phoneGetReceivedWareHelperReportItemApi();
        });
    }

    renderEmpty = () => {
        return this.state.isLoadingPage ? (
            <View style={{ flex: 1 }}>
                <Loading
                    message={'در حال دریافت اطلاعات'}
                    messageColor={Colors.green}
                />
            </View>
        ) : (
                <View style={{ flex: 1 }}>
                    <Empty />
                </View>
            );
    }

    renderItem(item) {
        const {
            Point,
            Price,
            WasteTypeCount,
            WasteTypeStr
        } = item;
        return (
            <View style={styles.itemContainer}>
                <View style={styles.itemLeftContainer}>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {WasteTypeStr}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'نام پسماند : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {WasteTypeCount}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'وزن : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {Point}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'امتیاز : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {Price}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'مبلغ : '}
                        </Text>
                    </View>
                </View>
                <View style={styles.itemRightContainer}>
                    <Image
                        source={require('./../../../assets/image/ic_pasmand.png')}
                        resizeMode={'contain'}
                        style={styles.itemImage}
                    />
                </View>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                    onBackPress={() => this.props.navigation.goBack()}
                    headerTitle={'جزئیات گزارش کالای دریافتی'}
                />
                <FlatList
                    data={this.state.list}
                    renderItem={({ item }) => this.renderItem(item)}
                    ListEmptyComponent={() => this.renderEmpty()}
                    numColumns={1}
                    keyExtractor={(item, index) => index.toString()}
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.handleRefresh()}
                    contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
                />
                <Portal>
                    <Dialog
                        visible={this.state.apiError}
                        style={styles.dialogContainer}
                        dismissable
                        onDismiss={() => {
                            this.setState({
                                apiError: false
                            });
                        }}
                    >
                        <Dialog.Content>
                            <Text style={styles.dialogText}>
                                {this.state.apiErrorDesc}
                            </Text>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button
                                buttonText={'تلاش مجدد'}
                                onPress={() => {
                                    this.setState({
                                        apiError: false,
                                        isLoadingPage: true
                                    }, () => {
                                        this.getApi();
                                    });
                                }}
                                style={styles.dialogButton}
                            />
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </View>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },

    itemContainer: {
        flexDirection: 'row',
        margin: 10,
        borderRadius: 15,
        borderColor: Colors.gray,
        borderWidth: 1,
        paddingTop: 5,
        paddingBottom: 5
    },
    itemRightContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginLeft: 10
    },
    itemImage: {
        height: wp('12%'),
        width: wp('12%'),
        marginRight: 5
    },
    itemLeftContainer: {
        flex: 1,
    },
    itemTextContainer: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'center'
    },
    itemTextTitle: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
    },
    itemTextDesc: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textGray,
    },

  
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(WareReportDetailScreen);
