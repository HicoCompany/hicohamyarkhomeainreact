import { Tab, Tabs, View } from 'native-base';
import React, { Component } from 'react';
import { StatusBar,BackHandler } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import HeaderBack from '../../../components/HeaderBack';
import TabItem from '../../../components/TabItem';
import { hp, normalize } from '../../../responsive';
import Colors from '../../../utility/Colors';
import Utils from '../../../utility/Utils';
import WareReport from './component/WareReport';
import FinancialReport from './component/FinancialReport';
import WasteReport from './component/WasteReport';

class ReportScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'گزارشات'}
                    source={require('./../../../assets/image/ic_report.png')}
                    color={tintColor}
                />,
        };
    };

    constructor(props) {
        super(props);
        this.tabs = null;
    }

   
    componentDidMount = () => {
        setTimeout(this.tabs.goToPage.bind(this.tabs, 2));
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      }
      
      handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
      } 
    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount = () => {

    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                    onBackPress={() => this.props.navigation.goBack()}
                    headerTitle={'گزارشات'}
                />
                <Tabs
                    ref={tabs => (this.tabs = tabs)}
                    tabContainerStyle={styles.tabs}
                    initialPage={1}
                    tabBarPosition={'top'}
                    tabBarUnderlineStyle={styles.tabBarUnderLineColor}
                    locked
                >
                    <Tab
                        heading={'مالی همیار'}
                        tabStyle={styles.tabStyle}
                        activeTabStyle={styles.tabActiveStyle}
                        textStyle={styles.tabText}
                        activeTextStyle={styles.tabActiveText}
                    >
                        <FinancialReport
                            navigation={this.props.navigation}
                        />
                    </Tab>
                    <Tab
                        heading={'کالاهای دریافتی'}
                        tabStyle={styles.tabStyle}
                        activeTabStyle={styles.tabActiveStyle}
                        textStyle={styles.tabText}
                        activeTextStyle={styles.tabActiveText}
                    >
                        <WareReport
                            navigation={this.props.navigation}
                        />
                    </Tab>
                    <Tab
                        heading={'پسماند تحویلی'}
                        tabStyle={styles.tabStyle}
                        activeTabStyle={styles.tabActiveStyle}
                        textStyle={styles.tabText}
                        activeTextStyle={styles.tabActiveText}
                    >
                        <WasteReport
                            navigation={this.props.navigation}
                        />
                    </Tab>
                </Tabs>
            </View>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    tabs: {
        height: hp('10%'),
        // backgroundColor: 'transparent',
    },
    tabBarUnderLineColor: {
        backgroundColor: 'transparent'
    },
    tabStyle: {
        backgroundColor: '#eeeeee'
    },
    tabActiveStyle: {
        backgroundColor: '#015641'
    },
    tabText: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#212121',
    },
    tabActiveText: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        textAlign: 'center',
        color: '#ffffff',
        fontWeight: 'normal'
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(ReportScreen);
