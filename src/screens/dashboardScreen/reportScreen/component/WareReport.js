import { Text, View } from 'native-base';
import React, { Component } from 'react';
import { ActivityIndicator, FlatList, Image, TouchableOpacity } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import { Empty, Loading } from '../../../../components';
import Repository from '../../../../repository/Repository';
import { hp, normalize, wp } from '../../../../responsive';
import Colors from '../../../../utility/Colors';
import Utils from '../../../../utility/Utils';

class WareReport extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoadingPage: true,
            list: [],
            refreshing: false,
            loadingData: false,

            apiError: false,
            apiErrorDesc: ''
        };
    }

    componentDidMount = () => {
        this.getApi();
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount = () => {

    }

    getApi = async () => {
        await this.phoneGetReceivedWareHelperReportApi();
    }

    async phoneGetReceivedWareHelperReportApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber,
            Page: 1
        };
        try {
            const response = await Repository.PhoneGetReceivedWareHelperReportApi(params, headers);
            if (response.ResultID === 100) {
                const list = response.Result;
                this.setState({
                    list
                });
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
            });
        } catch (error) {
            await this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }

    handleLoadMore = () => {
    }

    handleRefresh = () => {
    }
    renderEmpty = () => {
        return this.state.isLoadingPage ? (
            <View style={{ flex: 1 }}>
                <Loading
                    message={'در حال دریافت اطلاعات'}
                    messageColor={Colors.green}
                />
            </View>
        ) : (
                <View style={{ flex: 1 }}>
                    <Empty />
                </View>
            );
    }
    renderFooter = () => {
        if (!this.state.loadingData) {
            return null;
        }
        return (<ActivityIndicator size={'small'} color={'black'} />);
    }
    renderItem(item) {
        const {
            BoothTitle,
            DeliveryCode,
            DeliveryDateStr,
            DeliveryTime,
            DeliveryTypeStr,
            DeliveryWasteID,
            PersonlName,
            StatusStr,
            TotalCount,
            TotalPoint,
            TotalPrice,
            WareTitle
        } = item;
        return (
            <TouchableOpacity
                style={styles.itemContainer}
                activeOpacity={0.6}
                // onPress={() => {
                //     this.props.navigation.navigate('wareReportDetail', {
                //         DeliveryWasteID
                //     });
                // }}
            >
                <View style={styles.itemLeftContainer}>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {WareTitle}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'نام کالا : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {TotalCount}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'تعداد : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {TotalPrice}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'مبلغ ریالی : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {DeliveryTypeStr}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'وضعیت سفارش : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {PersonlName}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'نام پرسنل : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {DeliveryTypeStr}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'عنوان غرفه : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {`${DeliveryDateStr} ${DeliveryTime}`}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'تاریخ تحویل : '}
                        </Text>
                    </View>
                   
                   
                </View>
                <View style={styles.itemRightContainer}>
                    <Image
                        source={require('./../../../../assets/image/ic_ware.png')}
                        resizeMode={'contain'}
                        style={styles.itemImage}
                    />
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.list}
                    renderItem={({ item }) => this.renderItem(item)}
                    ListEmptyComponent={() => this.renderEmpty()}
                    ListFooterComponent={() => this.renderFooter()}
                    numColumns={1}
                    keyExtractor={(item, index) => item.DeliveryWasteID.toString()}
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.handleRefresh()}
                    onEndReached={() => this.handleLoadMore()}
                    onEndReachedThreshold={0.08}
                    contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
                />
            </View>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    screenHeader: {
        height: hp('7%'),
        backgroundColor: '#424242',
        justifyContent: 'center',
        alignItems: 'center',
    },
    screenHeaderTitle: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#fff',
    },
    title: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
        marginBottom: 10
    },

    itemContainer: {
        flexDirection: 'row',
        margin: 10,
        borderRadius: 5,
        // borderColor: Colors.gray,
        // borderWidth: 1,
        paddingTop: 5,
        paddingBottom: 5,
        shadowColor: '#727272',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    itemRightContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginLeft: 10
    },
    itemImage: {
        height: wp('12%'),
        width: wp('12%'),
        marginRight: 5
    },
    itemLeftContainer: {
        flex: 1,
    },
    itemTextContainer: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'center'
    },
    itemTextTitle: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
    },
    itemTextDesc: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textGray,
    },
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(WareReport);
