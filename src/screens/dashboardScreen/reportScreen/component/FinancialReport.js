import { Text, View } from 'native-base';
import React, { Component } from 'react';
import { ActivityIndicator, FlatList, Image } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import { Empty, Loading } from '../../../../components';
import Repository from '../../../../repository/Repository';
import { hp, normalize, wp } from '../../../../responsive';
import Colors from '../../../../utility/Colors';
import Utils from '../../../../utility/Utils';

class FinancialReport extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoadingPage: true,
            list: [],
            refreshing: false,
            loadingData: false,

            apiError: false,
            apiErrorDesc: ''
        };
    }

    componentDidMount = () => {
        this.getApi();
    }

    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount = () => {

    }

    getApi = async () => {
        await this.phoneGetFinancialHelperReportApi();
    }

    async phoneGetFinancialHelperReportApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber,
            Page: 1
        };
        try {
            const response = await Repository.PhoneGetFinancialHelperReportApi(params, headers);
            if (response.Success === 1) {
                const list = response.Result;
                if (list !== null) {
                    this.setState({
                        list
                    });
                }
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
            });
        } catch (error) {
            console.log('catch PhoneGetDeliveredWasteReportApi', error);
            await this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }

    handleLoadMore = () => {
        // if (this.state.countList > 0) {
        //     this.setState({
        //         offset: this.state.offset + this.state.pageSize, loadingData: true
        //     }, async () => {
        //         await this.phoneGetDeliveredWasteReportApi();
        //     });
        // }
    }

    handleRefresh = () => {
        // this.setState({ 
        //     refreshing: true, 
        //     isLoadingPage: true,
        //     list: [],
        // }, () => {
        //     this.phoneGetFinancialHelperReportApi();
        // });
    }

    renderEmpty = () => {
        return this.state.isLoadingPage ? (
            <View style={{ flex: 1 }}>
                <Loading
                    message={'در حال دریافت اطلاعات'}
                    messageColor={Colors.green}
                />
            </View>
        ) : (
                <View style={{ flex: 1 }}>
                    <Empty />
                </View>
            );
    }

    renderFooter = () => {
        if (!this.state.loadingData) {
            return null;
        }
        return (<ActivityIndicator size={'small'} color={'black'} />);
    }

    renderItem(item) {
        const {
            BoothTitle,
            DeliveryCode,
            DeliveryDateStr,
            DeliveryTime,
            DeliveryTypeStr,
            DeliveryWasteID,
            Discription,
            PersonlName,
            StatusStr,
            TotalCash,
            TotalPrice,
            TypeTitle,
            Cash
        } = item;
        return (
            <View style={styles.itemContainer}>
                <View style={styles.itemLeftContainer}>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {TypeTitle}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'نحوه پرداخت : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {Cash}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'معادل ریالی : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {BoothTitle}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'عنوان غرفه : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {PersonlName}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'پرسنل غرفه : '}
                        </Text>
                    </View>
                   
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {DeliveryDateStr}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'تاریخ پرداخت : '}
                        </Text>
                    </View>
                </View>
                <View style={styles.itemRightContainer}>
                    <Image
                        source={require('./../../../../assets/image/ic_financial.png')}
                        resizeMode={'contain'}
                        style={styles.itemImage}
                    />
                </View>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.list}
                    renderItem={({ item }) => this.renderItem(item)}
                    ListEmptyComponent={() => this.renderEmpty()}
                    ListFooterComponent={() => this.renderFooter()}
                    numColumns={1}
                    keyExtractor={(item, index) => item.DeliveryWasteID.toString()}
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.handleRefresh()}
                    onEndReached={() => this.handleLoadMore()}
                    onEndReachedThreshold={0.08}
                    contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
                />
            </View>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    screenHeader: {
        height: hp('7%'),
        backgroundColor: '#424242',
        justifyContent: 'center',
        alignItems: 'center',
    },
    screenHeaderTitle: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#fff',
    },
    title: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
        marginBottom: 10
    },

    itemContainer: {
        flexDirection: 'row',
        margin: 10,
        borderRadius: 5,
        // borderColor: Colors.gray,
        // borderWidth: 1,
        paddingTop: 5,
        paddingBottom: 5,
        shadowColor: '#727272',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    itemRightContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginLeft: 10
    },
    itemImage: {
        height: wp('12%'),
        width: wp('12%'),
        marginRight: 5
    },
    itemLeftContainer: {
        flex: 1,
    },
    itemTextContainer: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'center'
    },
    itemTextTitle: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
    },
    itemTextDesc: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textGray,
    },
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(FinancialReport);
