import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StatusBar,Alert, BackHandler, DeviceEventEmitter } from 'react-native';
import { Text, Card, CardItem, Container, Content,Badge } from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import { hp, normalize, wp } from '../../../responsive';
import TabItem from '../../../components/TabItem';
import HeaderMain2 from '../../../components/HeaderMain2';
import { exitApp } from '../../../redux/actions';
import Repository from '../../../repository/Repository';

class ManageUserScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingPage: true,
      list: [],
      listRequest: [],
      refreshing: false,
      loadingData: false,
      apiError: false,
      apiErrorDesc: '',
      StatusResult:[]
  };
    this.appTourTargets = [];
  }
    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'مدیریت کاربر '}
                    source={require('./../../../assets/image/TabManageUser.png')}
                    color={tintColor}
                />,
        };
    };
    onLogOutClick() {
      this.props.exitApp();
      this.props.navigation.replace('splash');
  }
    componentWillMount() {
    }
    componentDidMount = () => {
      
      this.getApi();
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  getApi = async () => {
   
    await this.PhoneGetStatusSuggestionForHelperApi();
    }
    async PhoneGetStatusSuggestionForHelperApi() {
      const headers = null;
      const params = {
        Mobile: this.props.userData.mobileNumber,
        Code: this.props.userData.codeNumber
      };
      
      try {
      
        const response = await Repository.PhoneGetStatusSuggestionForHelperApi(params, headers);
        if (response.ResultID === 100) {
            const StatusResult = response.StatusResult[0];
            this.setState({
              StatusResult
            });
        }
        else {
          if(response.Result!=100){
            this.setState({
                apiError: true,
                apiErrorDesc: response.Text
            });
          }
        }
        this.setState({
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
        });
      } catch (error) {
        await this.setState({
            loadingData: false,
            refreshing: false,
            isLoadingPage: false,
            apiError: true,
            apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
        });
      }
      }
    handleBackButton = () => {
     Alert.alert(
         'خارج شدن از برنامه',
         'آیا با خروج از برنامه موافق هستید؟', [{
             text: 'نه',
             onPress: () => console.log('کنسل'),
             style: 'cancel'
         }, {
             text: 'بله',
             onPress: () => BackHandler.exitApp()
         },], {
             cancelable: false
         }
      );
      return true;
    } 
   
    render() {
        return (
        <Container>
          <Content>
            <View style={styles.container}>
              <StatusBar
                  backgroundColor={Colors.statusBar}
                  barStyle="light-content"
              />
              <HeaderMain2
                onMenuPress={() => BackHandler.exitApp()}
                onPinPress={() => this.props.navigation.navigate('boothDynamic')}
                headerTitle={' مديريت كاربر  '}
              />
             
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
                  <TouchableOpacity                    
                    onPress={() => this.props.navigation.navigate('profile')}
                  >
                    <View>
                      <Card style={styles.BoxProduct}>
                        <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                          <Image source={require('./../../../assets/image/InformationUser.png')} style={styles.ImgProduct} />
                        </CardItem>
                        <Text style={styles.textProduct}>{'اطلاعات کاربر '}</Text>
                      </Card>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
                  <TouchableOpacity                    
                    onPress={() => this.props.navigation.navigate('IntroductionScreen')}
                  >
                    <View>
                      <Card style={styles.BoxProduct}>
                        <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                          <Image source={require('./../../../assets/image/InvitedToFriend.png')} style={styles.ImgProduct} />
                        </CardItem>
                        <Text style={styles.textProduct}>{'معرفی به دوستان'}</Text>
                      </Card>
                    </View>
                  </TouchableOpacity>
                </View>
               
              </View>
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
                  <TouchableOpacity                    
                    onPress={() => this.props.navigation.navigate('AboutScreen')}
                  >
                    <View>
                      <Card style={styles.BoxProduct}>
                        <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                          <Image source={require('./../../../assets/image/AboutUs.png')} style={styles.ImgProduct} />
                        </CardItem>
                        <Text style={styles.textProduct}>{'درباره ما'}</Text>
                      </Card>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
                  <TouchableOpacity                    
                    onPress={() => this.props.navigation.navigate('SuggestionListScreen')}
                  >
                   <View>
                      <Card style={styles.BoxProduct}>
                      
                        <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                        <Badge>
        <Text>{this.state.StatusResult.PostedAndUnread}</Text>
          </Badge>
                          <Image source={require('./../../../assets/image/suggestion.png')} style={styles.ImgProduct} />
                        </CardItem>
                        <Text style={styles.textProduct}>{'انتقادات و پشنهادات '}</Text>
                      
                      </Card>
                    </View>
                  </TouchableOpacity>
                </View>
               
              </View>
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
                  <TouchableOpacity                    
                    onPress={() => this.props.navigation.navigate('GuidScreen')}
                  >
                    <View>
                      <Card style={styles.BoxProduct}>
                        <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                          <Image source={require('./../../../assets/image/Guid.png')} style={styles.ImgProduct} />
                        </CardItem>
                        <Text style={styles.textProduct}>{'راهنما'}</Text>
                      </Card>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
                  <TouchableOpacity                    
                    onPress={() => this.onLogOutClick()}
                  >
                    <View>
                      <Card style={styles.BoxProduct}>
                        <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                          <Image source={require('./../../../assets/image/logout.png')} style={styles.ImgProduct} />
                        </CardItem>
                        <Text style={styles.textProduct}>{'خروج از حساب کاربری'}</Text>
                      </Card>
                    </View>
                  </TouchableOpacity>
                </View>
               
              </View>
          </View>
        </Content>
      </Container>
        );
    }
}

  export 
  const styles = EStyleSheet.create({
    
  top: {
    flex: 1
  },
  center: {
    flex: 1
  },
  bottom: {
    flex: 1
  },
    IRANSansNormal: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
     },
     IRANSansBold: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
     },
     header: {
      backgroundColor: '#1a237e',
     },
     badge: {
      position: 'absolute',
      top: 8, 
      right: 10, 
  },
      BtnImg: {
        width: '100%',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
      },
      textBtn: {
        color: 'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
        fontSize: 14,
      },
      bgProduct: {
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: '#fafafa',
        borderBottomWidth: 1,
        borderBottomColor: '#f0f0f0'
      },
      BoxProduct: {
        width: 160,
        height: 160,
        backgroundColor: 'white',
        alignItems: 'center',
      },
      ImgProduct: {
        width: '85%',
        height: 80,
        resizeMode: 'contain',
       color: Colors.black
      },
      textProduct: {
        color: 'black',
        fontSize: 14,
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
      },
      textGreen: {
        color: '#2e7d32',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
      },
      textRed: {
        color: '#e53935',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        textDecorationColor: '#000'
      },
      BtnProduct: {
          borderWidth: 1,
          borderColor: '#f0f0f0',
          height: 40,
          justifyContent: 'center',
         alignItems: 'center'
      },
      BtnProductOne: {
       flex: 2,
       justifyContent: 'center',
       alignItems: 'center',
       
       
       backgroundColor: 'green'
      },
      BtnProductTwo: {
        flex: 1, 
backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomRightRadius: 10,
        borderTopRightRadius: 10,
      },
      textBtnProduct: {
       color: 'white',
       '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
       fontSize: 11,
      },
      BoxBtnSort: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.5, 
        borderColor: '#bdbbbb',
        width: 330,
      },
      BtnSort: {
          width: 160,
          height: 40,
        paddingHorizontal: 4,
        backgroundColor: '#fafafa',
        alignItems: 'center',
      }
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {
  exitApp
};
export default connect(mapStateToProps, mapActionToProps)(ManageUserScreen);
