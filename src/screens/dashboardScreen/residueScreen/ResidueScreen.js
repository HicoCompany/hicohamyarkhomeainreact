import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar ,StyleSheet,BackHandler} from 'react-native';
import { Text,Card, CardItem,Container, Content, } from 'native-base';

import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import { normalize } from '../../../responsive';
import TabItem from '../../../components/TabItem';
import HeaderMain from '../../../components/HeaderMain';

class ResidueScreen extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
        tabBarIcon: ({ tintColor }) =>
            <TabItem
                name={'ارائه پسماند'}
                source={require('./../../../assets/image/ic_residue.png')}
                color={tintColor}
            />,
    };
};

    render() {
        return (
            <Container>
          <Content>

            <View style={styles.container}>
            <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderMain
                    onMenuPress={() => this.props.navigation.toggleDrawer()}
                />
               
            <View style={{flex:1,flexDirection:'row',paddingTop:5}}>
                <View style={{flex:1,flexDirection:'column',paddingRight:10}} >
            <TouchableOpacity
                style={[styles.BtnDetails]}
                onPress={() => this.props.navigation.navigate("DeliveryTimeScreen")}
              >
            <View>
              <Card style={styles.BoxProduct}>
                  <CardItem header style={{borderBottomWidth:1,borderBottomColor:'gray'}}>
                
                     <Image source={require('./../../../assets/image/PasmanVizeh.png')} style={styles.ImgProduct}/>
                     </CardItem>
                     <CardItem style={{flex:1,flexDirection:'column',alignItems: 'center' }}>
              <View style={{alignItems: 'center',}}>
              <Text style={styles.textProduct}> پسماند ویژه </Text>
              </View>
              <View style={{flex:1,flexDirection:'row',alignItems: 'center',}}>
              <Text style={[styles.textProduct,styles.textGreen]}> امتیاز </Text>
              <Text style={[styles.textProduct,styles.textGreen]}>300</Text>
              </View>
              </CardItem>
            </Card>
              </View>
            </TouchableOpacity>
             
             
            <View>
                    <TouchableOpacity
                style={[styles.BtnProduct]}
                onPress={this.onPress}
              >
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={[styles.BtnProductOne]}>
                 <Text style={[styles.textBtnProduct]}>افزودن</Text>
              </View>
              <View style={[styles.BtnProductTwo]}>
              <Text style={[styles.textBtnProduct,{color:'black'}]}> 1 عدد</Text>
              </View>
              </View>
           
            </TouchableOpacity>
              </View>
            </View>
                <View style={{flex:1,flexDirection:'column',paddingRight:10}} >
            <TouchableOpacity
                style={[styles.BtnDetails]}
                onPress={() => this.props.navigation.navigate("DeliveryTimeScreen")}
              >
            <View>
              <Card style={styles.BoxProduct}>
                  <CardItem header style={{borderBottomWidth:1,borderBottomColor:'gray'}}>
               
                     <Image source={require('./../../../assets/image/Shishe.png')} style={styles.ImgProduct}/>
                     </CardItem>
                     <CardItem style={{flex:1,flexDirection:'column',alignItems: 'center' }}>
              <View style={{alignItems: 'center',}}>
              <Text style={styles.textProduct}> شیشه </Text>
              </View>
              <View style={{flex:1,flexDirection:'row',alignItems: 'center',}}>
              <Text style={[styles.textProduct,styles.textGreen]}> امتیاز </Text>
              <Text style={[styles.textProduct,styles.textGreen]}>200</Text>
              </View>
              </CardItem>
            </Card>
              </View>
            </TouchableOpacity>
             
            <View>
                    <TouchableOpacity
                style={[styles.BtnProduct]}
                onPress={this.onPress}
              >
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={[styles.BtnProductOne]}>
                 <Text style={[styles.textBtnProduct]}> افزودن </Text>
              </View>
              <View style={[styles.BtnProductTwo]}>
              <Text style={[styles.textBtnProduct,{color:'black'}]}> ۱ عدد</Text>
              </View>
              </View>
           
            </TouchableOpacity>
              </View>
            </View>
            </View>

            <View style={{flex:1,flexDirection:'row',paddingTop:5}}>
                <View style={{flex:1,flexDirection:'column',paddingRight:10}} >
            <TouchableOpacity
                style={[styles.BtnDetails]}
                onPress={() => this.props.navigation.navigate("DeliveryTimeScreen")}
              >
            <View>
              <Card style={styles.BoxProduct}>
                  <CardItem header style={{borderBottomWidth:1,borderBottomColor:'gray'}}>
                  
                     <Image source={require('./../../../assets/image/KaghazKarton.png')} style={styles.ImgProduct}/>
                     </CardItem>
                     <CardItem style={{flex:1,flexDirection:'column',alignItems: 'center' }}>
              <View style={{alignItems: 'center',}}>
              <Text style={styles.textProduct}>کاغذ و کارتن </Text>
              </View>
              <View style={{flex:1,flexDirection:'row',alignItems: 'center',}}>
              <Text style={[styles.textProduct,styles.textGreen]}> امتیاز </Text>
              <Text style={[styles.textProduct,styles.textGreen]}>100</Text>
              </View>
              </CardItem>
            </Card>
              </View>
            </TouchableOpacity>
             
            <View>
                    <TouchableOpacity
                style={[styles.BtnProduct]}
                onPress={this.onPress}
              >
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={[styles.BtnProductOne]}>
                 <Text style={[styles.textBtnProduct]}> افزودن </Text>
              </View>
              <View style={[styles.BtnProductTwo]}>
              <Text style={[styles.textBtnProduct,{color:'black'}]}>  ۱ عدد </Text>
              </View>
              </View>
           
            </TouchableOpacity>
              </View>
            </View>
                <View style={{flex:1,flexDirection:'column',paddingRight:10}} >
            <TouchableOpacity
                style={[styles.BtnDetails]}
                onPress={() => this.props.navigation.navigate("DeliveryTimeScreen")}
              >
            <View>
              <Card style={styles.BoxProduct}>
                  <CardItem header style={{borderBottomWidth:1,borderBottomColor:'gray'}}>
                  
                     <Image source={require('./../../../assets/image/MakhlotKhoshk.png')} style={styles.ImgProduct}/>
                     </CardItem>
                     <CardItem style={{flex:1,flexDirection:'column',alignItems: 'center' }}>
              <View style={{alignItems: 'center',}}>
              <Text style={styles.textProduct}>مخلوط خشک </Text>
              </View>
              <View style={{flex:1,flexDirection:'row',alignItems: 'center',}}>
              <Text style={[styles.textProduct,styles.textGreen]}> امتیاز </Text>
              <Text style={[styles.textProduct,styles.textGreen]}>400</Text>
              </View>
              </CardItem>
            </Card>
              </View>
            </TouchableOpacity>
             
            <View>
                    <TouchableOpacity
                style={[styles.BtnProduct]}
                onPress={this.onPress}
              >
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={[styles.BtnProductOne]}>
                 <Text style={[styles.textBtnProduct]}> افزودن   </Text>
              </View>
              <View style={[styles.BtnProductTwo]}>
              <Text style={[styles.textBtnProduct,{color:'black'}]}>  ۱ عدد </Text>
              </View>
              </View>
           
            </TouchableOpacity>
              </View>
            </View>
            </View>
            <TouchableOpacity
                style={[styles.BtnProduct]}
                onPress={() => this.props.navigation.navigate("DeliveryTimeScreen")}
              >
              <View style={{flex:1,flexDirection:'row',}}>
                <View style={[styles.BtnProductOne]}>
                 <Text style={[styles.textBtnProduct]}> مرحله بعد   </Text>
              </View>
              
              </View>
           
            </TouchableOpacity>
            </View>
            </Content>
      </Container>
        );
    }

}

  export 
  const styles = EStyleSheet.create({
    IRANSansNormal: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
     },
     IRANSansBold: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
     },
     header:{
      backgroundColor:'#1a237e',
     },
     badge:{
      position: 'absolute',
      top: 8, 
      right: 10, 
  },
      BtnImg:{
        width:'100%',
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
      },
      textBtn:{
        color:'white',
        fontSize: normalize(17),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
      },
      bgProduct:{
        paddingHorizontal:10,
        paddingVertical:10,
        backgroundColor:'#fafafa',
        borderBottomWidth:1,
        borderBottomColor:"#f0f0f0"
      },
      BoxProduct:{
        width:160,
        height:230,
        backgroundColor: 'white',
        alignItems: 'center',
      },
      ImgProduct:{
        width:'85%',
        height: 80,
        resizeMode:'contain',
       
      },
      textProduct: {
        color: 'black',
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
      },
      textGreen:{
        color: '#2e7d32',
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
      },
      textRed:{
        color: '#e53935',
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        textDecorationColor: "#000"
      },
      BtnProduct:{
          borderWidth:1,
          borderColor:'#f0f0f0',
          height:40,
          justifyContent:"center",
         alignItems:"center"
      },
      BtnProductOne:{
       flex:2,
       justifyContent:"center",
       alignItems:"center",
       
       
       backgroundColor:'green'
      },
      BtnProductTwo:{
        flex:1,backgroundColor:'white',
        justifyContent:"center",
        alignItems:"center",
        borderBottomRightRadius:10,
        borderTopRightRadius:10,
      },
      textBtnProduct:{
       color:'white',
       fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
    },
      BoxBtnSort:{
        flex:1,
        flexDirection:'row',
        justifyContent:"center",
        alignItems:"center",
        borderWidth:0.5, 
        borderColor:"#bdbbbb",
        width:330,
      },
      BtnSort:{
          width:160,
          height:40,
        paddingHorizontal:4,
        backgroundColor:'#fafafa',
        alignItems:"center",
      }
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(ResidueScreen);
