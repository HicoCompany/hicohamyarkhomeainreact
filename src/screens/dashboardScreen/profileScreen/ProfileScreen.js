import { Icon, Picker } from 'native-base';
import React, { Component } from 'react';
import { ScrollView, StatusBar, Text, TextInput, TouchableOpacity, View,BackHandler } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import DeviceInfo from 'react-native-device-info';
import { Portal, Dialog } from 'react-native-paper';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import { Button, Loading } from '../../../components';
import HeaderBack from '../../../components/HeaderBack';
import TabItem from '../../../components/TabItem';
import { setUser } from '../../../redux/actions';
import Repository from '../../../repository/Repository';
import { hp, normalize, wp } from '../../../responsive';
import Colors from '../../../utility/Colors';
import Utils from '../../../utility/Utils';

class ProfileScreen extends Component {
 
    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor}) =>
                <TabItem
                    name={'پروفایل'}
                    source={require('./../../../assets/image/ic_home.png')}
                    color={tintColor}
                />
        };
    };
    constructor(props) {
        super(props);
        const {
            AccountNumber,
            AccountOwner,
            Adress,
            BankID,
            Email,
            Family,
            MainAreaID,
            Name,
            NationalCode,
            Sex,
            ShabaNumber,
        } = this.props.userData.user;
        this.state = {
            isLoadingPage: true,
            isLoadingButton: false,
            isChangeActive: false,
            Name,
            Family,
            NationalCode,
            MainAreaID,
            Email,
            ShabaNumber,
            AccountOwner,
            AccountNumber,
            BankID,
            Adress,
            Sex,
            bankList: [],
            areaList: [],
            birthYear: '',
            birthMonth: '',
            birthDate: '',
            errorName: '',
            errorFamily: '',
            errorBirth: '',
            errorEmail: '',
            errorBank: '',
            errorNationalCode: '',
            errorAdress: '',
            DeliveryCount: 0,
            PackageCount: 0,
            Points: 0,
            Price: 0,
            RequestDeliveryCount: 0,
            apiSuccess: false,
            apiError: false,
            apiErrorDesc: ''
        };
    }
    componentDidMount = () => {
        this.getApi();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      }
      
      handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
      } 
    onConfirm = () => {
        const {
            Name,
            Family,
            NationalCode,
            Email,
            ShabaNumber,
            AccountOwner,
            AccountNumber,
            BankID,
            Adress,
            birthYear,
            birthMonth,
            birthDate,
        } = this.state;

        let error = false;

        if (Name.length === 0) {
            error = true;
            this.setState({
                errorName: 'لطفا نام خود را وارد کنید.'
            });
        }

        if (Family.length === 0) {
            error = true;
            this.setState({
                errorFamily: 'لطفا نام خانوادگی خود را وارد کنید.'
            });
        }

        if (NationalCode.length === 0) {
            error = true;
            this.setState({
                errorNationalCode: 'لطفا کد ملی خود را وارد کنید.'
            });
        }

        // if (Email.length === 0) {
        //     error = true;
        //     this.setState({
        //         errorEmail: 'لطفا ایمیل خود را وارد کنید.'
        //     });
        // }

        // if (ShabaNumber.length === 0 || AccountOwner.length === 0) {
        //     error = true;
        //     this.setState({
        //         errorBank: 'اطلاعات بانکی کامل وارد نشده است'
        //     });
        // }

        if (Adress.length === 0) {
            error = true;
            this.setState({
                errorAdress: 'لطفا آدرس خود را وارد کنید'
            });
        }
        if (error) {
            return;
        }
        if (!error) {
           
            this.setState({
                isLoadingButton: true,
                isChangeActive: false,
                apiSuccess:false,

            }, () => {
                this.phoneUpdateNaturalPersonApi();
                
            });
        }
    }
    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }
    getApi = async () => {
        await this.phoneGetInfoNaturalPersonApi();
        await this.phoneGetInfoCooperationApi();
        await this.getAreaApi();
        await this.getBankApi();
    }
    async phoneUpdateNaturalPersonApi() {
        
        const deviceId = DeviceInfo.getUniqueID();
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber,
            Name: this.state.Name,
            Family: this.state.Family,
            NationalCode: this.state.NationalCode,
            MainAreaID: this.state.MainAreaID,
            DeviceID: deviceId,
            Email: this.state.Email,
            ShabaNumber: this.state.ShabaNumber,
            AccountOwner: this.state.AccountOwner,
            AccountNumber: '',
            BankID: this.state.BankID,
            Adress: this.state.Adress,
            Sex: this.state.Sex,
            BirthDay: `${this.state.birthYear}-${this.state.birthMonth}-${this.state.birthDate}`
        };
        try {
            const response = await Repository.PhoneUpdateNaturalPersonApi(params, headers);
            if (response.Success === 1) {
                this.setState({
                    apiSuccess: true,
                    apiSuccessDesc:response.Text,
                    apiSuccessTitle:response.Titel

                });
                this.props.setIsLogin();
               this.getApi();
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            if (this.mounted) {
                this.setState({
                    isLoadingButton: false,
                });
            }
        } catch (error) {
            if (this.mounted) {
                this.setState({
                    isLoadingButton: false,
                    apiError: true,
                    apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
                });
            }
        }
    }
    async getBankApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber
        };
        try {
            const response = await Repository.PhoneGetBankApi(params, headers);

            if (response.Success === 1) {
                const list = response.Result;
                this.setState({
                    bankList: list,
                });
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                isLoadingPage: false,
            });
        } catch (error) {
            this.setState({
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }
    async getAreaApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber
        };
        try {
            const response = await Repository.PhoneGetMainAreasApi(params, headers);

            if (response.Success === 1) {
                const list = response.Result;
                this.setState({
                    areaList: list,
                    MainAreaID:list.filter(el=>el.Selected==1).length > 0 ? list.filter(el=>el.Selected==1)[0].MainAreaCode : 1
                });
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                isLoadingPage: false,
            });
        } catch (error) {
            this.setState({
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }
    async phoneGetInfoNaturalPersonApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber,
        };
        try {
            const response = await Repository.PhoneGetInfoNaturalPersonApi(params, headers);
            if (response.Success === 1) {
                this.props.setUser(response.Result);
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                isLoadingPage: false,
            });
        } catch (error) {
            await this.setState({
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }
    async phoneGetInfoCooperationApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber,
        };
        try {
            const response = await Repository.PhoneGetInfoCooperationApi(params, headers);
            if (response.Success === 1) {
                const {
                    DeliveryCount,
                    PackageCount,
                    Points,
                    Price,
                    RequestDeliveryCount
                } = response.Result;
                this.setState({
                    DeliveryCount,
                    PackageCount,
                    Points,
                    Price,
                    RequestDeliveryCount
                });
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                isLoadingPage: false,
            });
        } catch (error) {
            await this.setState({
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }
    renderBankPickerItem() {
        return this.state.bankList.map(bank =>
            <Picker.Item key={bank.BankID.toString()} label={`${bank.Title}`} value={bank.BankID} />
        );
    }
    renderAreaPickerItem() {
        return this.state.areaList.map(area =>
            <Picker.Item key={area.MainAreaCode.toString()} label={`${area.Title}`} value={area.MainAreaCode} />
        );
    }
    render() {
        const {
            Mobile
        } = this.props.userData.user;
        return this.state.isLoadingPage ? (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.green}
                    barStyle="light-content"
                />
                 <HeaderBack
                        onBackPress={() => this.props.navigation.goBack()}
                        headerTitle={'پروفایل کاربران '}
                    />
                <Loading
                    message={'در حال دریافت اطلاعات'}
                    messageColor={Colors.green}
                />
            </View>
        ) :
            (
                <View style={styles.container}>
                    <StatusBar
                        backgroundColor={Colors.statusBar}
                        barStyle="light-content"
                    />
                     <HeaderBack
                        onBackPress={() => this.props.navigation.goBack()}
                        headerTitle={'پروفایل کاربران '}
                    />
                    <ScrollView>
                        <View style={styles.dashboardItemContainer}>
                            <View style={styles.dashboardItemHelpContainer}>
                                <Text style={styles.dashboardItemTextBoldLarge}>
                                    {'همیاری های شما'}
                                </Text>
                                <Text style={styles.dashboardItemTextBoldLarge}>
                                    {`${this.state.RequestDeliveryCount} عدد`}
                                </Text>
                                {/* <TouchableOpacity
                                    style={styles.dashboardItemDetailButton}
                                >
                                    <Text style={styles.dashboardItemTextGray}>
                                        {'مشاهده جزئیات'}
                                    </Text>
                                </TouchableOpacity> */}
                            </View>
                            <View style={styles.dashboardItemRankContainer}>
                                <Text style={styles.dashboardItemTextBoldLarge}>
                                    {'امتیاز های شما'}
                                </Text>
                                <Text style={styles.dashboardItemTextBoldLarge}>
                                    {this.state.Points}
                                </Text>
                                {/* <TouchableOpacity
                                    style={styles.dashboardItemDetailButton}
                                >
                                    <Text style={styles.dashboardItemTextGray}>
                                        {'مشاهده جزئیات'}
                                    </Text>
                                </TouchableOpacity> */}
                            </View>
                            <View style={styles.dashboardItemUserContainer}>
                                <View style={styles.userImageContainer} >
                                    <FastImage
                                        source={require('./../../../assets/image/ic_profile.png')}
                                        resizeMode={FastImage.resizeMode.contain}
                                        style={styles.userImage}
                                    />
                                </View>
                                <Text style={styles.dashboardItemTextBoldSamll}>
                                    {`${this.state.Name} ${this.state.Family}`}
                                </Text>
                                <Text style={styles.dashboardItemTextBoldSamll}>
                                    {Mobile}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.separator} />
                        <Text style={styles.inputTitle}>
                            {'نام'}
                        </Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'نام خود را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'default'}
                                autoFocus={false}
                                style={styles.input}
                                value={this.state.Name}
                                onChangeText={(Name) => {
                                    this.setState({
                                        Name,
                                        errorName: ''
                                    });
                                }}
                                editable={this.state.isChangeActive}
                            />
                            <FastImage
                                source={require('./../../../assets/image/ic_person.png')}
                                resizeMode={FastImage.resizeMode.contain}
                                style={styles.inputIcon}
                            />
                        </View>
                        {this.state.errorName.length > 0 &&
                            <Text style={styles.error}>
                                {this.state.errorName}
                            </Text>
                        }
                        <Text style={styles.inputTitle}>
                            {'نام خانوادگی'}
                        </Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'نام خانوادگی را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'default'}
                                autoFocus={false}
                                style={styles.input}
                                value={this.state.Family}
                                onChangeText={(Family) => {
                                    this.setState({
                                        Family,
                                        errorFamily: ''
                                    });
                                }}
                                editable={this.state.isChangeActive}
                            />
                            <FastImage
                                source={require('./../../../assets/image/ic_person.png')}
                                resizeMode={FastImage.resizeMode.contain}
                                style={styles.inputIcon}
                            />
                        </View>
                        {this.state.errorFamily.length > 0 &&
                            <Text style={styles.error}>
                                {this.state.errorFamily}
                            </Text>
                        }
                        <Text style={styles.inputTitle}>
                            {'کد ملی'}
                        </Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'کد ملی را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'default'}
                                autoFocus={false}
                                style={styles.input}
                                value={this.state.NationalCode}
                                onChangeText={(NationalCode) => {
                                    this.setState({
                                        NationalCode,
                                        errorNationalCode: ''
                                    });
                                }}
                                editable={this.state.isChangeActive}
                            />
                            <FastImage
                                source={require('./../../../assets/image/ic_person.png')}
                                resizeMode={FastImage.resizeMode.contain}
                                style={styles.inputIcon}
                            />
                        </View>
                        {this.state.errorNationalCode.length > 0 &&
                            <Text style={styles.error}>
                                {this.state.errorNationalCode}
                            </Text>
                        }
                        <Text style={styles.inputTitle}>
                            {'شماره همراه'}
                        </Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'شماره همراه خود را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'default'}
                                autoFocus={false}
                                style={styles.input}
                                editable={false}
                                value={Mobile}
                            />
                            <FastImage
                                source={require('./../../../assets/image/ic_mobile.png')}
                                resizeMode={FastImage.resizeMode.contain}
                                style={styles.inputIcon}
                            />
                        </View>
                        {/* <Text style={styles.inputTitle}>
                            {'تاریخ تولد'}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignSelf: 'center'
                            }}
                        >
                            <TextInput
                                placeholder={'سال : 1345'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'numeric'}
                                autoFocus={false}
                                style={styles.inputBirthDay}
                                maxLength={4}
                                value={this.state.birthYear}
                                onChangeText={(birthYear) => {
                                    this.setState({
                                        birthYear,
                                        errorBirth: ''
                                    });
                                }}
                                editable={this.state.isChangeActive}
                            />
                            <TextInput
                                placeholder={'ماه : 01'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'numeric'}
                                autoFocus={false}
                                style={styles.inputBirthDay}
                                maxLength={2}
                                value={this.state.birthMonth}
                                onChangeText={(birthMonth) => {
                                    this.setState({
                                        birthMonth,
                                        errorBirth: ''
                                    });
                                }}
                                editable={this.state.isChangeActive}
                            />
                            <TextInput
                                placeholder={'روز: 01'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'numeric'}
                                autoFocus={false}
                                style={styles.inputBirthDay}
                                maxLength={2}
                                value={this.state.birthDate}
                                onChangeText={(birthDate) => {
                                    this.setState({
                                        birthDate,
                                        errorBirth: ''
                                    });
                                }}
                                editable={this.state.isChangeActive}
                            />
                        </View> */}
                        <Text style={styles.inputTitle}>
                            {'ایمیل'}
                        </Text>
                        <View style={styles.inputContainer}>
                            <TextInput
                                placeholder={'ایمیل خود را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'default'}
                                autoFocus={false}
                                style={styles.input}
                                value={this.state.Email}
                                onChangeText={(Email) => {
                                    this.setState({
                                        Email,
                                        errorEmail: ''
                                    });
                                }}
                                editable={this.state.isChangeActive}
                            />
                            <FastImage
                                source={require('./../../../assets/image/ic_at.png')}
                                resizeMode={FastImage.resizeMode.contain}
                                style={styles.inputIcon}
                            />
                        </View>
                        {this.state.errorEmail.length > 0 &&
                            <Text style={styles.error}>
                                {this.state.errorEmail}
                            </Text>
                        }
                        <Text style={styles.inputTitle}>
                            {'اطلاعات حساب بانکی'}
                        </Text>
                        <TextInput
                            placeholder={'شماره شبا'}
                            placeholderTextColor={Colors.textGray}
                            selectionColor={Colors.textBlack}
                            keyboardType={'default'}
                            autoFocus={false}
                            style={styles.inputSingle}
                            value={this.state.ShabaNumber}
                            onChangeText={(ShabaNumber) => {
                                this.setState({
                                    ShabaNumber,
                                    errorBank: ''
                                });
                            }}
                            editable={this.state.isChangeActive}
                        />
                        <TextInput
                            placeholder={'نام صاحب حساب'}
                            placeholderTextColor={Colors.textGray}
                            selectionColor={Colors.textBlack}
                            keyboardType={'default'}
                            autoFocus={false}
                            style={styles.inputSingle}
                            value={this.state.AccountOwner}
                            onChangeText={(AccountOwner) => {
                                this.setState({
                                    AccountOwner,
                                    errorBank: ''
                                });
                            }}
                            editable={this.state.isChangeActive}
                        />
                        <View style={styles.pickerContainer}>
                            <Picker
                                style={styles.pickerStyle}
                                textStyle={styles.pickerTextStyle}
                                iosHeader="بانک را انتخاب کنید"
                                headerBackButtonText="بستن"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                headerStyle={styles.pickerHeaderStyle}
                                headerTitleStyle={styles.pickerHeaderTextStyle}
                                headerBackButtonTextStyle={styles.pickerBackHeaderTextStyle}
                                itemStyle={styles.pickerItemStyle}
                                itemTextStyle={styles.pickerItemTextStyle}
                                selectedValue={this.state.BankID}
                                onValueChange={(BankID) => {
                                    this.setState({ BankID });
                                }}
                                mode={'dropdown'}
                                enabled={this.state.isChangeActive}
                            >
                                {this.renderBankPickerItem()}
                            </Picker>
                        </View>
                        {this.state.errorBank.length > 0 &&
                            <Text style={styles.error}>
                                {this.state.errorBank}
                            </Text>
                        }
                        <Text style={styles.inputTitle}>
                            {'جنسیت'}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignSelf: 'center'
                            }}
                        >
                            <TouchableOpacity
                                style={this.state.Sex === 2 ? styles.sexActive : styles.sexDeactive}
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState({
                                        Sex: 2
                                    });
                                }}
                                disabled={!this.state.isChangeActive}
                            >
                                <Text style={this.state.Sex === 2 ? styles.sexActiveText : styles.sexDeactiveText}>
                                    {'زن'}
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={this.state.Sex === 1 ? styles.sexActive : styles.sexDeactive}
                                activeOpacity={0.6}
                                onPress={() => {
                                    this.setState({
                                        Sex: 1
                                    });
                                }}
                                disabled={!this.state.isChangeActive}
                            >
                                <Text style={this.state.Sex === 1 ? styles.sexActiveText : styles.sexDeactiveText}>
                                    {'مرد'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.inputTitle}>
                            {'منطقه'}
                        </Text>
                        <View style={styles.pickerContainer}>
                            <Picker
                                style={styles.pickerStyle}
                                textStyle={styles.pickerTextStyle}
                                iosHeader="منطقه را انتخاب کنید"
                                headerBackButtonText="بستن"
                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                headerStyle={styles.pickerHeaderStyle}
                                headerTitleStyle={styles.pickerHeaderTextStyle}
                                headerBackButtonTextStyle={styles.pickerBackHeaderTextStyle}
                                itemStyle={styles.pickerItemStyle}
                                itemTextStyle={styles.pickerItemTextStyle}
                                selectedValue={this.state.MainAreaID}
                                onValueChange={(MainAreaID) => {
                                    this.setState({ MainAreaID });
                                }}
                                mode={'dropdown'}
                                enabled={this.state.isChangeActive}
                            >
                                {this.renderAreaPickerItem()}
                            </Picker>
                        </View>
                        <Text style={styles.inputTitle}>
                            {'آدرس'}
                        </Text>
                        <View style={styles.inputAddressContainer}>
                            <TextInput
                                placeholder={'آدرس خود را وارد کنید'}
                                placeholderTextColor={Colors.textGray}
                                selectionColor={Colors.textBlack}
                                keyboardType={'default'}
                                autoFocus={false}
                                style={styles.inputAddress}
                                value={this.state.Adress}
                                onChangeText={(Adress) => {
                                    this.setState({
                                        Adress,
                                        errorAdress: ''
                                    });
                                }}
                                editable={this.state.isChangeActive}
                            />
                            <FastImage
                                source={require('./../../../assets/image/ic_pin.png')}
                                resizeMode={FastImage.resizeMode.contain}
                                style={styles.inputIcon}
                            />
                        </View>
                        {this.state.errorAdress.length > 0 &&
                            <Text style={styles.error}>
                                {this.state.errorAdress}
                            </Text>
                        }
                        {this.state.isChangeActive &&
                            <Button
                                buttonText={'اعمال تغییرات'}
                                style={styles.button}
                                onPress={() => {
                                    this.onConfirm();
                                }}
                                // onPress={() => {
                                //     this.setState(prevState => {
                                //         return {
                                //             isChangeActive: !prevState.isChangeActive
                                //         };
                                //     });
                                // }}
                            />
                            ||
                            <Button
                                buttonText={'تغییر اطلاعات'}
                                style={styles.button}
                                onPress={() => {
                                    this.setState(prevState => {
                                        return {
                                            isChangeActive: !prevState.isChangeActive
                                        };
                                    });
                                }}
                            />
                        }
                    </ScrollView>
                    <Portal>
                        <Dialog
                            visible={this.state.apiError}
                            style={styles.dialogContainer}
                            dismissable={false}
                        >
                            <Dialog.Content>
                                <Text style={styles.dialogText}>
                                    {this.state.apiErrorDesc}
                                </Text>
                            </Dialog.Content>
                            <Dialog.Actions>
                                <Button
                                    buttonText={'تلاش مجدد'}
                                    onPress={() => {
                                        this.setState({
                                            apiError: false,
                                            isLoadingPage: true,
                                            list: []
                                        }, () => {
                                            this.getApi();
                                        });
                                    }}
                                    style={styles.dialogButton}
                                />
                            </Dialog.Actions>
                        </Dialog>
                        <Dialog
                            visible={this.state.apiSuccess}
                            style={styles.dialogContainer}
                            onDismiss={() => {
                                this.setState({
                                    apiSuccess: false,
                                });
                                this.getApi();
                            }}
                        >
                         <Dialog.Title style={styles.dialogTitleText}>
                            {this.state.apiSuccessTitle}
                         </Dialog.Title>
                            <Dialog.Content style={styles.dialogContent}>

                               
                                <Text style={styles.dialogText}>
                                    {this.state.apiSuccessDesc}
                                </Text>
                                <Button
                                    buttonText={'بستن'}
                                    onPress={() => {
                                        this.setState({
                                            apiSuccess: false,
                                        });
                                        this.getApi();
                                    }}
                                    style={styles.dialogButton}
                                />
                            </Dialog.Content>
                           
                        </Dialog>
                    </Portal>
                </View>
            );
    }
}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
   
    //dashboard style
    dashboardItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: hp('20%'),
    },
    dashboardItemUserContainer: {
        justifyContent: 'center'
    },
    userImageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: wp('20%'),
        width: wp('20%'),
        borderRadius: wp('10%'),
        borderColor: Colors.textGray,
        borderWidth: 1,
        overflow: 'hidden',
    },
    userImage: {
        height: wp('20%'),
        width: wp('20%'),
    },
    dashboardItemRankContainer: {
        justifyContent: 'center'
    },
    dashboardItemHelpContainer: {
        justifyContent: 'center'
    },
    dashboardItemTextBoldSamll: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dashboardItemTextBoldLarge: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dashboardItemTextGray: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textGray,
    },
    dashboardItemDetailButton: {
        padding: 5,
        borderRadius: 5,
        borderColor: Colors.textGray,
        borderWidth: 1
    },
    separator: {
        height: 0.6,
        backgroundColor: Colors.border,
    },

    //edit style
    inputTitle: {
        width: wp('70%'),
        alignSelf: 'center',
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.textGray,
        textAlign: 'right',
        marginBottom: 10
    },
    inputContainer: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'flex-end',
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderRadius: 5,
        borderWidth: 1,
        marginBottom: 10,
        paddingRight: 10,
        paddingLeft: 10,
        overflow: 'hidden'
    },
    inputIcon: {
        height: wp('7%'),
        width: wp('7%'),
        marginLeft: 10
    },
    input: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'right',
        borderRadius:0,
        
    },
    inputSingle: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 5,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: 10
    },
    inputBirthDay: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        height: hp('6%'),
        width: wp('21%'),
        marginLeft: 5,
        marginRight: 5,
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 5,
        alignSelf: 'center',
        textAlign: 'center',
        marginBottom: 10
    },
    inputAddressContainer: {
        flexDirection: 'row',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'flex-end',
        height: hp('15%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderRadius: 5,
        borderWidth: 1,
        marginBottom: 10,
        paddingRight: 10,
        paddingLeft: 10,
        overflow: 'hidden'
    },
    inputAddress: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        alignSelf: 'center',
        textAlign: 'center',
        textAlignVertical: 'top'
    },
    button: {
        borderRadius:5,
        width: wp('70%')
    },
    error: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        color: Colors.error,
        textAlign: 'center'
    },

    sexActive: {
        backgroundColor: Colors.green,
        height: hp('6%'),
        width: wp('20%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 5,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5
    },
    sexDeactive: {
        height: hp('6%'),
        width: wp('20%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 5,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5
    },
    sexActiveText: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#ffffff',
    },
    sexDeactiveText: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
    },

    pickerContainer: {
        height: hp('6%'),
        width: wp('70%'),
        borderColor: Colors.border,
        borderWidth: 1,
        borderRadius: 5,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
    },
    pickerStyle: {
        // borderBottomWidth: 0.8,
        // borderColor: '#e2e2e2',
        width: wp('50%'),
        // justifyContent: 'center',
    },
    pickerHeaderStyle: {
        backgroundColor: Colors.purple,
    },
    pickerHeaderTextStyle: {
        width: wp('50%'),
        backgroundColor: 'transparent',
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: 'white',
        textAlign: 'right',
    },
    pickerBackHeaderTextStyle: {
        width: wp('50%'),
        backgroundColor: 'transparent',
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: 'white',
        textAlign: 'left',
    },
    pickerTextStyle: {
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: '#34495e',
        textAlign: 'center',
    },
    pickerItemStyle: {
        width: wp('90%'),
        backgroundColor: 'transparent'
    },
    pickerItemTextStyle: {
        fontSize: 16,
        width: wp('65%'),
        backgroundColor: 'transparent',
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: '#34495e',
        textAlign: 'right'
    },
    pickerSeparator: {
        '@media android': {
            height: 1,
            width: wp('90%'),
            backgroundColor: '#e2e2e2'
        }
    },

  
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {
    setUser
};

export default connect(mapStateToProps, mapActionToProps)(ProfileScreen);
