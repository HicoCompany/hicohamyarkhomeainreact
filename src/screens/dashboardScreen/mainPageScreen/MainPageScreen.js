import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StatusBar,Alert, BackHandler, DeviceEventEmitter } from 'react-native';
import { Text, Card, CardItem, Container, Content, } from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../../utility/Colors';
import { hp, normalize, wp } from '../../../responsive';
import TabItem from '../../../components/TabItem';
import HeaderMain2 from '../../../components/HeaderMain2';
import { AppTour, AppTourSequence, AppTourView } from 'react-native-app-tour';
import TouchOpacityAppTour from '../../../components/TouchOpacityAppTour';
import TouchOpacityAppTour1 from '../../../components/TouchOpacityAppTour1';
import { exitApp } from '../../../redux/actions';
import { setIsSeenTour } from '../../../redux/actions';

// import TouchOpacityAppTour2 from '../../../components/TouchOpacityAppTour2';

class MainPageScreen extends Component {
  constructor(props) {
    super(props);

    this.appTourTargets = [];
  }
    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'صفحه اصلي '}
                    source={require('./../../../assets/image/ic_home.png')}
                    color={tintColor}
                />,
        };
    };
    onLogOutClick() {
      this.props.exitApp();
      this.props.navigation.replace('splash');
  }
    componentWillMount() {
     // this.registerSequenceStepEvent();
     // this.registerFinishSequenceEvent();
    }
    componentDidMount = () => {
      const {
        isSeenTour
    } = this.props.userData;
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      if(isSeenTour){
       
        
    }
    else{
      setTimeout(() => {
       
      }, 500);
      this.props.setIsSeenTour();
    }
  }
 
  registerSequenceStepEvent = () => {
    if (this.sequenceStepListener) {
      this.sequenceStepListener.remove();
    }
    this.sequenceStepListener = DeviceEventEmitter.addListener(
      'onShowSequenceStepEvent',
      (e) => {
        console.log(e);
      }
     
    );
  }

  registerFinishSequenceEvent = () => {
    if (this.finishSequenceListener) {
      this.finishSequenceListener.remove();
    }
    this.finishSequenceListener = DeviceEventEmitter.addListener(
      'onFinishSequenceEvent',
      (e) => {
        console.log(e);
      }
    );
  }
    handleBackButton = () => {
     Alert.alert(
         'خارج شدن از برنامه',
         'آیا با خروج از برنامه موافق هستید؟', [{
             text: 'نه',
             onPress: () => console.log('کنسل'),
             style: 'cancel'
         }, {
             text: 'بله',
             onPress: () => BackHandler.exitApp()
         },], {
             cancelable: false
         }
      );
      return true;
    } 
   
    render() {
        return (
        <Container>
          <Content>
            <View style={styles.container}>
              <StatusBar
                  backgroundColor={Colors.statusBar}
                  barStyle="light-content"
              />
              <HeaderMain2
                onMenuPress={() => BackHandler.exitApp()}
                onPinPress={() => this.props.navigation.navigate('boothDynamic')}
                headerTitle={'صفحه اصلی  '}
              />
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
                                        
                </View>
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
                
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
              <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
                  <TouchableOpacity                    
                    onPress={() => this.props.navigation.navigate('RequestListScreen')}
                  >
                    <View>
                      <Card style={styles.BoxProduct}>
                        <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                          <Image source={require('./../../../assets/image/Request.png')} style={styles.ImgProduct} />
                        </CardItem>
                        <Text style={styles.textProduct}>{'درخواست ها '}</Text>
                      </Card>
                    </View>
                  </TouchableOpacity>
                </View>
               
              <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
                <TouchableOpacity                    
                    onPress={() => this.props.navigation.navigate('DeliveredwasteNewScreen')}
                  >
                    <View>
                      <Card style={styles.BoxProduct}>
                        <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                          <Image source={require('./../../../assets/image/waste.png')} style={styles.ImgProduct} />
                        </CardItem>
                        <Text style={styles.textProduct}>{'تحویل پسماند '}</Text>
                      </Card>
                    </View>
                  </TouchableOpacity>
                </View>
                 
               
              </View>
             <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
             <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
                  <TouchableOpacity                    
                    onPress={() => this.props.navigation.navigate('report')}
                  >
                    <View>
                      <Card style={styles.BoxProduct}>
                        <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                          <Image source={require('./../../../assets/image/Report.png')} style={styles.ImgProduct} />
                        </CardItem>
                        <Text style={styles.textProduct}>{'گزارشات '}</Text>
                      </Card>
                    </View>
                  </TouchableOpacity>
                </View>
             <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }} >
                <TouchableOpacity                    
                    onPress={() => this.props.navigation.navigate('DeliveryTypeScreen')}
                  >
                    <View>
                      <Card style={styles.BoxProduct}>
                        <CardItem header style={{ borderBottomWidth: 1, borderBottomColor: 'gray' }}>
                          <Image source={require('./../../../assets/image/RemainCash.png')} style={styles.ImgProduct} />
                        </CardItem>
                        <Text style={styles.textProduct}>{'مدیریت اعتبار '}</Text>
                      </Card>
                    </View>
                  </TouchableOpacity>
                </View>
               
              </View>
             
          </View>
        </Content>
      </Container>
        );
    }
}

  export 
  const styles = EStyleSheet.create({
    
  top: {
    flex: 1
  },
  center: {
    flex: 1
  },
  bottom: {
    flex: 1
  },
    IRANSansNormal: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
     },
     IRANSansBold: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
     },
     header: {
      backgroundColor: '#1a237e',
     },
     badge: {
      position: 'absolute',
      top: 8, 
      right: 10, 
  },
      BtnImg: {
        width: '100%',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
      },
      textBtn: {
        color: 'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
        fontSize: 14,
      },
      bgProduct: {
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: '#fafafa',
        borderBottomWidth: 1,
        borderBottomColor: '#f0f0f0'
      },
      BoxProduct: {
        width: 160,
        height: 160,
        backgroundColor: 'white',
        alignItems: 'center',
      },
      ImgProduct: {
        width: '85%',
        height: 80,
        resizeMode: 'contain',
       color: Colors.black
      },
      textProduct: {
        color: 'black',
        fontSize: 14,
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
      },
      textGreen: {
        color: '#2e7d32',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
      },
      textRed: {
        color: '#e53935',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        textDecorationColor: '#000'
      },
      BtnProduct: {
          borderWidth: 1,
          borderColor: '#f0f0f0',
          height: 40,
          justifyContent: 'center',
         alignItems: 'center'
      },
      BtnProductOne: {
       flex: 2,
       justifyContent: 'center',
       alignItems: 'center',
       
       
       backgroundColor: 'green'
      },
      BtnProductTwo: {
        flex: 1, 
backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomRightRadius: 10,
        borderTopRightRadius: 10,
      },
      textBtnProduct: {
       color: 'white',
       '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
       fontSize: 11,
      },
      BoxBtnSort: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.5, 
        borderColor: '#bdbbbb',
        width: 330,
      },
      BtnSort: {
          width: 160,
          height: 40,
        paddingHorizontal: 4,
        backgroundColor: '#fafafa',
        alignItems: 'center',
      }
   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {
  exitApp,
  setIsSeenTour
};
export default connect(mapStateToProps, mapActionToProps)(MainPageScreen);
