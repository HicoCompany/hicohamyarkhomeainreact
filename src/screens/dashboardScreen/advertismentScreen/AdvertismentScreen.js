import React, { Component } from 'react';
import { ActivityIndicator, FlatList, Image, StatusBar, Text, 
    TouchableOpacity, View ,Alert,BackHandler} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Dialog, Portal } from 'react-native-paper';
import { connect } from 'react-redux';
import HeaderMain2 from '../../../components/HeaderMain2';
import TabItem from '../../../components/TabItem';
import Repository from '../../../repository/Repository';
import { hp, normalize, wp } from '../../../responsive';
import Colors from '../../../utility/Colors';
import Utils from '../../../utility/Utils';
import { Button, Empty, Loading } from './../../../components';
import { Separator , Icon} from 'native-base';


class AdvertismentScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'تبلیغات'}
                    source={require('./../../../assets/image/TabAdv.png')}
                    color={tintColor}
                />,
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoadingPage: true,
            list: [],
            refreshing: false,
            loadingData: false,

            apiError: false,
            apiErrorDesc: ''
        };
    }

    componentDidMount = () => {
        this.getApi();
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
      handleBackButton = () => {
       Alert.alert(
           'خارج شدن از برنامه',
           'آیا با خروج از برنامه موافق هستید؟', [{
               text: 'نه',
               onPress: () => console.log('کنسل'),
               style: 'cancel'
           }, {
               text: 'بله',
               onPress: () => BackHandler.exitApp()
           }, ], {
               cancelable: false
           }
        )
        return true;
      } 
    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    getApi = async () => {
        await this.PhoneGetAdvertisementForHelperApi();
    }

    async PhoneGetAdvertisementForHelperApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber
        };
        try {
            const response = await Repository.PhoneGetAdvertisementForHelperApi(params, headers);
            if (response.ResultID === 100) {
                const list = response.Result;
                this.setState({
                    list
                });
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
            });
        } catch (error) {
            await this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }

    handleLoadMore = () => {
        // if (this.state.countList > 0) {
        //     this.setState({
        //         offset: this.state.offset + this.state.pageSize, loadingData: true
        //     }, async () => {
        //         await this.PhoneGetAdvertisementForHelperApi();
        //     });
        // }
    }

    handleRefresh = () => {
        this.setState({ 
            refreshing: true, 
            isLoadingPage: true,
            list: [],
        }, () => {
            this.PhoneGetAdvertisementForHelperApi();
        });
    }

    renderEmpty = () => {
        return this.state.isLoadingPage ? (
            <View style={{ flex: 1 }}>
                <Loading
                    message={'در حال دریافت اطلاعات'}
                    messageColor={Colors.green}
                />
            </View>
        ) : (
                <View style={{ flex: 1 }}>
                    <Empty />
                </View>
            );
    }

    renderFooter = () => {
        if (!this.state.loadingData) {
            return null;
        }
        return (<ActivityIndicator size={'small'} color={'black'} />);
    }

    renderItem(item) {
        const {
            CreateDateStr,
            ImagePath,
            Like,
            AdvertisementID,
            Title
            //View
        } = item;
        return (
            <TouchableOpacity
                style={styles.itemContainer}
                activeOpacity={0.7}
                onPress={() => {
                    this.props.navigation.navigate('advertismentDetail', {
                        item
                    });
                }}
            >
                <View style={styles.itemLeftContainer}>
                    <Text style={styles.itemTextTitle}>
                        {Title}
                    </Text>
                    <Text style={styles.itemTextDate}>
                        {`تاریخ : ${CreateDateStr}`}
                    </Text>
                   {/* <View style={styles.itemSocial}>
                    <Icon type="Entypo" name="eye" style={[styles.IconDisabled]}/>
                    <Text style={styles.itemTextTitle}>
                    {item.View}
                    </Text>
                    <Icon type="Entypo" name="heart" style={[styles.IconRed]}/>
                    <Text style={styles.itemTextTitle}>
                    {Like}
                    </Text>
                    </View> */}
                </View>
                <View style={styles.itemRightContainer}>
                    <Image
                        source={{ uri: ImagePath }}
                        resizeMode={'contain'}
                        style={styles.itemImage}
                    />
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
               <HeaderMain2
                    onMenuPress={() => BackHandler.exitApp()}
                    onPinPress={() => this.props.navigation.navigate('boothDynamic')}
                    headerTitle={'تبلیغات'}
                />
                {/* <HeaderMain
                    onMenuPress={() => this.props.navigation.toggleDrawer()}
                /> */}
                {/* <View style={styles.screenHeader}>
                    <Text style={styles.screenHeaderTitle}>
                        {'لیست پیام ها ، اطلاعیه ها ، طرح های تشویقی و ...'}
                    </Text>
                </View> */}
                <FlatList
                    data={this.state.list}
                    renderItem={({ item }) => this.renderItem(item)}
                    ListEmptyComponent={() => this.renderEmpty()}
                    ListFooterComponent={() => this.renderFooter()}
                    numColumns={1}
                    keyExtractor={(item, index) => item.AdvertisementID.toString()}
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.handleRefresh()}
                    onEndReached={() => this.handleLoadMore()}
                    onEndReachedThreshold={0.08}
                    contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
                />
                <Portal>
                    <Dialog
                        visible={this.state.apiError}
                        style={styles.dialogContainer}
                        dismissable
                        onDismiss={() => {
                            this.setState({
                                apiError: false,
                            });
                        }}
                    >
                        <Dialog.Content>
                            <Text style={styles.dialogText}>
                                {this.state.apiErrorDesc}
                            </Text>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button 
                                buttonText={'تلاش مجدد'}
                                onPress={() => {
                                    this.setState({
                                        apiError: false,
                                        isLoadingPage: true,
                                        list: []
                                    }, () => {
                                        this.getApi();
                                    });
                                }}
                                style={styles.dialogButton}
                            />
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </View>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.disabled
    },
    screenHeader: {
        height: hp('7%'),
        backgroundColor: '#424242',
        justifyContent: 'center',
        alignItems: 'center',
    },
    screenHeaderTitle: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#fff',
    },
    title: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
        marginBottom: 10
    },

    itemContainer: {
        flexDirection: 'row',
        height: hp('20%'),
        margin: 5,
        borderRadius: 10,
        borderColor: Colors.buttonlightdark,
        backgroundColor:Colors.backgroundColor,
        borderWidth: 1
    },
    itemSocial:{
        marginTop:hp('5%'),
        flexDirection:'row',
        justifyContent:'space-around'
    },
    itemRightContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginLeft: 10
    },
    itemImage: {
        height: wp('30%'),
        width: wp('35%'),
        borderRadius: 10,
        marginRight: 10
    },
    itemLeftContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    itemTextTitle: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
       
        color: Colors.buttonlightdark,
    },
    itemTextDate: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.buttonlightdark,
    },
    IconDisabled:{
        color:Colors.disabled,
        
       paddingHorizontal:5
    },
    IconRed:{
        color:Colors.red,
       paddingHorizontal:5
    },
   
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    },
    separate: {
        marginBottom: 20,
        marginTop: 20,
        height: 0.8,
        width: wp('70%'),
        backgroundColor: Colors.buttonlightdark,
        alignSelf: 'center'
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(AdvertismentScreen);
