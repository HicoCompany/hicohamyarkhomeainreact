import React, { Component } from 'react';
import { FlatList, Image, StatusBar, Text, View,BackHandler } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Dialog, Portal } from 'react-native-paper';
import { connect } from 'react-redux';
import { Button, Empty, Loading } from '../../../components';
import HeaderBack from '../../../components/HeaderBack';
import Repository from '../../../repository/Repository';
import { hp, normalize, wp } from '../../../responsive';
import Colors from '../../../utility/Colors';
import Utils from '../../../utility/Utils';

let detailList = -1;

class WasteDetailScreen extends Component {

    constructor(props) {
        super(props);
        detailList = this.props.navigation.getParam('Result');
    }

   
    componentDidMount = () => {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      }
      
      handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
      } 
    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount = () => {

    }

    renderEmpty = () => {
        return (
            <View style={{ flex: 1 }}>
                <Empty />
            </View>
        );
    }

    renderItem(item) {
        const {
            WasteTypeCount,
            WasteTypeStr,
            HelperPrice,
            Points,
        } = item;
        return (
            <View style={styles.itemContainer}>
               
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {WasteTypeStr}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'نوع پسماند : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {WasteTypeCount}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'وزن : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {Points}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'امتیاز : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {HelperPrice}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'مبلغ : '}
                        </Text>
                    </View>
              
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                    onBackPress={() => this.props.navigation.goBack()}
                    headerTitle={'جزئیات'}
                />
                <FlatList
                    data={detailList}
                    renderItem={({ item }) => this.renderItem(item)}
                    ListEmptyComponent={() => this.renderEmpty()}
                    numColumns={1}
                    keyExtractor={(item, index) => index.toString()}
                    contentContainerStyle={detailList.length > 0 ? {} : { flex: 1 }}
                />
                <Button 
                    buttonText={'ثبت نظر سنجی'}
                />
            </View>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },

    itemContainer: {
        margin: 10,
        borderRadius: 15,
        borderColor: Colors.gray,
        borderWidth: 1,
        padding: 5
    },
    itemRightContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginLeft: 10
    },
    itemImage: {
        height: wp('12%'),
        width: wp('12%'),
        marginRight: 5
    },
    itemLeftContainer: {
        flex: 1,
    },
    itemTextContainer: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'center'
    },
    itemTextTitle: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
    },
    itemTextDesc: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textGray,
    },

    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    dialogText: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(WasteDetailScreen);
