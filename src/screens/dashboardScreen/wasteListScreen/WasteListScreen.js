import React, { Component } from 'react';
import { ActivityIndicator, FlatList, Image, StatusBar, Text, TouchableOpacity, View,BackHandler } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Dialog, Portal } from 'react-native-paper';
import { connect } from 'react-redux';
import HeaderMain from '../../../components/HeaderMain';
import TabItem from '../../../components/TabItem';
import Repository from '../../../repository/Repository';
import { hp, normalize, wp } from '../../../responsive';
import Colors from '../../../utility/Colors';
import Utils from '../../../utility/Utils';
import { Button, Empty, Loading } from './../../../components';

class WasteListScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'لیست همیاری'}
                    source={require('./../../../assets/image/ic_list.png')}
                    color={tintColor}
                />,
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoadingPage: true,
            list: [],
            refreshing: false,
            loadingData: false,

            apiError: false,
            apiErrorDesc: ''
        };
    }

 
    componentDidMount = () => {
        this.getApi();

        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      }
      
      handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
      } 
    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    componentWillUnmount = () => {

    }

    getApi = async () => {
        await this.phoneGetDeliverWasteHelperApi();
    }

    async phoneGetDeliverWasteHelperApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber
        };
        try {
            const response = await Repository.PhoneGetDeliverWasteHelperApi(params, headers);
            if (response.ResultID === 100) {
                const list = response.Result;
                if (list !== null) {
                    this.setState({
                        list
                    });
                }
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
            });
        } catch (error) {
            await this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }

    handleRefresh = () => {
        this.setState({ 
            refreshing: true, 
            isLoadingPage: true,
            list: [],
        }, () => {
            this.phoneGetDeliverWasteHelperApi();
        });
    }

    renderEmpty = () => {
        return this.state.isLoadingPage ? (
            <View style={{ flex: 1 }}>
                <Loading
                    message={'در حال دریافت اطلاعات'}
                    messageColor={Colors.green}
                />
            </View>
        ) : (
                <View style={{ flex: 1 }}>
                    <Empty />
                </View>
            );
    }

    renderItem(item) {
        const {
            CreateDateStr,
            CreateTimeStr,
            DeliveryDateStr,
            DeliveryTimeStr,
            DeliveryWasteID,
            Result,
            StatusStr
        } = item;
        return (
            <TouchableOpacity
                style={styles.itemContainer}
                activeOpacity={0.6}
                onPress={() => {
                    this.props.navigation.navigate('wasteListDetail', {
                        Result
                    });
                }}
            >
                <View style={styles.itemLeftContainer}>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {DeliveryWasteID}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'کد درخواست : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {`${CreateDateStr} ${CreateTimeStr}`}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'تاریخ درخواست : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {StatusStr}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'وضعیت : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {''}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'وزن کل : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {''}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'مبلغ کل : '}
                        </Text>
                    </View>
                    <View style={styles.itemTextContainer}>
                        <Text style={styles.itemTextDesc}>
                            {''}
                        </Text>
                        <Text style={styles.itemTextTitle}>
                            {'امتیاز کل : '}
                        </Text>
                    </View>
                </View>
                <View style={styles.itemRightContainer}>
                    <Image
                        source={require('./../../../assets/image/ic_pasmand.png')}
                        resizeMode={'contain'}
                        style={styles.itemImage}
                    />
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderMain
                    onMenuPress={() => this.props.navigation.toggleDrawer()}
                    onPinPress={() => this.props.navigation.navigate('boothDynamic')}
                />
                <View style={styles.screenHeader}>
                    <Text style={styles.screenHeaderTitle}>
                        {'لیست همیاری ها'}
                    </Text>
                </View>
                <FlatList
                    data={this.state.list}
                    renderItem={({ item }) => this.renderItem(item)}
                    ListEmptyComponent={() => this.renderEmpty()}
                    numColumns={1}
                    keyExtractor={(item, index) => item.DeliveryWasteID.toString()}
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.handleRefresh()}
                    contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
                />
                <Portal>
                    <Dialog
                        visible={this.state.apiError}
                        style={styles.dialogContainer}
                        dismissable
                        onDismiss={() => {
                            this.setState({
                                apiError: false,
                            });
                        }}
                    >
                        <Dialog.Content>
                            <Text style={styles.dialogText}>
                                {this.state.apiErrorDesc}
                            </Text>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button 
                                buttonText={'تلاش مجدد'}
                                onPress={() => {
                                    this.setState({
                                        apiError: false,
                                        isLoadingPage: true,
                                        list: []
                                    }, () => {
                                        this.getApi();
                                    });
                                }}
                                style={styles.dialogButton}
                            />
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </View>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    screenHeader: {
        height: hp('7%'),
        backgroundColor: '#424242',
        justifyContent: 'center',
        alignItems: 'center',
    },
    screenHeaderTitle: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#fff',
    },
    title: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
        marginBottom: 10
    },

    itemContainer: {
        flexDirection: 'row',
        margin: 10,
        borderRadius: 15,
        borderColor: Colors.gray,
        borderWidth: 1,
        paddingTop: 5,
        paddingBottom: 5
    },
    itemRightContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginLeft: 10
    },
    itemImage: {
        height: wp('12%'),
        width: wp('12%'),
        marginRight: 5
    },
    itemLeftContainer: {
        flex: 1,
    },
    itemTextContainer: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        alignItems: 'center'
    },
    itemTextTitle: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
    },
    itemTextDesc: {
        fontSize: normalize(11),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textGray,
    },

   
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(WasteListScreen);
