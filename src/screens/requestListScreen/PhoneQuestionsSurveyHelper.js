import React, { Component } from 'react';
import { ActivityIndicator, FlatList, Image, StatusBar, Text, 
    TouchableOpacity, View ,Alert,BackHandler} from 'react-native';
import { ListItem,Left, Right,Radio } from 'native-base';

import EStyleSheet from 'react-native-extended-stylesheet';
import { Dialog, Portal } from 'react-native-paper';
import { connect } from 'react-redux';
import HeaderBack from '../../components/HeaderBack';
import TabItem from '../../components/TabItem';
import Repository from '../../repository/Repository';
import { hp, normalize, wp } from '../../responsive';
import Colors from '../../utility/Colors';
import Utils from '../../utility/Utils';
import { Button, Empty, Loading } from './../../components';``
import { Separator , Icon} from 'native-base';


class PhoneQuestionsSurveyHelper extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'نظرسنجی'}
                    source={require('./../../assets/image/ic_notice.png')}
                    color={tintColor}
                />,
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoadingPage: true,
            list: [],
            listSelected: [],
            refreshing: false,
            loadingData: false,
            isLoadingButton: false,
            apiError: false,
            apiErrorDesc: '',
            Active:true
        };
        this.PhoneAddReqSurveyHelper = this.PhoneAddReqSurveyHelper.bind(this);
    }

    componentDidMount = () => {
        var DeliveryWasteID = this.props.navigation.getParam('DeliveryWasteID',0);
        this.getApi();
    this.setState({DeliveryWasteID:DeliveryWasteID});
      
    }
    PhoneAddReqSurveyHelper = async () => {
        this.setState({
            ApiSuccsess: false,
            ApiSuccessDesc: ''
        });
        const headers = null;
        var list = this.state.list.map((item) => {
              return {
                QuestionsSurveyHelperId:item.QuestionsSurveyHelperId,
                ExcellentPoints:(item.ExcellentPointsSelected=='true'?item.ExcellentPoints:0),
                GoodPoints:(item.GoodPointsSelected=='true'?item.GoodPoints:0),
                AveragePoints:(item.AveragePointsSelected=='true'?item.AveragePoints:0),
                WeakPoints:(item.WeakPointsSelected=='true'?item.WeakPoints:0)
            };
            
          });
          
        //   this.props.navigation.navigate("DeliveryTimeScreen",{laps:this.state.laps})
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber,
            DeliveryWasteID:this.state.DeliveryWasteID,
            QuestionHelperList:list
        };
        try {
            const response = await Repository.PhoneAddReqSurveyHelperApi(params, headers);
            if (response.Success === 1) {
                this.setState({
                    ApiSuccsess: true,
                    ApiSuccessDesc: response.Text
                });
               this.props.navigation.navigate("RequestListScreen");
                
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                isLoadingPage: false,
            });
        } catch (error) {
            this.setState({
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }
    shouldComponentUpdate = (nextProps, nextState) => {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    
    getApi = async () => {
        await this.PhoneQuestionsSurveyHelperApi();
    }
    MapListRadio= (list)=>{
        var listNew = list.map((item) => {
              return ({
                  ...item,
                  AveragePointsSelected : 'false',
                  ExcellentPointsSelected : 'false',
                  GoodPointsSelected : 'false',
                  WeakPointsSelected : 'false',
              });
          });
          this.setState({
            list: listNew
        });
    }
    async PhoneQuestionsSurveyHelperApi() {
        const headers = null;
        const params = {
            Mobile: this.props.userData.mobileNumber,
            Code: this.props.userData.codeNumber
        };
        try {
            const response = await Repository.PhoneQuestionsSurveyHelperApi(params, headers);
            if (response.Success === 1) {
                this.MapListRadio(response.Result);
            }
            else {
                this.setState({
                    apiError: true,
                    apiErrorDesc: response.Text
                });
            }
            this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
            });
        } catch (error) {
            await this.setState({
                loadingData: false,
                refreshing: false,
                isLoadingPage: false,
                apiError: true,
                apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
            });
        }
    }

    handleLoadMore = () => {
        // if (this.state.countList > 0) {
        //     this.setState({
        //         offset: this.state.offset + this.state.pageSize, loadingData: true
        //     }, async () => {
        //         await this.phoneGetNotificationForHelperApi();
        //     });
        // }
    }

    handleRefresh = () => {
        this.setState({ 
            refreshing: true, 
            isLoadingPage: true,
            list: [],
        }, () => {
            this.PhoneQuestionsSurveyHelperApi();
        });
    }

    renderEmpty = () => {
        return this.state.isLoadingPage ? (
            <View style={{ flex: 1 }}>
                <Loading
                    message={'در حال دریافت اطلاعات'}
                    messageColor={Colors.green}
                />
            </View>
        ) : (
                <View style={{ flex: 1 }}>
                    <Empty />
                </View>
            );
    }

    renderFooter = () => {
        if (!this.state.loadingData) {
            return null;
        }
        return (<ActivityIndicator size={'small'} color={'black'} />);
    }
    SelectedRadioButton=(QuestionNo,Status)=>{
            
        var listNewNew = this.state.list;
        const ItemSelectedIndex = listNewNew.findIndex(el=>el.QuestionNo==QuestionNo);
        if(Status==1){
            listNewNew[ItemSelectedIndex].ExcellentPointsSelected='true';
            listNewNew[ItemSelectedIndex].GoodPointsSelected='false';
            listNewNew[ItemSelectedIndex].AveragePointsSelected ='false';
            listNewNew[ItemSelectedIndex].WeakPointsSelected='false' 
          }
          else if(Status==2){
            listNewNew[ItemSelectedIndex].ExcellentPointsSelected='false';
            listNewNew[ItemSelectedIndex].GoodPointsSelected='true';
            listNewNew[ItemSelectedIndex].AveragePointsSelected ='false';
            listNewNew[ItemSelectedIndex].WeakPointsSelected='false' 
          }
             else if(Status==3){
                listNewNew[ItemSelectedIndex].ExcellentPointsSelected='false';
                listNewNew[ItemSelectedIndex].GoodPointsSelected='false';
                listNewNew[ItemSelectedIndex].AveragePointsSelected ='true';
                listNewNew[ItemSelectedIndex].WeakPointsSelected='false'
          } 
          else if(Status==4){
            listNewNew[ItemSelectedIndex].ExcellentPointsSelected='false';
            listNewNew[ItemSelectedIndex].GoodPointsSelected='false';
            listNewNew[ItemSelectedIndex].AveragePointsSelected ='false';
            listNewNew[ItemSelectedIndex].WeakPointsSelected='true'
        }
          this.setState({
              list:listNewNew,
            Active:!this.state.Active
        });
         

    }
    renderItem(item) {
        const {
            AveragePoints,
            ExcellentPoints,
            GoodPoints,
            QuestionNo,
            QuestionText,
            QuestionsSurveyHelperId,
            RemainPoints,
            SurveyHelperID,
            WeakPoints,
            AveragePointsSelected ,
            ExcellentPointsSelected,
            GoodPointsSelected,
            WeakPointsSelected ,
            //View
        } = item;
        return (
            <ListItem   style={styles.itemContainer}>
                <View style={{flexDirection:'column',flex:1,paddingRight:10}}>

                <View style={{flexDirection:'row-reverse',flex:1}}>
                 <Text style={styles.itemTextTitle} >{QuestionText}</Text>
                 </View>
                  <View style={{flexDirection:'row-reverse',justifyContent:'space-between',flex:1,paddingRight:10}}>
                      <View style={{flexDirection:'row-reverse'}}>
                <Text  style={styles.itemTextTitle}>{'عالی'} </Text>
                    <Radio selected={String(ExcellentPointsSelected)=='true'?true:false} onPress={()=>this.SelectedRadioButton(QuestionNo,1)} />
                    </View>
                    <View style={{flexDirection:'row-reverse'}}>
                    <Text  style={styles.itemTextTitle}>{'خوب'} </Text>
                    <Radio  selected={String(GoodPointsSelected)=='true'?true:false} onPress={()=>this.SelectedRadioButton(QuestionNo,2)}/>
                    </View>
                    <View style={{flexDirection:'row-reverse'}}>
                    <Text  style={styles.itemTextTitle}>{'متوسط'} </Text>
                    <Radio selected={String(AveragePointsSelected)=='true'?true:false} onPress={()=>this.SelectedRadioButton(QuestionNo,3)}/>
                    </View>
                    <View style={{flexDirection:'row-reverse'}}>
                    <Text  style={styles.itemTextTitle}>{'ضعیف'} </Text>
                    <Radio   selected={String(WeakPointsSelected)=='true'?true:false} onPress={()=>this.SelectedRadioButton(QuestionNo,4)}/>
                    </View>
                </View>
                </View> 
            </ListItem>
          
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                    onBackPress={() => this.props.navigation.goBack()}
                    headerTitle={'نظرسنجی'}
                />
                
                <FlatList
                    data={this.state.list}
                    renderItem={({ item }) => this.renderItem(item)}
                    ListEmptyComponent={() => this.renderEmpty()}
                    ListFooterComponent={() => this.renderFooter()}
                    numColumns={1}
                    keyExtractor={(item, index) => item.QuestionNo.toString()}
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.handleRefresh()}
                    onEndReached={() => this.handleLoadMore()}
                    onEndReachedThreshold={0.08}
                    contentContainerStyle={this.state.list.length > 0 ? {} : { flex: 1 }}
                />
                 <Button
          buttonText={this.state.isLoadingButton ? 'لطفا منتظر بمانید' : 'ثبت نظرسنجی'}
          isLoading={this.state.isLoadingButton}
          style={styles.button}
          onPress={this.PhoneAddReqSurveyHelper}
        />
                <Portal>
                    <Dialog
                        visible={this.state.apiError}
                        style={styles.dialogContainer}
                        dismissable
                        onDismiss={() => {
                            this.setState({
                                apiError: false,
                            });
                        }}
                    >
                        <Dialog.Content>
                            <Text style={styles.dialogText}>
                                {this.state.apiErrorDesc}
                            </Text>
                        </Dialog.Content>
                        <Dialog.Actions>
                            <Button 
                                buttonText={'تلاش مجدد'}
                                onPress={() => {
                                    this.setState({
                                        apiError: false,
                                        isLoadingPage: true,
                                        list: []
                                    }, () => {
                                        this.getApi();
                                    });
                                }}
                                style={styles.dialogButton}
                            />
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </View>
        );
    }

}

const styles = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.disabled
    },
    
  button: {
    marginTop:0,
    width: wp('70%'),
    backgroundColor: Colors.buttondark,
    
    borderColor: '#035e6f',
     borderWidth: 1,
     borderRadius: 5,
    //borderRaduisWidth:0,
  },
    screenHeader: {
        height: hp('7%'),
        backgroundColor: '#424242',
        justifyContent: 'center',
        alignItems: 'center',
    },
    screenHeaderTitle: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#fff',
    },
    title: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: '#035e6f',
        marginBottom: 10
    },

    itemContainer: {
        flexDirection: 'row',
        height: hp('20%'),
        margin: 5,
        borderRadius: 10,
        borderColor: Colors.buttonlightdark,
        backgroundColor:Colors.backgroundColor,
        borderWidth: 1
    },
    itemSocial:{
        marginTop:hp('5%'),
        flexDirection:'row',
        justifyContent:'space-around'
    },
    itemRightContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginLeft: 10
    },
    itemImage: {
        height: wp('30%'),
        width: wp('35%'),
        borderRadius: 10,
        marginRight: 10
    },
    itemLeftContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    itemTextTitle: {
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        }, 
       paddingLeft:5,
        color: Colors.buttonlightdark,
    },
    itemTextDate: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.buttonlightdark,
    },
    IconDisabled:{
        color:Colors.disabled,
        
       paddingHorizontal:5
    },
    IconRed:{
        color:Colors.red,
       paddingHorizontal:5
    },
   
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    },
    separate: {
        marginBottom: 20,
        marginTop: 20,
        height: 0.8,
        width: wp('70%'),
        backgroundColor: Colors.buttonlightdark,
        alignSelf: 'center'
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(PhoneQuestionsSurveyHelper);
