import React, {Component} from 'react';
import { connect } from 'react-redux';
import { StatusBar,Share,BackHandler } from 'react-native';
import Colors from '../../utility/Colors';
import HeaderBack from '../../components/HeaderBack';
import Repository from '../../repository/Repository';

import { View,Image,TouchableOpacity,ScrollView,} from 'react-native';
import { Container, Header,Content, Footer, FooterTab, Button, 
   Icon, Text,Card, CardItem ,Badge} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';

class IntroductionScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
        tabBarIcon: ({ tintColor }) =>
            <TabItem
                name={' دعوت از دوستان'}
                source={require('./../../assets/image/ic_notice.png')}
                color={tintColor}
            />,
    };
};
constructor(props) {
  super(props);

  this.state = {
    horizontal: false,
    list: [],
  };
}
onShare = async () => {
  try {
    const result = await Share.share({
      message:
        'با دعوت از دوستان خود از امتیاز های ویژه ما برخوردار شوید کد دعوت : ' + this.state.list.ReagentCode,
    });

    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        // shared with activity type of result.activityType
      } else {
        // shared
      }
    } else if (result.action === Share.dismissedAction) {
      // dismissed
    }
  } catch (error) {
    alert(error.message);
  }
};

componentDidMount = () => {
  this.getApi();

  BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

handleBackButton = () => {
  this.props.navigation.goBack();
  return true;
} 
getApi = async () => {
  await this.PhoneSettingsForHelperApi();
  
}
async PhoneSettingsForHelperApi() {
  const headers = null;
  const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber
  };
  try {
      const response = await Repository.PhoneSettingsForHelperApi(params, headers);

      if (response.Success === 1) {

          const list = response.Result;
          this.setState({
            list: list,
           
          });
      }
      else {
          this.setState({
              apiError: true,
              apiErrorDesc: response.Text
          });
      }
      this.setState({
          isLoadingPage: false,
      });
  } catch (error) {
      this.setState({
          isLoadingPage: false,
          apiError: true,
          apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
  }
}
  render() {
    return (
        <Container>
           <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                    onBackPress={() => this.props.navigation.goBack()}
                    headerTitle={'دعوت از دوستان '}
                />
         <Content>
              <View style={[styles.Center,{paddingTop:10}]}>
                <Card style={styles.Box}>
                 <CardItem>
                  <Text style={styles.text}> با معرفی به دوستان و آشنایان خود , به حفظ محیط زیست کمک کنید</Text>
                 </CardItem>
              </Card>
              </View>
              <View style={[styles.Center,{paddingTop:10}]}>
                <Card style={[styles.Box,{width:'80%'}]}>
                 <CardItem>
                  <Text style={[styles.text,{paddingRight:10}]}>{this.state.list.ReagentCode}</Text>
                  <Text style={styles.textGreen}>کد معرفی :</Text>
                 </CardItem>
              </Card>
              </View>
              <View style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'space-around',paddingTop:30,paddingHorizontal:30}}>
              <TouchableOpacity
                style={[styles.BtnSocial,{backgroundColor:'#1e98cb'} ]}
                onPress={this.onShare}
                    >
                  
                  <Text style={styles.textWhite}>ارسال کد</Text>
                   
                  
                </TouchableOpacity>
                {/* <TouchableOpacity
                style={[styles.BtnSocial,{backgroundColor:'#f79b0b'} ]}
                onPress={this.onShare}
                    >
                  <View style={[styles.IconTotal]}>
                   <Icon type="MaterialIcons" name="message" style={[styles.Icon]}/>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                style={[styles.BtnSocial,{backgroundColor:'#e85556'} ]}
                onPress={this.onShare}
                    >
                 <View style={[styles.IconTotal]}>
                   <Icon type="Entypo" name="mail" style={[styles.Icon]}/>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                style={[styles.BtnSocial,{backgroundColor:'#43a59d'} ]}
                onPress={this.onShare}
                    >
                 <View style={[styles.IconTotal]}>
                   <Icon type="AntDesign" name="sharealt" style={[styles.Icon]}/>
                  </View>
                </TouchableOpacity> */}
              </View>
        </Content>

      </Container>
    );
  }

}


const styles = EStyleSheet.create({
  IRANSansNormal: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_M',
  },
   },
   IRANSansBold: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_B',
  },
   },
   header:{
    backgroundColor:'#50b3ae',
   },
    textHeader:{
      color:'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
      fontSize: 14,
     },
    text:{
      color:'black',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
      fontSize: 12,
     },
     textTitle:{
      color:'black',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
      fontSize: 14,
     },
     Title:{
      flex:1,
      flexDirection:'row',
      alignItems: 'center',
      justifyContent:'flex-end',
      paddingTop:15,
      paddingHorizontal:8
     },
     TotalCard:{
      flex:1,
      flexDirection:'column',
      alignItems: 'center',
      justifyContent:'center',
      paddingHorizontal:10,
      paddingTop:10
    },
    CardSelect:{
      flex:1,
      flexDirection:'row',
      justifyContent:'space-around',
      borderRadius:5,
      padding:8,
      paddingVertical:12
    },
    IconGreen:{
      color:'#50b3ae', 
      fontSize: 20
    },
    IconBag:{
      color:'#50b3ae',
      fontSize:25
    },
    textFooter:{
      color:'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
      fontSize: 14,
     },
     BtntTime:{
         justifyContent:'center',
         alignItems:'center',
         
     },
     CardTime:{
      flex:1,
      flexDirection:'row',
      justifyContent:'space-between',
      borderRadius:5,
      paddingHorizontal:12,
      paddingVertical:10
    },
    Box:{
      width:'90%',
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent:'center',
      borderRadius: 5,
    },
    Center:{
        justifyContent:'center',
        alignItems:'center'
    },
    textGreen:{
      color: '#50b3ae',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
      fontSize: 12,
    },
    
    textWhite:{
      color: '#FFFFFF',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
      fontSize: 12,
    },
    BtnSocial:{
        width:'22%',
        height:63,
        borderRadius:300,
        justifyContent:'center',
        alignItems:'center'
    },
    IconTotal:{
      flexDirection:'row',
      justifyContent:"center",
      alignItems:'center'
    },
    Icon:{
      color:'white',
      fontSize:22
    },
});
 
const mapStateToProps = (state) => {
  return {
      userData: state.userData
  };
};

const mapActionToProps = {

};
export default connect(mapStateToProps, mapActionToProps)(IntroductionScreen);
