import React, {Component} from 'react';
import { connect } from 'react-redux';
import { View,Image,TouchableOpacity,StatusBar,ScrollView,ActivityIndicator, FlatList,BackHandler} from 'react-native';
import Colors from '../../utility/Colors';
import HeaderBack from '../../components/HeaderBack';
import Repository from '../../repository/Repository';
import { Container, Header,Content, Footer, FooterTab, 
   Icon, Text,Card, CardItem ,Badge} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Dialog, Portal } from 'react-native-paper';
import TabItem from '../../components/TabItem';
import Utils from '../../utility/Utils';
import { hp, normalize, wp } from '../../responsive';

import { Button, Empty, Loading } from './../../components';

class RequestListScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
        tabBarIcon: ({ tintColor }) =>
            <TabItem
                name={'لیست انتقادات و پیشنهادات'}
                source={require('./../../assets/image/ic_notice.png')}
                color={tintColor}
            />,
    };
};
constructor(props) {
  super(props);
  this.state = {
      isLoadingPage: true,
      list: [],
      listRequest: [],
      refreshing: false,
      loadingData: false,
      apiError: false,
      apiErrorDesc: ''
  };
}

componentDidMount = () => {
  this.getApi();
 BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

handleBackButton = () => {
 this.props.navigation.goBack();
 return true;
} 

getApi = async () => {
await this.PhoneGetSuggestionForHelperApi();
}
async PhoneGetSuggestionForHelperApi() {
  const headers = null;
  const params = {
    Mobile: this.props.userData.mobileNumber,
    Code: this.props.userData.codeNumber
  };
  
  try {
  
    const response = await Repository.PhoneGetSuggestionForHelperApi(params, headers);
  
    if (response.ResultID === 100) {
        const listRequest = response.Result;
        this.setState({
          listRequest
        });
    }
    else {
      if(response.Result!=100){
        this.setState({
            apiError: true,
            apiErrorDesc: response.Text
        });
      }
    }
    this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
    });
  } catch (error) {
    await this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
    });
  }
  }
async PhoneGetInfoCooperationApi() {
const headers = null;
const params = {
  Mobile: this.props.userData.mobileNumber,
  Code: this.props.userData.codeNumber
};

try {

  const response = await Repository.PhoneGetInfoCooperationApi(params, headers);

  if (response.ResultID === 100) {
      const list = response.Result;
   
      this.setState({
          list
      });
      await this.PhoneGetDeliverWasteHelperApi();

  }
  else if(response.ResultID===-100){
    
    this.setState({
      apiError: true,
      apiErrorDesc: 'هیچ درخواستی ثبت نشده است'
  });
  }
  else {
      this.setState({
          apiError: true,
          apiErrorDesc: response.Text
      });
  }
  this.setState({
      loadingData: false,
      refreshing: false,
      isLoadingPage: false,
  });
} catch (error) {
  await this.setState({
      loadingData: false,
      refreshing: false,
      isLoadingPage: false,
      apiError: true,
      apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
  });
}
}
async PhoneDelRequestDeliverWasteApi(DeliveryWasteID) {
  const headers = null;
  const params = {
    Mobile: this.props.userData.mobileNumber,
    Code: this.props.userData.codeNumber,
    DeliveryWasteID:DeliveryWasteID
  };
  
  try {
  
    const response = await Repository.PhoneDelRequestDeliverWasteApi(params, headers);
  
    if (response.Success === 1) {
      this.setState({
        ApiSuccess: true,
        ApiTitleSuccess: response.Text
    });
    
        this.handleRefresh();
    }
    else {
        this.setState({
            apiError: true,
            apiErrorDesc: response.Text
        });
    }
    this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
    });
  } catch (error) {
    await this.setState({
        loadingData: false,
        refreshing: false,
        isLoadingPage: false,
        apiError: true,
        apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
    });
  }
  }
shouldComponentUpdate = (nextProps, nextState) => {
  return Utils.shallowCompare(this, nextProps, nextState);
}
handleLoadMore = () => {
  // if (this.state.countList > 0) {
  //     this.setState({
  //         offset: this.state.offset + this.state.pageSize, loadingData: true
  //     }, async () => {
  //         await this.phoneGetNotificationForHelperApi();
  //     });
  // }
}

handleRefresh = () => {
  this.setState({ 
      refreshing: true, 
      isLoadingPage: true,
      
  }, () => {
      this.PhoneGetDeliverWasteHelperApi();
  });
}

renderEmpty = () => {
  return this.state.isLoadingPage ? (
      <View style={{ flex: 1 }}>
          <Loading
              message={'در حال دریافت اطلاعات'}
              messageColor={Colors.green}
          />
      </View>
  ) : (
          <View style={{ flex: 1 }}>
              <Empty />
          </View>
      );
}

renderFooter = () => {
  if (!this.state.loadingData) {
      return null;
  }
  return (<ActivityIndicator size={'small'} color={'black'} />);
}


renderCancel(Status,DeliveryWasteID) {
  if (Status!=2) {
    return (
      <TouchableOpacity
      style={[styles.BtnRed]}
     
      onPress={() => {
        this.setState({
          ApiSuccess: false,
            isLoadingPage: true,
        }, 
         async () => {
          await   this.PhoneDelRequestDeliverWasteApi(DeliveryWasteID);
                });
    }}
   >
<Text  style={[styles.text,{color:'white'}]}> لغو</Text>
   </TouchableOpacity>
    );
} else {
  return (
    <TouchableOpacity
    style={[styles.BtnBlue]}
   
    onPress={async () => {
        await   this.props.navigation.navigate("PhoneQuestionsSurveyHelper",{DeliveryWasteID:DeliveryWasteID});
      }}
 >
<Text  style={[styles.text,{color:'white'}]}>نظرسنجی</Text>
 </TouchableOpacity>
  );
}
  
}

renderItem(item) {
  const {
    HelperId,
    IsReadText,
    MessageText,
    PMessageDate,
    PResponseDate,
    Responder,
    ResponseStatusText,
    ResponseText,
    Subject,
    SuggestionId
  } = item;
  return (
    <Card>
               <CardItem>
             <View style={[styles.TotalContent]}>
             <View style={styles.SubContent}>
  <Text style={[styles.text,{paddingRight:12}]}>{Subject}</Text>
            <Text style={[styles.text,{paddingRight:12}]}>{"عنوان: "}</Text>

                 <Icon type="AntDesign" name="calendar" style={{color:'#50b3ae', fontSize: 20}}/>
           </View>
            <View style={styles.SubContent}>
                <Text style={[styles.text,{paddingRight:12}]}>{MessageText}</Text>
            <Text style={[styles.text,{paddingRight:12}]}>{"متن: "}</Text>

                 <Icon type="AntDesign" name="calendar" style={{color:'#50b3ae', fontSize: 20}}/>
           </View>
           <View style={styles.SubContent}>
           <Text style={[styles.text,{paddingRight:12}]}>{IsReadText}</Text>
                 <Text style={[styles.text ,{paddingRight:12}]}> {"وضعیت:"} </Text>
                 <Icon type="Entypo" name="shopping-bag" style={{color:'#50b3ae',fontSize: 20}}/>
           </View>
           <View style={[styles.SubContent]}>
                        
                    
                <Text style={[styles.text,{paddingRight:12}]}>{ResponseText}</Text>
                   
                     <Text style={[styles.text,{color:'#50b3ae'}]}> پاسخ</Text>
                     </View>
                      
                     </View>
           </CardItem>
           </Card>
  );
}

  render() {
    return (
        <Container>
           <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                    onBackPress={() => this.props.navigation.goBack()}
                    headerTitle={'انتقادات و پیشنهادات'}
                />
          <Content>
              <View style={[styles.Center]}>
              <Card style={{width:'90%'}}>
               <CardItem>
             <View style={[styles.TotalContent]}>
             <TouchableOpacity
      style={[styles.BtnBlue]}
     
      onPress={() => {
       this.props.navigation.navigate("SuggestionAdd")
    }}
   >
<Text  style={[styles.text,{color:'white'}]}> انتقادات و پیشنهادات جدید</Text>
   </TouchableOpacity>
            {/* <View style={styles.SubContent}>
                 <Text style={[styles.text,{paddingRight:12}]}>{this.state.list.PackageCount}</Text>
                 <Text style={[styles.text ,{paddingRight:12}]}>تعداد پکیج درخواست شده :</Text>
                 <Icon type="FontAwesome5" name="file-signature" style={{color:'#50b3ae', fontSize: 20}}/>
           </View> */}
           {/* <View style={styles.SubContent}>
                 <Text style={[styles.text,{paddingRight:12}]}>{this.state.list.RequestDeliveryCount}</Text>
                 <Text style={[styles.text ,{paddingRight:12}]}>  تعداد درخواست های تحویلی :</Text>
                 <Icon type="Entypo" name="shopping-bag" style={{color:'#50b3ae',fontSize: 20}}/>
           </View>*/}
            {/*
           <View style={styles.SubContent}>
                 <Text style={[styles.text,{paddingRight:12}]}>{this.state.list.DeliveryCount}</Text>
                 <Text style={[styles.text ,{paddingRight:12}]}> تعداد پسماند های تحویلی: </Text>
                 <Icon type="Entypo" name="shopping-bag" style={{color:'#50b3ae',fontSize: 20}}/>
           </View>
           */}
            {/*
                 <View style={styles.SubContent}>
                 <Text style={[styles.text,{paddingRight:12}]}>{this.state.list.Points}</Text>
                 <Text style={[styles.text ,{paddingRight:12}]}> امتیاز : </Text>
                 <Icon type="Entypo" name="star-outlined" style={{color:'#50b3ae',fontSize: 20}}/>
                 </View>
                  */}
                 </View>
                  
           </CardItem>
           </Card>
              </View>
         
              <View style={[styles.Center]}>
            
              <FlatList
                  data={this.state.listRequest}
                  renderItem={({ item }) => this.renderItem(item)}
                  ListEmptyComponent={() => this.renderEmpty()}
                  ListFooterComponent={() => this.renderFooter()}
                  numColumns={1}
                  keyExtractor={(item, index) => item.SuggestionId.toString()}
                  refreshing={this.state.refreshing}
                  onRefresh={() => this.handleRefresh()}
                  onEndReached={() => this.handleLoadMore()}
                  onEndReachedThreshold={0.08}
                  contentContainerStyle={this.state.listRequest.length > 0 ? {} : { flex: 1 }}
              />
              
              </View>
              <Portal>
                  <Dialog
                      visible={this.state.apiError}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                              apiError: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.apiErrorDesc}
                          </Text>
                      </Dialog.Content>
                      <Dialog.Actions>
                          <Button 
                              buttonText={'تلاش مجدد'}
                              onPress={() => {
                                  this.setState({
                                      apiError: false,
                                      isLoadingPage: true,
                                      list: []
                                  }, () => {
                                      this.getApi();
                                  });
                              }}
                              style={styles.dialogButton}
                          />
                          <Button 
                              buttonText={'کنسل'}
                              onPress={ () => {
                                this.setState({
                                  apiError: false,
                                });
                            }}
                              style={styles.dialogButton}
                          />
                      </Dialog.Actions>
                  </Dialog>
                  <Dialog
                      visible={this.state.ApiSuccess}
                      style={styles.dialogContainer}
                      dismissable
                      onDismiss={() => {
                          this.setState({
                            ApiSuccess: false,
                          });
                      }}
                  >
                      <Dialog.Content>
                          <Text style={styles.dialogText}>
                              {this.state.ApiTitleSuccess}
                          </Text>
                      </Dialog.Content>
                      
                  </Dialog>
              </Portal>
        </Content>
      </Container>
    );
  }

}


const styles = EStyleSheet.create({
  IRANSansNormal: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_M',
  },
   },
   IRANSansBold: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_B',
  },
   },
   header:{
    backgroundColor:'#50b3ae',
   },
   textHeader:{
    color:'white',
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_B',
  },
   },
    text:{
      color:'black',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     TotalContent:{
      width:"100%",
      flexDirection:'column',
    },
     SubContent:{
      width:"100%",
      justifyContent:'flex-end',
      paddingVertical:10,
      alignItems: 'center',
      flexDirection:'row',
      flex:1,
      borderBottomWidth:1,
      borderBottomColor:"#f0f0f0",
    },
    Center:{
      alignItems: 'center',
      justifyContent:'center',
      paddingTop:10
     },
      BtnGreen:{
        borderWidth:1,
        borderColor:'#50b3ae',
        borderRadius:5,
        padding:4,
        justifyContent:'center',
        alignItems:'center',
        flex:1,
      },
      BtnRed:{
        borderRadius:5,
        padding:4,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'red',
        flex:1,
        marginRight:5
      },
      BtnBlue:{
        borderRadius:5,
        padding:4,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#0099e5',
        flex:1,
        marginRight:5
      },
    
      dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    }

});
 
const mapStateToProps = (state) => {
  return {
      userData: state.userData
  };
};

const mapActionToProps = {

};
export default connect(mapStateToProps, mapActionToProps)(RequestListScreen);
