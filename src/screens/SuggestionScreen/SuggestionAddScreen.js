import React, { Component } from 'react';
import { View,Image,TouchableOpacity,StatusBar,TextInput ,BackHandler} from 'react-native';
import { Text,Card, CardItem,Container, Content,Footer, FooterTab,Icon, Picker } from 'native-base';
import { Portal, Dialog } from 'react-native-paper';

import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import Colors from '../../utility/Colors';
import { hp, normalize, wp } from '../../responsive';
import Repository from '../../repository/Repository';

import TabItem from '../../components/TabItem';
import HeaderBack from '../../components/HeaderBack';
import { Button, Loading } from '../../components';
import Utils from '../../utility/Utils';

class SuggestionAddScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            tabBarIcon: ({ tintColor }) =>
                <TabItem
                    name={'افزودن انتقادات و پیشنهادات  '}
                    source={require('./../../assets/image/ic_residue.png')}
                    color={tintColor}
                />,
        };
    };
    constructor(props) {
      super(props);
      const {
        Subject,
        MessageText,
    } = this.props.userData.user;
      this.state = {
        MessageText:'',
        Subject:'',
        apiSuccess: false,
        apiSuccessDesc:'',
        apiSuccessTitle:'',
        apiError: false,
        apiErrorDesc: '',
        isLoadingButton:false
      };
    }
    componentDidMount = () => {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      }
      
      handleBackButton = () => {
        this.props.navigation.goBack();
        return true;
      } 
  
shouldComponentUpdate = (nextProps, nextState) => {
  return Utils.shallowCompare(this, nextProps, nextState);
}
onConfirm = () => {

  const {
    Subject,
    MessageText
  } = this.state;

  let error = false;

  if (Subject.length === 0) {
      error = true;
      this.setState({
          errorPayment: 'لطفا عنوان را وارد نمایید.'
      });
  }
  if (MessageText.length === 0) {
    error = true;
    this.setState({
        errorPayment: 'لطفا متن را وارد نمایید.'
    });
}
  if (error) {
      return;
  }
  if (!error) {
     
      this.setState({
          isLoadingButton: true,
          
      }, () => {
          this.PhoneAddSuggestionForHelperApi();
      });
  }
}
async PhoneAddSuggestionForHelperApi() {
        
  const headers = null;
  const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber,
      Subject: this.state.Subject,
      MessageText:this.state.MessageText,
  };
  try {
      const response = await Repository.PhoneAddSuggestionForHelperApi(params, headers);
      if (response.Success === 1) {
        
          this.setState({
              isLoadingButton:false,
              apiSuccess:true,
              apiSuccessDesc:response.Text,
              apiSuccessTitle:response.Titel,
              Payment:''
          });
      }
      else {
          this.setState({
              apiError: true,
              apiErrorDesc: response.Text
          });
      }
     
  } catch (error) {
      if (this.mounted) {
          this.setState({
              isLoadingButton: false,
              apiError: true,
              apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
          });
      }
  }
}
  
    render() {
        return (
            <Container>
               
               <Content>
               <StatusBar
                        backgroundColor={Colors.statusBar}
                        barStyle="light-content"
                    />
                    <HeaderBack
                        onBackPress={() => this.props.navigation.goBack()}
                        headerTitle={' افزودن پیشنهادات و انتقادات '}
                    />
         
          <View style={[styles.Center]}>
              <View style= {styles.inputTotal}>
             
                <TextInput   
                    placeholder="عنوان مورد نظر "  
                    style={[styles.input ]} placeholderTextColor={'#50b3ae'}
                    value={this.state.Subject}
                    onChangeText={(Subject) => {
                        this.setState({
                            Subject,
                          errorPayment: ''
                        });
                      }}
                />
           </View>
            </View>
            <View style={[styles.Center]}>
              <View style= {styles.inputTotal}>
             
                <TextInput   
                    placeholder="متن مورد نظر "  
                    style={[styles.input ]} placeholderTextColor={'#50b3ae'}
                    value={this.state.MessageText}
                    multiline={true}
                    onChangeText={(MessageText) => {
                        this.setState({
                            MessageText,
                          errorPayment: ''
                        });
                      }}
                />
           </View>
            </View>
            <Portal>
                        <Dialog
                            visible={this.state.apiError}
                            style={styles.dialogContainer}
                            dismissable={false}
                        >
                            <Dialog.Content>
                                <Text style={styles.dialogText}>
                                    {this.state.apiErrorDesc}
                                </Text>
                            </Dialog.Content>
                            <Dialog.Actions>
                                <Button
                                    buttonText={'تلاش مجدد'}
                                    onPress={() => {
                                        this.setState({
                                            apiError: false,
                                            isLoadingPage: true,
                                            list: []
                                        }, () => {
                                            this.getApi();
                                        });
                                    }}
                                    style={styles.dialogButton}
                                />
                            </Dialog.Actions>
                        </Dialog>
                        <Dialog
                            visible={this.state.apiSuccess}
                            style={styles.dialogContainer}
                            onDismiss={() => {
                                this.setState({
                                    apiSuccess: false,
                                });
                            }}
                        >
                         <Dialog.Title style={styles.dialogTitleText}>
                            {this.state.apiSuccessTitle}
                         </Dialog.Title>
                            <Dialog.Content style={styles.dialogContent}>

                               
                                <Text style={styles.dialogText}>
                                    {this.state.apiSuccessDesc}
                                </Text>
                                <Button
                                    buttonText={'بستن'}
                                    onPress={() => {
                                        this.setState({
                                            apiSuccess: false,
                                        });
                                    }}
                                    style={styles.dialogButton}
                                />
                            </Dialog.Content>
                           
                        </Dialog>
                    </Portal>
        </Content>
        <Footer style={{height:70,width:"100%"}}>
          <FooterTab  style={{backgroundColor:'#50b3ae'}}>
          <TouchableOpacity  
                style={[styles.BtnFooter]}
                onPress={() => {
                  this.onConfirm();
              }}
                                     >
                <Text style={[styles.textFooter]}>
                {this.state.isLoadingButton ? 'لطفا منتظر بمانید' : 'ثبت انتقادات و پیشنهادات '} 
                </Text>
            </TouchableOpacity>
          </FooterTab>
        </Footer>
      </Container>
        );
    }

}

  export 
  const styles = EStyleSheet.create({
    IRANSansNormal: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     IRANSansBold: {
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     header:{
      backgroundColor:'#50b3ae',
     },
     textHeader:{
      color:'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
      fontSize: 14,
     },
      text:{
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_M',
      },
        fontSize: 12,
       },

    inputTotal:{
        borderColor:'#b3afaf',
        backgroundColor:'white',
        borderRadius: 5,
        borderWidth: 1,
        justifyContent:'space-around',
        alignItems: 'center',
        flexDirection:'row',
        flex:1,
        marginTop:20,
        paddingHorizontal: 10,
        width:'90%',
      },
      input:{
        fontSize:12,
        color:'black',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
        flex:1,
        paddingHorizontal: 10,
        textAlign:'center'
      },
       textFooter:{
        color:'white',
        '@media ios': {
          fontFamily: '$IR',
          fontWeight: '500',
      },
      '@media android': {
          fontFamily: '$IR_B',
      },
        fontSize: 14,
        
       },
       BtnFooter:{
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
       },
       Center:{
        alignItems: 'center',
        justifyContent:'center',
       },
       TotalText:{
        flexDirection:'row',
        flex:1,  
        alignItems: 'center',
        justifyContent:'space-around',
        marginTop:20,
       },
       BtnShow:{
           marginTop:20,
           borderWidth:1,
           borderColor:'#50b3ae',
           borderRadius:5,
           padding:10,
           width:'60%',
           alignItems: 'center',
           justifyContent:'center',
       },
       pickerContainer: {
        height: hp('6%'),
        width: wp('70%'),
        //borderColor: Colors.border,
        //borderWidth: 1,
        //borderRadius: 20,
        alignSelf: 'center',
        textAlign: 'center',
        justifyContent: 'center',
    },
    pickerStyle: {
        // borderBottomWidth: 0.8,
        // borderColor: '#e2e2e2',
        width: wp('50%'),
        // justifyContent: 'center',
    },
    pickerHeaderStyle: {
        backgroundColor: Colors.purple,
    },
    pickerHeaderTextStyle: {
        width: wp('50%'),
        backgroundColor: 'transparent',
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: 'white',
        textAlign: 'right',
    },
    pickerBackHeaderTextStyle: {
        width: wp('50%'),
        backgroundColor: 'transparent',
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: 'white',
        textAlign: 'left',
    },
    pickerTextStyle: {
        fontSize: 16,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        color: '#34495e',
        textAlign: 'center',
    },
    pickerItemStyle: {
        width: wp('90%'),
        backgroundColor: 'transparent'
    },
    pickerItemTextStyle: {
        fontSize: 16,
        width: wp('65%'),
        backgroundColor: 'transparent',
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: '#34495e',
        textAlign: 'right'
    },
    pickerSeparator: {
        '@media android': {
            height: 1,
            width: wp('90%'),
            backgroundColor: '#e2e2e2'
        }
    },
    dialogContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding:0
        
    },
    dialogContent:{
        margin:0
    },
    dialogTitleText:{
        width: '100%',
        borderBottomColor: Colors.green,
        borderBottomWidth: 2,
        color:Colors.green,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
    },
    dialogText: {
        margin:0,
        fontSize: normalize(13),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': { 
            fontFamily: '$IR_M',
        },
        textAlign: 'center',
        color: Colors.textBlack,
    },
    dialogButton: {
        width: wp('30%')
    }
    

   });


const mapStateToProps = (state) => {
    return {
        userData: state.userData
    };
};

const mapActionToProps = {

};

export default connect(mapStateToProps, mapActionToProps)(SuggestionAddScreen);
