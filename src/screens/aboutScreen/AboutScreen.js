import React, {Component} from 'react';
import { connect } from 'react-redux';
import { StatusBar,BackHandler } from 'react-native';
import Colors from '../../utility/Colors';
import HeaderBack from '../../components/HeaderBack';
import Repository from '../../repository/Repository';

import { View,Image,TouchableOpacity,ScrollView,Linking} from 'react-native';
import { Container, Header,Content, Footer, FooterTab, Button, 
   Icon, Text,Card, CardItem ,Badge} from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';

class AboutScreen extends Component {
  handleClick = () => {
        Linking.openURL(this.state.list.Web);
  };
  handleClickDial = () => {
  Linking.openURL(`tel:`+this.state.list.Tel)
};

  constructor(props) {
    super(props);

    this.state = {
      horizontal: false,
      list: [],
    };
}

componentDidMount = () => {
  this.getApi();
  BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

handleBackButton = () => {
  this.props.navigation.goBack();
  return true;
} 

getApi = async () => {
  await this.PhoneSettingsForHelperApi();
  
}
async PhoneSettingsForHelperApi() {
  const headers = null;
  const params = {
      Mobile: this.props.userData.mobileNumber,
      Code: this.props.userData.codeNumber
  };
  try {
      const response = await Repository.PhoneSettingsForHelperApi(params, headers);
      if (response.Success === 1) {

          const list = response.Result;
          this.setState({
            list: list,
           
          });
      }
      else {
          this.setState({
              apiError: true,
              apiErrorDesc: response.Text
          });
      }
      this.setState({
          isLoadingPage: false,
      });
  } catch (error) {
      this.setState({
          isLoadingPage: false,
          apiError: true,
          apiErrorDesc: 'ارتباط دستگاه خود را بررسی کنید.'
      });
  }
}
  static navigationOptions = ({ navigation }) => {
    
    return {
     
        tabBarIcon: ({ tintColor,horizontal }) =>
            <TabItem
                name={'درباره ما'}
                source={require('./../../assets/image/ic_notice.png')}
                color={tintColor}
            />,
    };
};

  render() {
    return (
        <Container>
           <StatusBar
                    backgroundColor={Colors.statusBar}
                    barStyle="light-content"
                />
                <HeaderBack
                    onBackPress={() => this.props.navigation.goBack()}
                    headerTitle={'درباره ما'}
                />
          <Content>
              <View style={[styles.center,{paddingTop:20}]}>
                 <Image source={require('./../../assets/image/About.png')} style={styles.ImgProduct}/>  
              </View>
         
              <View style={[styles.center,{paddingTop:20}]}>
                 <TouchableOpacity
                   style={[styles.BtnContent]}
                   onPress={this.handleClick}
                     >
                     <Text style={[styles.textBtn]}>{this.state.list.Web}</Text>
                  </TouchableOpacity>
                 </View>
                 <View style={[styles.center,{paddingTop:20}]}>
                 <TouchableOpacity
                   style={[styles.BtnContent]}
                   onPress={this.handleClickDial}
                     >
                     <Text style={[styles.textBtn]}>{this.state.list.Tel}</Text>
                  </TouchableOpacity>
                 </View>
               
                 <View style={[styles.center,{paddingTop:20}]}>
                   <Card style={styles.Box}>
                    <CardItem>
                      <Text style={styles.text}>{this.state.list.about}</Text>
                   </CardItem>
                  </Card>
              </View>
              <View style={[styles.center,{paddingTop:20}]}>
                   <Card style={styles.Box}>
                    <CardItem>
                    <Text style={styles.text}>  {'نسخه اپلیکیشن : 0.0.32'}</Text>
                   </CardItem>
                  </Card>
              </View>
        </Content>
      </Container>
    );
  }
}
const styles = EStyleSheet.create({
  IRANSansNormal: {
    '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
   },
   IRANSansBold: {
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_B',
  },
   },
   header:{
    backgroundColor:'#50b3ae',
   },
    textHeader:{
      color:'white',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
    textBtn:{
      color:'#e2f3ba',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_B',
    },
     },
     text:{
      color:'black',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
     },
     ImgProduct:{
      width:'85%',
      height: 80,
      resizeMode:'contain',
     
    },
     center:{
         alignItems:'center',
         justifyContent:'center'
     },
     BtnContent:{
       backgroundColor:'#50b3ae',
         width:'90%',
         padding:10,
         borderRadius: 5,
         alignItems:'center',
         justifyContent:'center',
      
     },
     IconTotal:{
      flexDirection:'row',
      justifyContent:"center",
      alignItems:'center'
    },
    Box:{
      width:'90%',
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent:'center',
      borderRadius: 5,
      borderColor:'#8a987f'
    },
    textGreen:{
      color: '#50b3ae',
      '@media ios': {
        fontFamily: '$IR',
        fontWeight: '500',
    },
    '@media android': {
        fontFamily: '$IR_M',
    },
    },
});
 
const mapStateToProps = (state) => {
  return {
      userData: state.userData
  };
};

const mapActionToProps = {

};
export default connect(mapStateToProps, mapActionToProps)(AboutScreen);
