import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EStyleSheet from 'react-native-extended-stylesheet';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import { hp, normalize, wp } from '../responsive';
import Colors from '../utility/Colors';

const HeaderMain = (
    {
        userData,
        headerTitle,
        onMenuPress,
        onPinPress
    }
) => {
    return (
        <View style={mainHeaderStyle.header} >
            <View style={mainHeaderStyle.leftButton} >
                <TouchableOpacity
                    style={mainHeaderStyle.messageContainer}
                    activeOpacity={0.6}
                    onPress={onPinPress}
                >
                    <Ionicons 
                        name={'md-pin'}
                        size={35}
                        color={'#fff'}
                    />
                </TouchableOpacity>
            </View>
            <View style={mainHeaderStyle.backTitle}>
                <Text style={mainHeaderStyle.backTitle}>
                    {headerTitle}
                </Text>
            </View>
            <View style={mainHeaderStyle.rightButton} >
                {/* <TouchableOpacity
                    style={mainHeaderStyle.menuContainer}
                    activeOpacity={0.6}
                    onPress={onMenuPress}
                >
                    <FastImage
                        source={require('./../assets/image/ic_menu.png')}
                        resizeMode={FastImage.resizeMode.contain}
                        style={mainHeaderStyle.menu}
                    />
                </TouchableOpacity> */}
                <TouchableOpacity
                        onPress={onMenuPress}
                        activeOpacity={0.6}
                        style={mainHeaderStyle.menuContainer}
                    >
                        {/* <Text style={mainHeaderStyle.buttonText}>
                            {'خروج از حساب کاربری'}
                        </Text> */}
                        <Ionicons
                            name={'md-exit'}
                            size={25}
                            style={mainHeaderStyle.menu}
                        />
                    </TouchableOpacity>
            </View>
        </View>
    );
};

const mainHeaderStyle = EStyleSheet.create({
    header: {
        '@media ios': {
            paddingTop: 21,
            height: hp('15%')
        },
        '@media android': {
            height: hp('12%')
        },
        backgroundColor: Colors.green,
        flexDirection: 'row',
    },

    leftButton: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        alignSelf: 'center'
    },
    rightButton: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        alignSelf: 'center'
    },
    
    backTitle: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M'
        },
        height: wp('13%'),
        marginTop : 10,
        color: '#ffffff',
        fontSize: normalize(16),
        textAlign: 'right',
        marginLeft: 3,
    },
    title: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M'
        },
        color: '#675beb',
        fontSize: normalize(11),
        textAlign: 'right',
        marginLeft: 5,
        marginRight: 5
    },
    messageContainer: {
        height: wp('13%'),
        width: wp('13%'),
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    message: {
        height: wp('8%'),
        width: wp('8%'),
    },
    badgeContainer: {
        position: 'absolute',
        bottom: 5,
        right: 0,
        backgroundColor: Colors.badge,
        height: wp('5%'),
        width: wp('5%'),
        borderRadius: wp('2.5%')
    },
    badgeText: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M'
        },
        color: '#fff',
        fontSize: normalize(10),
        textAlign: 'center',
    },

    menuContainer: {
        height: wp('13%'),
        width: wp('13%'),
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    buttonText: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: Colors.black,
        fontSize: normalize(15),
        textAlign: 'right',
    },
    menu: {
        height: wp('9%'),
        width: wp('9%'),
        color:Colors.white
    },
    logo: {
        height: wp('13%'),
        width: wp('13%'),
        alignSelf: 'center'
    },

});

export default connect(
    (state) => ({
        userData: state.userData,
    }),
    null
)(HeaderMain);

/*
<TouchableOpacity
style={mainHeaderStyle.messageContainer}
activeOpacity={0.6}
onPress={onMessagePress}
>
<FastImage
    source={require('./../assets/image/ic_message.png')}
    resizeMode={FastImage.resizeMode.contain}
    style={mainHeaderStyle.message}
/>
<View style={mainHeaderStyle.badgeContainer}>
    <Text style={mainHeaderStyle.badgeText}>
        {'6'}
    </Text>
</View>
</TouchableOpacity>
*/
