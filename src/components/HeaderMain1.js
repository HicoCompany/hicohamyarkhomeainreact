import { Icon } from 'native-base';
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { connect } from 'react-redux';
import { hp, moderateScale, normalize } from '../responsive';
import Colors from '../utility/Colors';

const HeaderMain1 = (
    {
        userData,
        headerTitle,
        onBackPress,
        backTitle,
    }
) => {
    return (
        <View style={backHeaderStyle.header}>
            <View style={backHeaderStyle.leftButton}>
                <TouchableOpacity
                    onPress={onBackPress}
                    activeOpacity={0.4}
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        alignSelf: 'center',
                        padding: 10
                    }}
                >
                    {/* <Icon
                        name="ios-arrow-back"
                        style={{ fontSize: moderateScale(20), color: '#ffffff' }}
                    /> */}
                    {/* <Text style={backHeaderStyle.backTitle}>
                        {backTitle || 'بازگشت'}
                    </Text> */}
                </TouchableOpacity>
            </View>
            <View style={backHeaderStyle.rightButton}>
                <Text style={backHeaderStyle.title}>
                    {headerTitle}
                </Text>
            </View>
        </View>
    );
};

const backHeaderStyle = EStyleSheet.create({
    '@media (min-width: 600) and (min-height: 900)': {
        header: {
            '@media ios': {
                paddingTop: 25,
                height: hp('12%')
            },
            '@media android': {
                height: hp('10%')
            },
            backgroundColor: Colors.toolbar,
            flexDirection: 'row',
        },
    },
    header: {
        '@media ios': {
            paddingTop: 21,
            height: hp('15%')
        },
        '@media android': {
            height: hp('12%')
        },
        backgroundColor: Colors.green,
        paddingVertical: 10,
        flexDirection: 'row',
    },

    leftButton: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        alignSelf: 'center',
        marginLeft: 10
    },
    rightButton: {
        flex: 2,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        alignSelf: 'center',
        marginRight: 10
    },

    title: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M'
        },
        color: '#ffffff',
        fontSize: normalize(11),
        textAlign: 'right',
    },
    backTitle: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M'
        },
        color: '#ffffff',
        fontSize: normalize(13),
        textAlign: 'right',
        marginLeft: 3,
    }
});

export default connect(
    (state) => ({
        userData: state.userData,
    }),
    null
)(HeaderMain1);
