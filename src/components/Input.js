import { Input as TextInput, Text, View } from 'native-base';
import React from 'react';
import { Dimensions } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Colors from '../utility/Colors';
import Utils from './../utility/Utils';

const { height, width } = Dimensions.get('window');

class Input extends React.Component {

    shouldComponentUpdate (nextProps, nextState)
    {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    render () 
    {
      return (
        <View >
            <View style={[customFormStyle.viewInput, this.props.viewStyle]}>
                {this.props.leftIcon &&
                    <View style={{ flex: 0.1, justifyContent: 'center' }} >
                        {this.props.leftIcon}
                    </View>
                }
                <View style={{ flex: 0.9, justifyContent: 'center' }}>
                    <TextInput
                        autoFocus={this.props.autoFocus}
                        placeholder={this.props.placeholder}
                        placeholderTextColor={this.props.placeholderTextColor}
                        onFocus={this.props.onFocus}
                        maxLength={this.props.maxLength}
                        keyboardType={this.props.keyboardType}
                        returnKeyType={this.props.returnKeyType}
                        onSubmitEditing={this.props.onSubmitEditing}
                        blurOnSubmit={this.props.blurOnSubmit}
                        style={[customFormStyle.input, this.props.inputStyle]}
                        underlineColorAndroid={this.props.underlineColorAndroid}
                        selectionColor={this.props.selectionColor}
                        secureTextEntry={this.props.secureTextEntry}
                        onChangeText={this.props.onChangeText}
                        value={this.props.value}
                        editable={this.props.editable}
                    />
                </View>
            </View>
            <Text style={customFormStyle.textError}>{this.props.error}</Text>
        </View>
      );
    }
}

const customFormStyle = EStyleSheet.create({
    viewInput: {
        height: 40,
        width: width / 1.2,
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 10,
        borderRadius: 3,
        borderBottomWidth: 2,
        borderBottomColor: '#a6a6a6'
    },
    input: {
        height: 40,
        backgroundColor: 'transparent',
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '300',
        },
        '@media android': {
            fontFamily: '$IR_L',
        },
        textAlign: 'center',
        color: Colors.grayHard,
        fontSize: 12,
        margin: 5,
    },
    textError: {
        fontSize: 14,
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: Colors.red,
        textAlign: 'center'
    }
});

export { Input };
