import { array, bool, func, object, oneOf, string } from 'prop-types';
import React, { Component } from 'react';
import { Text, TouchableOpacity, View, ViewPropTypes, ActivityIndicator } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { normalize, scale, wp } from '../responsive';
import Colors from '../utility/Colors';
import Utils from '../utility/Utils';

class Button extends Component
{

    static ButtonType = {
        NORMAL: 0,
        SEPARATED: 1
    };

    constructor(props)
    {
        super(props);
    }

    shouldComponentUpdate(nextProps, nextState)
    {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    renderButton()
    {
        const {
            type,
            buttonSeparatorContainer,
            buttonSeparatorInner,
            separatorStyle,
            leftButton,
            rightButton,
            separatorColor,
            showIconRemove,

            buttonText,
            onPress,
            colors,
            style,
            textColor,
        } = this.props;
        if (type === Button.ButtonType.NORMAL)
        {
            return (
                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={onPress}
                    style={this.props.outerStyle}
                >
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        colors={colors}
                        style={[buttonStyle.buttonInner, style]}
                    >
                        <Text style={[buttonStyle.buttonText, { color: textColor }]}>{buttonText}</Text>
                    </LinearGradient>
                </TouchableOpacity>
            );
        }
        if (type === Button.ButtonType.SEPARATED)
        {
            return (
                <View style={[buttonStyle.buttonSeparatorContainer, buttonSeparatorContainer]}>
                    <View style={[buttonStyle.buttonSeparatorInner, buttonSeparatorInner]}>
                        <TouchableOpacity
                            style={[buttonStyle.buttonLeft, leftButton.style]}
                            onPress={leftButton.onPress}
                            activeOpacity={0.6}
                        >
                            <Text style={buttonStyle.buttonText}>
                                {leftButton.title}
                            </Text>
                            {showIconRemove &&
                                <FontAwesome
                                    name={'remove'}
                                    size={scale(11)}
                                    color={'#ffffff'}
                                    style={buttonStyle.buttonLeftIconStyle}
                                />
                            }
                        </TouchableOpacity>
                        <View
                            style={[{ width: 8, backgroundColor: separatorColor }, separatorStyle]}
                        />
                        <TouchableOpacity
                            style={[buttonStyle.buttonRight, rightButton.style]}
                            onPress={rightButton.onPress}
                            activeOpacity={0.6}
                        >
                            <Text style={buttonStyle.buttonText}>{rightButton.title}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }
        return null;
    }

    render()
    {
        const { isLoading, buttonText, style, textColor } = this.props;

        return isLoading ? (
            <View style={[buttonStyle.button, style]} >
                <ActivityIndicator
                    style={buttonStyle.spinner}
                    size={'small'}
                    color={'white'}
                />
                <Text style={[buttonStyle.buttonText, { color: textColor }]}>{'لطفا منتظر بمانید'}</Text>
            </View>
        ) : (
             this.renderButton()
        );
    }
    
}

Button.propTypes = {
    isLoading: bool,
    buttonText: string,
    onPress: func,
    style: ViewPropTypes.style,
    colors: array,
    type: oneOf([0, 1]),
    rightButton: object,
    leftButton: object,
    separatorColor: string,
    
    buttonSeparatorContainer: ViewPropTypes.style,
    buttonSeparatorInner: ViewPropTypes.style,
    separatorStyle: ViewPropTypes.style,
    showIconRemove: bool
};

Button.defaultProps = {
    textColor: '#FFFFFF',
    isLoading: false,
    buttonText: '',
    onPress: () => {},
    style: {},
    colors: ['#50b3ae', '#50b3ae'],
    rightButton: {
        title: 'Right',
        onPress: () => {},
        style: {},
    },
    leftButton: {
        title: 'Left',
        onPress: () => {},
        style: {},
    },
    separatorColor: '#FFFFFF55',
    type: Button.ButtonType.NORMAL,
    buttonSeparatorContainer: {},
    buttonSeparatorInner: {},
    separatorStyle: {},
    showIconRemove: true
};

const buttonStyle = EStyleSheet.create({
    '@media (min-width: 600) and (min-height: 900)': {

    },
    button: {
        flexDirection: 'row',
        height: 42,
        width: wp('95%'),
        borderRadius: 40,
        backgroundColor: '#85858599',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding: 10,
        margin: 10,
    },
    buttonInner: {
        height: 42,
        width: wp('95%'),
        borderRadius: 40,
        backgroundColor: Colors.purple,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        padding: 3,
        margin: 10,
        elevation: 1,
    },
    buttonText: {
        fontSize: normalize(12),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: 'white',
        textAlign: 'center'
    },
    spinner: {
        marginRight: 10
    },

    buttonSeparatorContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10
    },
    buttonSeparatorInner: {
        height: 44,
        width: wp('95%'),
        flexDirection: 'row',
        justifyContent: 'center'
    },
    buttonLeft: {
        flex: 0.3,
        paddingLeft: 3,
        paddingRight: 3,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 5,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 5,
        backgroundColor: '#fe5050',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    buttonRight: {
        flex: 0.7,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 20,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 20,
        backgroundColor: '#675beb',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    buttonLeftIconStyle: {
        marginLeft: 10
    }
});

export { Button };

