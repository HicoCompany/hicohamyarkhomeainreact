import { Text, View } from 'native-base';
import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import FastImage from 'react-native-fast-image';
import { Colors } from 'react-native-paper';
import { normalize, wp } from '../responsive';
import Button from './Button';

const Error = ({ message, isLoading, onPress }) => {
    return (
        <View style={loadingStyles.container}>
            <FastImage
                // source={require('./../assets/image/ic_error.png')}
                resizeMode={FastImage.resizeMode.contain}
                style={loadingStyles.image}
            />
            <Text style={loadingStyles.warning}>
                {(message !== '' || message !== undefined) ? message : 'ارتباط دستگاه خود را بررسی کنید.'}
            </Text>
            <Button 
                isLoading={isLoading}
                buttonText={'تلاش مجدد'}
                onPress={onPress}
                style={{ width: wp('60%') }}
            />
        </View>
    );
};

const loadingStyles = EStyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        flex: 1,
        backgroundColor: Colors.backgroundColor,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    warning: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: '#34495e',
        fontSize: normalize(14),
        textAlign: 'center',
        margin: 10
    },
    image: {
        marginTop: 10,
        height: wp('20%'),
        width: wp('20%'),
        alignSelf: 'center'
    },
});

export { Error };
