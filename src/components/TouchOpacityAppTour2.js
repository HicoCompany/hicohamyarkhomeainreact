import React, { Component } from 'react'
import { StyleSheet, View, Button, Platform,TouchableOpacity,Image } from 'react-native'
import { Text,Card, CardItem,Container, Content, } from 'native-base';
import EStyleSheet from 'react-native-extended-stylesheet';
import Colors from '../utility/Colors';
import { hp, normalize, wp } from '../responsive';

import { AppTour, AppTourView } from 'react-native-app-tour'

class TouchOpacityAppTour2 extends Component {
  render() {
    return (
        <TouchableOpacity
          key={'Center Left'}
          title={'Center Left'}
          ref={ref => {
            if (!ref) return

            this.TouchableOpacity1 = ref

            let props = {
              order: 14,
              title: ' درخواست پکیج',
              description: 'در این قسمت نوع پسماند تفکیک شده خود را انتخاب و زمان جمع آوری پسماند را توسط سفیران پاکی مشخص مینمایید ',
              outerCircleColor: '#3f52ae',
              cancelable: false
            }

            this.props.addAppTourTarget &&
              this.props.addAppTourTarget(AppTourView.for(ref, { ...props }))
          }}
          onPress={() => {
            this.props.navigation.navigate("SelectPackageScreen");
          }}
       >
                   <View>
                     <Card style={styles.BoxProduct}>
                         <CardItem header style={{borderBottomWidth:1,borderBottomColor:'gray'}}>
                           <Image source={require('./../assets/image/ic_wareR.png')} style={styles.ImgProduct}/>
                           </CardItem>
                    <Text style={styles.textProduct}>{"درخواست پکیج"}</Text>
                     
                     </Card>
                   </View>
                 </TouchableOpacity>
    )
  }
}

const styles = EStyleSheet.create({

    IRANSansNormal: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_M',
        },
     },
     IRANSansBold: {
        fontSize: normalize(10),
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: '500',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
     },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  BoxProduct:{
    width:160,
    height:160,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  ImgProduct:{
    width:'85%',
    height: 80,
    resizeMode:'contain',
   color:Colors.black
  },
  textProduct: {
    color: 'black',
    fontSize: 14,
    '@media ios': {
      fontFamily: '$IR',
      fontWeight: '500',
  },
  '@media android': {
      fontFamily: '$IR_B',
  }}
})

export default TouchOpacityAppTour2
