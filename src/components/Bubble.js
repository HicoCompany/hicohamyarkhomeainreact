import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import Colors from '../utility/Colors';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingVertical: 16,
    minHeight: 60,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  myLocation: {    
    backgroundColor: Colors.statusBar,
    padding: 10,
    margin: 10,
    borderRadius: 10,    
  },
});

class Bubble extends React.PureComponent {
  static propTypes = {
    onPress: PropTypes.func,
  };
  render() {
    let innerChildView = this.props.children;
    if (this.props.onPress) {
      innerChildView = (
        <TouchableOpacity 
          style={styles.myLocation}
          onPress={this.props.onPress}
        >
          {this.props.children}
        </TouchableOpacity>
      );
    }
    return (
      <View style={[styles.container, this.props.style]}>
        {innerChildView}
      </View>
    );
  }
}

export default Bubble;
