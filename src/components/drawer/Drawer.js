import React, { Component } from 'react';
import { Text, TouchableOpacity, View, ScrollView } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EStyleSheet from 'react-native-extended-stylesheet';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import { exitApp } from '../../redux/actions';
import Utils from '../../utility/Utils';
import { hp, wp, normalize } from './../../responsive';
import Colors from './../../utility/Colors';

class Drawer extends Component {

    constructor(props) {
        super(props);
        this.onLogOutClick = this.onLogOutClick.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return Utils.shallowCompare(this, nextProps, nextState);
    }

    onLogOutClick() {
        this.props.exitApp();
        this.props.navigation.replace('splash');
    }

    render() {
        return (
            <View style={drawerStyle.container} >
                <View style={drawerStyle.topLayer} >
                    <FastImage
                        source={require('./../../assets/image/ic_logo_login.png')}
                        resizeMode={FastImage.resizeMode.contain}
                        style={drawerStyle.logo}
                    />
                </View>
                <ScrollView>
                    <TouchableOpacity
                           onPress={() => this.props.navigation.navigate("profile")}
                        
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonText}>
                            {'اطلاعات کاربر'}
                        </Text>
                        <Ionicons
                            name={'ios-contact'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                           onPress={() => this.props.navigation.navigate("MapAreeScreen")}
                        
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonText}>
                            {'نقشه مناطق'}
                        </Text>
                        <Ionicons
                            name={'ios-contact'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                           onPress={() => this.props.navigation.navigate("DeliveryTypeScreen")}
                        
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonText}>
                            {' مدیریت اعتباری'}
                        </Text>
                        <Ionicons
                            name={'md-card'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                    {/* <TouchableOpacity
                           onPress={() => this.props.navigation.navigate("SelectPackageScreen")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonText}>
                            {'درخواست پکیج'}
                        </Text>
                        <Ionicons
                            name={'md-basket'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity> */}
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate("DeliveredwasteScreen")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonText}>
                            {'تحویل پسماند'}
                        </Text>
                        <Ionicons
                            name={'ios-trash'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate("RequestListScreen")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonText}>
                            {'لیست درخواست ها'}
                        </Text>
                        <Ionicons
                            name={'ios-list'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate("report")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonText}>
                            {'گزارشات'}
                        </Text>
                        <Ionicons
                            name={'ios-stats'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                  
                    <TouchableOpacity
                         onPress={() => this.props.navigation.navigate("notice")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonText}>
                            {'اطلاع رسانی'}
                        </Text>
                        <Ionicons
                            name={'ios-search'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                         onPress={() => this.props.navigation.navigate("IntroductionScreen")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonText}>
                            {'معرفی به دوستان'}
                        </Text>
                        <Ionicons
                            name={'md-gift'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                    {/* <TouchableOpacity
                        // onPress={this.onLogOutClick}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonText}>
                            {'تماس با پشتیبانی'}
                        </Text>
                        <Ionicons
                            name={'md-call'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity> */}
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate("AboutScreen")}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonText}>
                            {'درباره ما'}
                        </Text>
                        <Ionicons
                            name={'ios-more'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.onLogOutClick}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonText}>
                            {'خروج از حساب کاربری'}
                        </Text>
                        <Ionicons
                            name={'md-exit'}
                            size={25}
                            style={drawerStyle.buttonIcon}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.onLogOutClick}
                        activeOpacity={0.7}
                        style={drawerStyle.buttonItem}
                    >
                        <Text style={drawerStyle.buttonTextVersion}>
                            {'ورژن : 0.0.18'}
                        </Text>
                       
                    </TouchableOpacity>
                </ScrollView>
            </View>
        );
    }

}

const drawerStyle = EStyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white
    },
    topLayer: {
        height: hp('25%'),
        backgroundColor: Colors.green,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        height: wp('30%'),
        width: wp('30%')
    },

    buttonItem: {
        height: hp('7%'),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginRight: 10
    },
    buttonIcon: {
        marginLeft: 10
    },
    buttonText: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: Colors.black,
        fontSize: normalize(15),
        textAlign: 'right',
    }
    ,
    buttonTextVersion: {
        '@media ios': {
            fontFamily: '$IR',
            fontWeight: 'bold',
        },
        '@media android': {
            fontFamily: '$IR_B',
        },
        color: Colors.black,
        fontSize: normalize(15),
        textAlign: 'center',
    }
});

const mapStateToProps = (state) => {
    return {
        userData: state.userData || {}
    };
};

const mapActionToProps = {
    exitApp
};

export default connect(mapStateToProps, mapActionToProps)(Drawer);
