import {
    SET_USER,
    SET_IS_LOGIN,
    SET_IS_SEEN_INTRODUCE,
    SET_MOBILE_NUMBER,
    SET_CODE_NUMBER,
    EXIT_APP,
    SET_IS_SEEN_TOUR,
    SET_IS_MAPID,
    SET_IS_ADDRESS
} from './types';

export const setUser = (data) => {
    return {
        type: SET_USER,
        payload: data
    };
};
export const setMapId = (data) => {
    return {
        type: SET_IS_MAPID,
        payload: data
    };
};
export const setMapAddress = (data) => {
    return {
        type: SET_IS_ADDRESS,
        payload: data
    };
};
export const setIsLogin = () => {
    return {
        type: SET_IS_LOGIN
    };
};

export const setIsSeenIntroduce = () => {
    return {
        type: SET_IS_SEEN_INTRODUCE
    };
};
export const setIsSeenTour = () => {
    return {
        type: SET_IS_SEEN_TOUR
    };
};
export const setMobileNumber = (mobile) => {
    return {
        type: SET_MOBILE_NUMBER,
        payload: mobile
    };
};

export const setCodeNumber = (code) => {
    return {
        type: SET_CODE_NUMBER,
        payload: code
    };
};

export const exitApp = () => {
    return {
        type: EXIT_APP,
    };
};

