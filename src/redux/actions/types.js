export const SET_USER = 'SET_USER';

export const SET_IS_SEEN_INTRODUCE = 'SET_IS_SEEN_INTRODUCE';
export const SET_IS_LOGIN = 'SET_IS_LOGIN';
export const SET_MOBILE_NUMBER = 'SET_MOBILE_NUMBER';
export const SET_CODE_NUMBER = 'SET_CODE_NUMBER';
export const SET_IS_SEEN_TOUR = 'SET_IS_SEEN_TOUR';
export const EXIT_APP = 'EXIT_APP';
export const SET_IS_MAPID = 'SET_IS_MAPID';
export const SET_IS_ADDRESS = 'SET_IS_ADDRESS';
