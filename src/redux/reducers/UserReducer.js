import update from 'immutability-helper';
import {
    SET_USER,
    SET_IS_LOGIN,
    SET_IS_SEEN_INTRODUCE,
    SET_MOBILE_NUMBER,
    SET_CODE_NUMBER,
    EXIT_APP,
    SET_IS_SEEN_TOUR,
    SET_IS_MAPID,
    SET_IS_ADDRESS
} from '../actions/types';

const INITIAL_STATE = {
    isSeenIntroduce: false,
    isSeenTour: false,
    isLogin: false,
    mobileNumber: '',
    codeNumber: '',
    mapid:'-1',
    address:'',
    user: {
        AccountNumber: '',
        AccountOwner: '',
        Adress: '',
        BankID: -1,
        BankTitle: '',
        BoothId: -1,
        CharityID: -1,
        Code: '',
        DeliveryWasteID: -1,
        DeviceID: '',
        DiscountCardId: -1,
        Email: '',
        Family: '',
        HelperId: -1,
        IdentificationCode: '',
        LatitudeLocation: null,
        LongitudeLocation: null,
        MainAreaID: -1,
        MainAreaTitle: '',
        Mobile: '',
        Name: '',
        NationalCode: '',
        NoticesID: -1,
        Page: -1,
        Password: null,
        Payment: -1,
        ReagentCode: -1,
        Sex: -1,
        ShabaNumber: '',
        UserName: null,
        WareId: -1,
        WasteTypeID: -1
    }
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SET_USER:
            return {
                ...state,
                user: action.payload
            };
        case SET_IS_LOGIN:
            return {
                ...state,
                isLogin: true
            };
        case SET_IS_SEEN_INTRODUCE:
            return {
                ...state,
                isSeenIntroduce: true,
            };
        case SET_IS_SEEN_TOUR:
            return {
                ...state,
                isSeenTour: true,
            };
        case SET_MOBILE_NUMBER:
            return {
                ...state,
                mobileNumber: action.payload,
            };
        case SET_CODE_NUMBER:
            return {
                ...state,
                codeNumber: action.payload,
            };
        case SET_IS_MAPID:
            return {
                ...state,
                mapid: action.payload,
            };
        case SET_IS_ADDRESS:
            return {
                ...state,
                address: action.payload,
            };
        case EXIT_APP:
            return {
                ...state,
                isLogin: false,
                mobileNumber: '',
                codeNumber: ''
            };

        default:
            return state;
    }
};
