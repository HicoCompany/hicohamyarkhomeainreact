import moment from 'moment-jalaali';
import ImagePicker from 'react-native-image-picker';
import md5 from './MD5';

const Utils = {
    /***
     *
     * Add a ',' in strings fo money type ( every three characters )
     * @returns {*}
     * @param num
     * @param persianDigit
     */
    formatMoney(num, persianDigit = false) {
        if (num === 'undefined' || typeof num === undefined) {
            return '';
        }
        // if (typeof num !== 'number') {
        //     num = parseInt(num.replace(',', ''));
        // }
        let n = num,
            c = 0, //isNaN(c = Math.abs(c)) ? 2 : c,
            d = '',
            t = ',',
            s = n < 0 ? '-' : '',
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;
        const result = s + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, `$1${t}`) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
        return persianDigit ? Utils.toPersianDigits(result) : result;
    },

    formatMoneyInput(num) {
        let number = '';

        number = num.replace(/\D/g, '');

        number = number.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, '$1,');

        return number;
    },

    /***
     *
     * Add three charachter seprator in mobile numbers strings
     * @param number
     * @param toPersianDigits
     * @returns {*}
     */
    formatMobile(number, toPersianDigits = false) {
        if (typeof number === 'undefined') return '';

        number += ''; // Convert to string

        let result = '';

        if (number.length >= 11) {
            result =
                `${number.substr(0, 4)} ${
                    number.substr(4, 3)} ${
                    number.substr(7, 2)} ${
                    number.substr(9)}`;
        }

        return toPersianDigits ? Utils.toPersianDigits(result) : result;
    },

    /***
     *
     * Convert english digits to global persian digits
     * @returns {*}
     * @param text
     */
    toPersianDigits(text) {
        if (typeof text === 'undefined' || text === null) {
            return '';
        }
        const id = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        return text.replace(/[0-9]/g, (w) => {
            return id[+w];
        });
    },

    /***
     *
     * Convert persian & arabic digits to global digits
     * @param number
     * @returns {*}
     */
    toEnglishDigits(number) {
        return number.replace(/[\u0660-\u0669]/g, (c) => {
            return c.charCodeAt(0) - 0x0660;
        }).replace(/[\u06f0-\u06f9]/g, (c) => {
            return c.charCodeAt(0) - 0x06f0;
        });
    },

    /***
     *
     * The input number is a valid mobile number or not
     * @returns {*}
     * @param mobile
     */
    mobileIsValid(mobile) {
        // const NUMBER_REGEX = /^\d+$/;
        const NUMBER_REGEX = /(9[0-9]{9})|(09[0-9]{9})/;
        // if (mobile.length < 1)
        //     return 'شماره همراه وارد نشده است';
        // if (mobile.length < 10)
        //     return 'شماره همراه صحیح نیست';
        if (!NUMBER_REGEX.test(mobile)) {
            return 'شماره همراه صحیح نیست';
        }
        // if (mobile.substr(0, 1) != '9')
        //     return 'شماره همراه صحیح نیست';
        return '';
    },

    /***
     *
     * Only string !
     * @returns {*}
     * @param string
     */
    justString(string) {
        const STRING_REGEX = /^[A-z, ا-ی]+$/;
        if (STRING_REGEX.test(string)) {
            return '';
        }
        return 'فرمت اشتباه است';
    },

    nationalCodeIsValid(token) {
        const NUMBER_REGEX = /^\d+$/;
        if (token.length < 4) {
            return 'شماره ملی صحیح نیست';
        }
        if (!NUMBER_REGEX.test(token)) {
            return 'شماره ملی صحیح نیست';
        }
        return '';
    },

    /***
     *
     * Create a dialog for picking images from gallery & camera in live
     * @returns {*}
     * @param callback
     */
    imagePicker(callback) {
        const options = {
            title: 'انتخاب عکس',
            cancelButtonTitle: 'انصراف',
            takePhotoButtonTitle: 'گرفتن عکس',
            chooseFromLibraryButtonTitle: 'انتخاب عکس از گالری',
            customButtons: [],
            storageOptions: {
                skipBackup: false,
                path: 'images',
                cameraRoll: true,
                allowsEditing: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                // console.log('User cancelled image picker');
            }
            else if (response.error) {
                // console.log('ImagePicker Error: ', response.error);
                callback('', response, true);
            }
            else if (response.customButton) {
                // console.log('User tapped custom button: ', response.customButton);
            }
            else {
                const source = { uri: response.uri };
                callback(source, response, false);
            }
        });
    },

    MD5(value) {
        return md5(value);
    },

    /***
     *
     * One or more property is in second object or not
     * @returns {*}
     * @param objA
     * @param objB
     */
    shallowEqual(objA: mixed, objB: mixed): boolean {
        if (objA === objB) {
            return true;
        }

        if (typeof objA !== 'object' || objA === null ||
            typeof objB !== 'object' || objB === null) {
            return false;
        }

        const keysA = Object.keys(objA);
        const keysB = Object.keys(objB);

        if (keysA.length !== keysB.length) {
            return false;
        }

        // Test for A's keys different from B.
        const bHasOwnProperty = hasOwnProperty.bind(objB);
        for (let i = 0; i < keysA.length; i++) {
            if (!bHasOwnProperty(keysA[i]) || objA[keysA[i]] !== objB[keysA[i]]) {
                return false;
            }
        }
        return true;
    },

    /***
     *
     * Compare object for better performance in the render of react native pages
     * @returns {*}
     * @param instance
     * @param nextProps
     * @param nextState
     */
    shallowCompare(instance, nextProps, nextState) {
        return (
            !Utils.shallowEqual(instance.props, nextProps) ||
            !Utils.shallowEqual(instance.state, nextState)
        );
    },

    /***
     *
     * Convert unix timestamp to human readable format in jalali calendar
     * @param {number} Date timestamp
     * @param format
     * @returns {*}
     */
    convertTimeStampToJalali(Date, format) {
        if (Date !== null && !isNaN(Date)) {
            return moment.unix(Date).format(format || 'jYYYY/jMM/jDD');
        }
        return '';
    },

    convertDateToTimeStamp(date) {
        return new Date(date.toString()).getTime() / 1000;
    },

    convertDateToJalali(date) {
        return moment.unix(new Date(date.toString()).getTime() / 1000).format('jYYYY/jMM/jDD');
    },

    createDateJalaali(maxDate, minDate) {
        let MaxMonth;
        let MaxYear;
        let MaxDay;
        let m;
        if (maxDate === undefined) {
            maxDate = moment();
            maxDate.add(3, 'jYear');
            MaxMonth = maxDate.jMonth();
            MaxYear = maxDate.jYear();
            MaxDay = maxDate.jDate();
        } else {
            m = moment(maxDate, 'jYYYY/jM/jD');
            MaxMonth = m.jMonth();
            MaxYear = m.jYear();
            MaxDay = m.jDate();
        }
        m = minDate === undefined ? moment() : moment(minDate, 'jYYYY/jM/jD');
        const month = m.jMonth();
        const year = m.jYear();
        const day = m.jDate();

        const data = [];
        for (let i = 0; m.jYear() <= MaxYear; i++) {
            const _year = m.jYear();
            const months = [];
            for (let j = 0; j < 12; j++) {
                let daysLength = (moment.jIsLeapYear(_year)) ? 30 : 29;
                if (j !== 11) {
                    daysLength = 30;
                }
                if (j <= 5) {
                    daysLength = 31;
                }
                if (_year === MaxYear && j > MaxMonth) break;
                if (year === _year && j < month) continue;
                const days = [];
                for (let k = 0; k < daysLength; k++) {
                    if (_year === MaxYear && j === MaxMonth && k === MaxDay) break;
                    if (_year === year && month === j && k < day - 1) continue;
                    days.push(k + 1);
                }
                const _days = {};
                _days[this.getMonthString(j + 1)] = days;
                months.push(_days);
            }
            const _data = {};
            _data[_year] = months;
            data.push(_data);
            m.add(1, 'jYear');
        }
        return data;
    },

    getMonthString(number) {
        switch (number) {
            case 1:
                return 'فروردین';
            case 2:
                return 'اردیبهشت';
            case 3:
                return 'خرداد';
            case 4:
                return 'تیر';
            case 5:
                return 'مرداد';
            case 6:
                return 'شهریور';
            case 7:
                return 'مهر';
            case 8:
                return 'آبان';
            case 9:
                return 'آذر';
            case 10:
                return 'دی';
            case 11:
                return 'بهمن';
            case 12:
                return 'اسفند';
            default:
                return '';
        }
    },

    convertWeightToString(weight) {
        let w = '';
        if (weight < 1000) {
            w = `${weight} کیلوگرم `;
        } else {
            w = `${weight / 1000} تن `;
        }
        return w;
    }

};

export default Utils;
