import Constants from './../utility/Constants';
import WebService from './WebService';

export default class Repository
{
    //login 
    static PhoneRegisterNaturalPersonApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneRegisterNaturalPerson`, { method: 'POST', headers });
    }

    //check code number after login
    static PhoneLoginNaturalPersonApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneLoginNaturalPerson`, { method: 'POST', headers });
    }
 //PhoneAddRequestWareHelperByHelper
 static PhoneAddRequestWareHelperByHelperApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddRequestWareHelperByHelper`, { method: 'POST', headers });
}
//PhoneShowRequestWareDetialApi
static PhoneShowRequestWareDetialApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneShowRequestWareDetial`, { method: 'POST', headers });
}
 //PhoneShowAllReciveWareApi
 static PhoneShowAllReciveWareApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneShowAllReciveWare`, { method: 'POST', headers });
}
//PhoneDeleteRequestWareHelperApi
static PhoneDeleteRequestWareHelperApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneDeleteRequestWareHelper`, { method: 'POST', headers });
}
//PhoneShowAllWareDetailsHelerApi
static PhoneShowAllWareDetailsHelerApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneShowAllWareDetailsHeler`, { method: 'POST', headers });
}
  //PhoneEditRequestWareHelperByHelper
  static PhoneEditRequestWareHelperByHelperApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneEditRequestWareHelperByHelper`, { method: 'POST', headers });
}
    //edit profile or register
    static PhoneUpdateNaturalPersonApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneUpdateNaturalPerson`, { method: 'POST', headers });
    }

    //PhoneGetDeliverWasteHelperApi
    //get user detail
    static PhoneGetInfoNaturalPersonApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetInfoNaturalPerson`, { method: 'POST', headers });
    }

    //PhoneGetDeliverWasteByDeliveryWasteID
    static PhoneGetDeliverWasteByDeliveryWasteIDApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDeliverWasteByDeliveryWasteID`, { method: 'POST', headers });
    }

    //PhoneSettingsForHelper
    static PhoneSettingsForHelperApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneSettingsForHelper`, { method: 'POST', headers });
    }   

    //PhoneDelRequestDeliverWasteApi
    static PhoneDelRequestDeliverWasteApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneDelRequestDeliverWaste`, { method: 'POST', headers });
    }

    //PhoneGetDeliverWasteHelperApi
    static PhoneGetDeliverWasteHelperApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDeliverWasteHelper`, { method: 'POST', headers });
    }

    //PhoneAddRequestDeliverWaste
    static PhoneAddRequestDeliverWasteApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneAddRequestDeliverWaste`, { method: 'POST', headers });
    }
//PhoneAddReqSurveyHelper
static PhoneAddReqSurveyHelperApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddReqSurveyHelper`, { method: 'POST', headers });
}
    //get user points and hamyari
    static PhoneGetInfoCooperationApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetInfoCooperation`, { method: 'POST', headers });
    }
     //get user points and hamyari
     static PhoneGetInfoCooperationApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetInfoCooperation`, { method: 'POST', headers });
    }
     //PhoneGetWareOfBooth 
   static PhoneGetWareOfBoothApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetWareOfBooth`, { method: 'POST', headers });
}
    //get user points and hamyari
    static PhoneGetSuggestionForHelperApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetSuggestionForHelper`, { method: 'POST', headers });
    }   
     //get user points and hamyari
     static PhoneGetStatusSuggestionForHelperApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetStatusSuggestionForHelper`, { method: 'POST', headers });
    }
    //PhoneShowAppRules
    static PhoneShowAppRulesApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneShowAppRules`, { method: 'POST', headers });
    }
    //PhoneAddSuggestionForHelper
    static PhoneAddSuggestionForHelperApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneAddSuggestionForHelper`, { method: 'POST', headers });
    }
    //PhoneShowAppGuideApi
    static PhoneShowAppGuideApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneShowAppGuide`, { method: 'POST', headers });
    }
    //PhoneAddRequestWareApi
    static PhoneAddRequestWareApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneAddRequestWare`, { method: 'POST', headers });
    }
    
    //add regeant code
    static PhoneAddReagentCodeHelperApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneAddReagentCodeHelper`, { method: 'POST', headers });
    }
    //PhoneQuestionsSurveyHelper
    static PhoneQuestionsSurveyHelperApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneQuestionsSurveyHelper`, { method: 'POST', headers });
    }
    
    //notice
    static PhoneGetNotificationForHelperApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetNotificationForHelper`, { method: 'POST', headers });
    }
      //Advertisment
      static PhoneGetAdvertisementForHelperApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetAdvertisementForHelper`, { method: 'POST', headers });
    }
    //PhoneGetInfoCreditManagement
    static PhoneGetInfoCreditManagementApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetInfoCreditManagement`, { method: 'POST', headers });
    }
     //ConfirmSelectPackage
  static PhoneAddRequestPackageApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddRequestPackage`, { method: 'POST', headers });
}
  //PhoneGetCharity
  static PhoneGetCharityApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetCharity`, { method: 'POST', headers });
}
 //PhoneAddDiscountCard
 static PhoneAddDiscountCardApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddDiscountCard`, { method: 'POST', headers });
}

//PhoneAddCharityPayment
static PhoneAddCharityPaymentApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddCharityPayment`, { method: 'POST', headers });
}
//PhoneAddCashpayment
static PhoneAddCashpaymentApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddCashpayment`, { method: 'POST', headers });
}
//PhoneAddSupportingHico
static PhoneAddSupportingHicoApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddSupportingHico`, { method: 'POST', headers });
}
   //PhoneGetWastType
   static PhoneGetWastTypeApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetWastType`, { method: 'POST', headers });
}
//PhoneAddLikeNotificationForHelper

 static PhoneAddLikeNotificationForHelperApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneAddLikeNotificationForHelper`, { method: 'POST', headers });
}
  //PhoneGetDetermineDeliveryTime
  static PhoneGetDetermineDeliveryTimeApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDetermineDeliveryTime`, { method: 'POST', headers });
}
//PhoneGetMoreDetermineDeliveryTime
static PhoneGetMoreDetermineDeliveryTime(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetMoreDetermineDeliveryTime`, { method: 'POST', headers });
}
//PhoneGetTimeOfSettringCollect
static PhoneGetTimeOfSettringCollect(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetTimeOfSettringCollect`, { method: 'POST', headers });
}
  //PhoneGetWare
  static PhoneGetWareApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetWare`, { method: 'POST', headers });
}
 
//PhoneGetDiscountCard
static PhoneGetDiscountCardApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDiscountCard`, { method: 'POST', headers });
}
//PhoneGetPackage
static PhoneGetPackageApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetPackage`, { method: 'POST', headers });
}

//PhoneGetNotificationByIDApi
    static PhoneGetNotificationByIDApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetNotificationByID`, { method: 'POST', headers });
    }
//PhoneGetAdvertisementByIDForHelperApi
static PhoneGetAdvertisementByIDForHelperApi(params, headers) {
    return WebService._requestSync(params, `${Constants.API_URL}PhoneGetAdvertisementByIDForHelper`, { method: 'POST', headers });
}
    //report
    static PhoneGetDeliveredWasteReportApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDeliveredWasteReport`, { method: 'POST', headers });
    }

    static PhoneGetDeliveredWasteReportItemApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDeliveredWasteReportItem`, { method: 'POST', headers });
    }

    static PhoneGetFinancialHelperReportApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetFinancialHelperReport`, { method: 'POST', headers });
    }

    static PhoneGetReceivedWareHelperReportApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetReceivedWareHelperReport`, { method: 'POST', headers });
    }

    static PhoneGetReceivedWareHelperReportItemApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetReceivedWareHelperReportItem`, { method: 'POST', headers });
    }
    
    // hamyari list
    static PhoneGetDeliverWasteHelperApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetDeliverWasteHelper`, { method: 'POST', headers });
    }

    //get bank name detail
    static PhoneGetBankApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetBank`, { method: 'POST', headers });
    }

    //get area name and id detail
    static PhoneGetMainAreasApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetMainAreas`, { method: 'POST', headers });
    }  
    
    //PhoneGetAllBoothes
    static PhoneGetAllBoothesApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetAllBoothes`, { method: 'POST', headers });
    }  
    
    //PhoneGetBoothesItemByBoothesIDApi
    static PhoneGetBoothesItemByBoothesIDApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetBoothesItemByBoothesID`, { method: 'POST', headers });
    }
    
    //PhoneGetLocationContractor
    static PhoneGetLocationContractorApi(params, headers) {
        return WebService._requestSync(params, `${Constants.API_URL}PhoneGetLocationContractor`, { method: 'POST', headers });
    }


}
