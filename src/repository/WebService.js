const _HEADERS = { 'Content-Type': 'application/json' };
const _REQUEST_TIMEOUT = 55000;
const _UPLOAD_TIMEOUT = 240000;

const WebService = {

    _tryParse(value) {
        let result = {};
        try
        {
            result = JSON.parse(value);
        }
        catch (ignored) {}
        return result;
    },

    _requestSync(params, url, options = {}, onProgress) {
        options = {
            timeout: options.timeout || _REQUEST_TIMEOUT,
            headers: options.headers || _HEADERS,
            method: options.method || 'POST',
        };

        console.log('\n');
        console.log('%c *********************START**********************', 'background: #222; color: #bada55');
        console.log('url    is: ', url);
        console.log('method is: ', options.method);
        console.log('params is: ', params);
        console.log('header is: ', options.headers);

        const headerKeys = Object.keys(options.headers);
        const headerVals = Object.values(options.headers);

        if (options.method === 'GET')
        {
            if (params && Object.keys(params).length > 0)
                { url = `${url}?${ 
                    Object.keys(params).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`).join('&')}`; }
        }

        return new Promise((resolve, reject) => {
                const xhr = new XMLHttpRequest();

                xhr.open(options.method, url);
                xhr.timeout = options.timeout;

                for (const index in headerKeys)
                    { xhr.setRequestHeader(headerKeys[index], headerVals[index]); }

                if (headerKeys['Content-Type'] === undefined)
                    { xhr.setRequestHeader('Content-Type', 'application/json'); }

                if (xhr.upload && onProgress) { xhr.upload.onprogress = onProgress; }

                xhr.onload = (event) => {
                    resolve(WebService._tryParse(event.target.responseText));
                    console.log('result is: ', WebService._tryParse((event.target.responseText)));
                    console.log('%c *********************END************************', 'background: #222; color: #bada55');
                    console.log('\n');
                };

                xhr.onerror = (error) => {
                    reject(error);
                    console.log('error is: ', error);
                    console.log('%c *********************END************************', 'background: #222; color: #bada55');
                    console.log('\n');
                };

                xhr.onabort = () => {
                    reject('abort');
                    console.log('error is: ', 'aborted');
                    console.log('%c *********************END************************', 'background: #222; color: #bada55');
                    console.log('\n');
                };

                xhr.ontimeout = () => {
                    reject('timeout');
                    console.log('error is: ', 'timeout');
                    console.log('%c *********************END************************', 'background: #222; color: #bada55');
                    console.log('\n');
                };

                if (options.method === 'GET')
                    { xhr.send(); }

                if (options.method === 'POST')
                    { xhr.send(JSON.stringify(params)); }

                xhr.stop = xhr.abort;

                setTimeout(() => {
                    if (xhr && !xhr._aborted) {
                        xhr.abort();
                        reject('abort');
                    }
                }, options.timeout);
            }
        );
    },

    _upload(params, url, options = {}, onProgress) {
        options = {
            timeout: options.timeout || _UPLOAD_TIMEOUT,
            headers: options.headers || _HEADERS,
            method: 'POST',
        };

        console.log('\n');
        console.log('%c *********************START**********************', 'background: #222; color: #bada55');
        console.log('url    is: ', url);
        console.log('method is: ', options.method);
        console.log('params is: ', params);
        console.log('header is: ', options.headers);

        const headerKeys = Object.keys(options.headers);
        const headerVals = Object.values(options.headers);

        return new Promise((resolve, reject) => {
                const xhr = new XMLHttpRequest();

                xhr.open(options.method, url);
                xhr.timeout = options.timeout;

                for (const index in headerKeys)
                    { xhr.setRequestHeader(headerKeys[index], headerVals[index]); }

                if (xhr.upload && onProgress)
                    { xhr.upload.onprogress = onProgress; }

                xhr.onload = (event) => {
                    resolve(WebService._tryParse(event.target.responseText));
                    console.log('result is: ', WebService._tryParse(event.target.responseText));
                    console.log('%c *********************END************************', 'background: #222; color: #bada55');
                    console.log('\n');
                };

                xhr.onerror = (error) => {
                    reject(error);
                    console.log('error is: ', error);
                    console.log('%c *********************END************************', 'background: #222; color: #bada55');
                    console.log('\n');
                };

                xhr.onabort = () => {
                    reject('abort');
                };

                xhr.ontimeout = () => {
                    reject('timeout');
                };

                xhr.send(params);

                xhr.stop = xhr.abort;
            }
        );
    }

};

/******************************************************************************


 WebService.TryParse = (value) =>
 {
     let result = {};
     try { result = JSON.parse (value) } catch (ignored) {}
     return result;
 };

 WebService._requestSync = (params, url, options = {}) =>
 {
     options = {
         timeout : options.timeout || _REQUEST_TIMEOUT ,
         headers : options.headers || _HEADERS         ,
         method  : options.method  || 'POST'           ,
     };

     console.log ('method is: ', options.method);
     console.log ('params is: ', params);
     console.log ('header is: ', options.headers);

     const headerKeys = Object.keys   (options.headers);
     const headerVals = Object.values (options.headers);

     if (options.method === 'GET')
     {
         if (params && Object.keys (params).length > 0)
             url = url + '?' +
                 (Object.keys (params).map (k => encodeURIComponent (k) + '=' + encodeURIComponent (params[k])).join ('&'));
     }

     return new Promise (function (resolve, reject)
         {
             let xhr = new XMLHttpRequest ();

             xhr.open (options.method, url);
             xhr.timeout = options.timeout;

             for (let index in headerKeys)
                 xhr.setRequestHeader (headerKeys [index], headerVals [index]);

             if (headerKeys['Content-Type'] == undefined)
                 xhr.setRequestHeader ('Content-Type', 'application/json');

             xhr.onload = (event) =>
             {
                 resolve (WebService.TryParse (event.target.responseText));
                 console.log ('result is: ', WebService.TryParse (event.target.responseText));
             };

             xhr.onerror = (error) => {
                 reject(error);
                 console.log ('error is: ', error);
             };

             xhr.onabort = () => {
                 reject('abort');
             };

             xhr.ontimeout = () => {
                 reject('timeout');
             };

             if (options.method === 'GET')
                 xhr.send ();

             if (options.method === 'POST')
                 xhr.send (JSON.stringify (params));

             xhr.stop = xhr.abort;
         }
     );
 };

 WebService._upload = (params, url, options = {}, onProgress) =>
 {
     options = {
         timeout : options.timeout || _REQUEST_TIMEOUT ,
         headers : options.headers || _HEADERS         ,
         method  : 'POST'                              ,
     };

     console.log ('params is: ', params);
     console.log ('header is: ', options.headers);

     const headerKeys = Object.keys   (options.headers);
     const headerVals = Object.values (options.headers);

     return new Promise (function (resolve, reject)
         {
             let xhr = new XMLHttpRequest ();

             xhr.open (options.method, url);
             xhr.timeout = options.timeout;

             for (let index in headerKeys)
                 xhr.setRequestHeader (headerKeys [index], headerVals [index]);

             if (headerKeys['Content-Type'] == undefined)
                 xhr.setRequestHeader ('Content-Type', 'application/json');

             if (xhr.upload && onProgress)
                 xhr.upload.onprogress = onProgress;

             xhr.onload = (event) =>
             {
                 resolve (WebService.TryParse (event.target.responseText));
                 console.log ('result is: ', WebService.TryParse (event.target.responseText));
             };

             xhr.onerror = (error) => {
                 reject(error);
                 console.log ('error is: ', error);
             };

             xhr.onabort = () => {
                 reject('abort');
             };

             xhr.ontimeout = () => {
                 reject('timeout');
             };

             xhr.send (params);

             xhr.stop = xhr.abort;
         }
     );
 };

 WebService._requestCallBack = (params, url, callback, options = {}) =>
 {
     options = {
         timeout : options.timeout || _REQUEST_TIMEOUT ,
         headers : options.headers || _HEADERS         ,
         method  : options.method  || 'POST'           ,
     };
     console.log('method is: ', options.method);
     console.log('params is: ', params);
     console.log('header is: ', options.headers);
     const headerKeys = Object.keys   (options.headers);
     const headerVals = Object.values (options.headers);

     if (options.method === 'GET')
     {
         if (params && Object.keys (params).length > 0)
             url = url + '?' +
                 (Object.keys (params).map (k => encodeURIComponent (k) + '=' + encodeURIComponent (params[k])).join ('&'));
     }

     const xhr = new XMLHttpRequest ();

     xhr.open (options.method, url, true);
     xhr.timeout = options.timeout;

     for (let index in headerKeys)
         xhr.setRequestHeader (headerKeys [index], headerVals [index]);

     xhr.onload = (event) =>
     {
         if (callback) callback (WebService.TryParse (event.target.responseText));
         console.log('result is: ', WebService.TryParse (event.target.responseText));
     };

     xhr.onerror = (error) => {
         if (callback) callback (null, 'onError :(');
     };

     xhr.onabort = () => {
         if (callback) callback (null, 'onAbort :(');
     };

     xhr.ontimeout = () => {
         if (callback) callback (null, 'onTimeout :(');
     };

     if (options.method === 'GET')
         xhr.send ();


     if (options.method === 'POST')
         xhr.send (JSON.stringify (params));

     xhr.stop = xhr.abort;

     return xhr;
 };

 WebService._requestFetch = (params, url, options = {}) =>
 {
     options = {
         headers : options.headers || _HEADERS         ,
         method  : options.method  || 'POST'           ,
     };

     console.log ('method is: ', options.method);
     console.log ('params is: ', params);
     console.log ('header is: ', options.headers);

     if (options.method === 'GET')
     {
         if (params && Object.keys (params).length > 0)
             url = url + '?' +
                 (Object.keys (params).map (k => encodeURIComponent (k) + '=' + encodeURIComponent (params[k])).join ('&'));
     }

     let didTimeOut = false;

     return new Promise(function(resolve, reject) {
         const timeout = setTimeout(function() {
             didTimeOut = true;
             reject(new Error('Request timed out'));
         }, _REQUEST_TIMEOUT);

         fetch(url, options)
             .then(function(response) {
                 clearTimeout(timeout);
                 if(!didTimeOut) {
                     resolve(WebService.TryParse (response));
                 }
             })
             .catch(function(err) {
                 console.log('fetch failed! ', err);
                 if(didTimeOut) return;
                 reject(err);
             });
     })
 };

 *****************************************************************************/

export default WebService;
