import { Platform } from 'react-native';
import {
    createStackNavigator,
    createBottomTabNavigator,
    BottomTabBar,
    createDrawerNavigator,
    TabBarTop,
    createMaterialTopTabNavigator
} from 'react-navigation';

import EStyleSheet from 'react-native-extended-stylesheet';
import Colors from './utility/Colors';
import { GetDim } from './utility/Constants';
import Drawer from './components/drawer/Drawer';
import SplashScreen from './screens/splashScreen/SplashScreen';
import IntroScreen from './screens/introScreen/IntroScreen';
import LoginScreen from './screens/authScreen/LoginScreen';
import LoginConfirmScreen from './screens/authScreen/LoginConfirmScreen';
import RegisterScreen from './screens/authScreen/RegisterScreen';
import ReagentCodeScreen from './screens/authScreen/ReagentCodeScreen';
import ProfileScreen from './screens/dashboardScreen/profileScreen/ProfileScreen';
import NoticeScreen from './screens/dashboardScreen/noticeScreen/NoticeScreen';
import AdvertismentScreen from './screens/dashboardScreen/advertismentScreen/AdvertismentScreen';

import NoticeDetailScreen from './screens/dashboardScreen/noticeScreen/NoticeDetailScreen';
import AdvertismentDetailScreen from './screens/dashboardScreen/advertismentScreen/AdvertismentDetailScreen';

import ReportScreen from './screens/dashboardScreen/reportScreen/ReportScreen';
import ResidueScreen from './screens/dashboardScreen/residueScreen/ResidueScreen';
import MainPageScreen from './screens/dashboardScreen/mainPageScreen/MainPageScreen';
import ManageUserScreen from './screens/dashboardScreen/manageUserScreen/ManageUserScreen';

import WasteListScreen from './screens/dashboardScreen/wasteListScreen/WasteListScreen';
import WasteReportDetailScreen from './screens/dashboardScreen/reportScreen/WasteReportDetailScreen';
import WareReportDetailScreen from './screens/dashboardScreen/reportScreen/WareReportDetailScreen';
import WasteDetailScreen from './screens/dashboardScreen/wasteListScreen/WasteDetailScreen';

import AboutScreen from './screens/aboutScreen/AboutScreen';
import GuidScreen from './screens/aboutScreen/GuidScreen';

import IntroductionScreen from './screens/introductionScreen/IntroductionScreen';
import SelectPackageScreen from './screens/registerPasmandScreen/selectPackageScreen/SelectPackageScreen';
import DeliveredwasteScreen from './screens/registerPasmandScreen/deliveredwasteScreen/DeliveredwasteScreen';
import DeliveredwasteNewScreen from './screens/registerPasmandScreen/deliveredwasteNewScreen/DeliveredwasteNewScreen';
import DeliveryTimeScreen from './screens/registerPasmandScreen/deliveryTimeScreen/DeliveryTimeScreen';
import DeliveryTimeNewScreen from './screens/registerPasmandScreen/deliveryTimeNewScreen/DeliveryTimeNewScreen';
import DeliveryTypeScreen from './screens/creditManagementScreen/deliveryTypeScreen/DeliveryTypeScreen';
import RequestDetailsScreen from './screens/registerPasmandScreen/requestDetailsScreen/RequestDetailsScreen';
import RequestListScreen from './screens/requestListScreen/RequestListScreen';
import CashWithdrawalScreen from './screens/creditManagementScreen/cashWithdrawalScreen/CashWithdrawalScreen';
import SupportingHicoScreen from './screens/creditManagementScreen/supportingHicoScreen/SupportingHicoScreen';
import CharityScreen from './screens/creditManagementScreen/charityScreen/CharityScreen';
import SpecialServiceScreen from './screens/creditManagementScreen/specialServiceScreen/SpecialServiceScreen';
import ProductScreen from './screens/creditManagementScreen/productScreen/ProductScreen';
import EditProductScreen from './screens/creditManagementScreen/productScreen/EditProductScreen';
import RequestProductScreen from './screens/creditManagementScreen/productScreen/RequestProductScreen';
import ConfirmScreen from './screens/confirmScreen/ConfirmScreen';
import {TabbarComponent} from './components/TabbarComponent';
import PhoneQuestionsSurveyHelper from './screens/requestListScreen/PhoneQuestionsSurveyHelper';
import MapAreaScreen from './screens/registerPasmandScreen/mapAreaScreen/MapAreaScreen';
import SuggestionListScreen from './screens/SuggestionScreen/SuggestionListScreen'
import SuggestionAddScreen from './screens/SuggestionScreen/SuggestionAddScreen'
import SelectedProductScreen from './screens/creditManagementScreen/selectedProductScreen/SelectedProductScreen';


const Dashboard = createBottomTabNavigator({
    notice: {
        screen: NoticeScreen
    },
    Advertisement: {
        screen: AdvertismentScreen
    },
    ManageUserScreen: {
        screen: ManageUserScreen
    },
    DeliveryTypeScreen:{
        screen:DeliveryTypeScreen
    },
    MainPageScreen:{
        screen:MainPageScreen
    }
}, {
    initialRouteName: 'MainPageScreen',
    swipeEnabled: false,
    tabBarOptions: {
        showLabel: false,
        activeTintColor: Colors.green,
        inactiveTintColor: Colors.gray,
        style: {
            backgroundColor: '#e0e0e0',
            height: Platform.OS === 'android' ? 62 : 60,
        },
    },
    tabBarComponent: BottomTabBar,
    tabBarPosition: 'bottom'
});

const drawerWidth = (GetDim().width / 1.4 > 275) ? GetDim().width / 1.6 : GetDim().width / 1.2;

// const DrawerScreen = createDrawerNavigator({
//     drawer: { screen: Dashboard}
   
// }, {
//     contentComponent: Drawer,
//     drawerWidth,
//     drawerPosition: 'right',
//     //drawerType: 'right',
//     //drawerLockMode: 'unlocked'
// });

export const Router = createStackNavigator({
    splash: { screen: SplashScreen },
    intro: { screen: IntroScreen },
    login: { screen: LoginScreen },
    loginConfirm: { screen: LoginConfirmScreen },
    register: { screen: RegisterScreen },
    reagentCode: { screen: ReagentCodeScreen },
    dashboard: { screen: Dashboard },
    noticeDetail: { screen: NoticeDetailScreen },
    advertismentDetail: { screen: AdvertismentDetailScreen },
    wasteReportDetail: { screen: WasteReportDetailScreen },
    wareReportDetail: { screen: WareReportDetailScreen },
    wasteListDetail: { screen: WasteDetailScreen },
    AboutScreen: { screen: AboutScreen },
    GuidScreen: { screen: GuidScreen },
    IntroductionScreen:{screen:IntroductionScreen},
    SelectPackageScreen: { screen: SelectPackageScreen },
    DeliveredwasteScreen:{ screen: DeliveredwasteScreen },
    DeliveredwasteNewScreen:{ screen: DeliveredwasteNewScreen },
    DeliveryTimeScreen:{screen:DeliveryTimeScreen},
    DeliveryTimeNewScreen:{screen:DeliveryTimeNewScreen},
    wasteList: {screen: WasteListScreen},
    RequestDetailsScreen:{screen:RequestDetailsScreen},
    RequestListScreen:{screen:RequestListScreen},
    CashWithdrawalScreen:{screen:CashWithdrawalScreen},
    SupportingHicoScreen:{screen:SupportingHicoScreen},
    CharityScreen:{screen:CharityScreen},
    SpecialServiceScreen:{screen:SpecialServiceScreen},
    ProductScreen:{screen:ProductScreen},
    EditProductScreen:{screen:EditProductScreen},
    RequestProductScreen:{screen:RequestProductScreen},
    report:{screen:ReportScreen},
    profile: {screen: ProfileScreen},
    ConfirmScreen:{screen:ConfirmScreen},
    PhoneQuestionsSurveyHelper:{screen:PhoneQuestionsSurveyHelper},
    MapAreaScreen:{screen:MapAreaScreen},
    SuggestionListScreen:{screen:SuggestionListScreen},
    SuggestionAdd:{screen:SuggestionAddScreen},
    SelectedProductScreen:{screen:SelectedProductScreen}

}, {
    headerMode: 'none',
    initialRouteName: 'splash',
});
