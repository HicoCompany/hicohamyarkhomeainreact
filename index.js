/** @format */

import moment from 'moment-jalaali';
import { AppRegistry, YellowBox } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { name as appName } from './app.json';
import App from './src/App';

moment.loadPersian({ dialect: 'persian-modern' });

YellowBox.ignoreWarnings ([
    'Remote debugger', 
    'Require cycle:',
]);

EStyleSheet.build({
    $IR_L: 'IRANYekanLightMobile(FaNum)',
    $IR_M: 'IRANYekanRegularMobile(FaNum)',
    $IR_B: 'IRANYekanMobileBold(FaNum)',
});

AppRegistry.registerComponent(appName, () => App);
